<?php


$title = 'Апи документација';

ob_start();

?>


    <h2 class="text-center"></h2>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="tabele">

                    <h5>Почетна страница</h5>
                    <table class="table">
                        <thead>
                        <tr class="table-success">
                            <th>Метода</th>
                            <th>Урл</th>
                            <th>Опис</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://www.iteh-sajt.com/api</td>
                            <td>Приказ АПИ документације.</td>
                        </tr>
                        </tbody>
                    </table>

                    <h5>Аутори АПИ</h5>
                    <table class="table">
                        <thead>
                        <tr class="table-success">
                            <th>Метода</th>
                            <th>Урл</th>
                            <th>Опис</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/autorapi</td>
                            <td>Враћа све ауторе сајта</td>
                        </tr>
                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/autorapi/id/{num}</td>
                            <td>Враћа аутора чији ИД је једнак {num} вредности. {num} је целобројна вредност.</td>
                        </tr>
                        <tr>
                            <td class="table-info">POST</td>
                            <td>http://iteh-sajt.com/autorapi/create</td>
                            <td>Креира новог аутора чији подаци су смештени у телу захтева.</td>
                        </tr>
                        </tbody>
                    </table>

                    <h5>Производ АПИ</h5>
                    <table class="table">
                        <thead>
                        <tr class="table-success">
                            <th>Метода</th>
                            <th>Урл</th>
                            <th>Опис</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/proizvodapi</td>
                            <td>Враћа све производе.</td>
                        </tr>

                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/proizvodapi/id/{num}</td>
                            <td>Враћа производ чији је ИД једнак {num} вредности.</td>
                        </tr>

                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/proizvodapi/kategorija/{num}</td>
                            <td>Враћа све производе чији је ИД категорије једнак {num} вредности.</td>
                        </tr>

                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/proizvodapi/nazivLike/{string}</td>
                            <td>Враћа све производе чији назив садржи текст који је прослеђен као {string} параметар.
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <h5>Продавница АПИ</h5>
                    <table class="table">
                        <thead>
                        <tr class="table-success">
                            <th>Метода</th>
                            <th>Урл</th>
                            <th>Опис</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/prodavnicaapi</td>
                            <td>Враћа све продавнице.</td>
                        </tr>

                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/prodavnicaapi/id/{num}</td>
                            <td>Враћа продавницу чији је ИД једнак {num} вредности.</td>
                        </tr>

                        <tr>
                            <td class="table-info">POST</td>
                            <td>http://iteh-sajt.com/prodavnicaapi/create</td>
                            <td>Креира нову продавницу чији подаци су смештени у телу захтева.</td>
                        </tr>

                        <tr>
                            <td class="table-info">PUT</td>
                            <td>http://iteh-sajt.com/prodavnicaapi/update/{num}</td>
                            <td>Врши измену продавнице чији ИД је једнак {num} вредности, а измењени подаци се налазе у
                                телу
                                захтева.
                            </td>
                        </tr>

                        <tr>
                            <td class="table-info">DELETE</td>
                            <td>http://iteh-sajt.com/prodavnicaapi/delete/{num}</td>
                            <td>Брише продавницу чији ИД је једнак {num} вредности.</td>
                        </tr>
                        </tbody>
                    </table>

                    <h5>Коментар АПИ</h5>
                    <table class="table">
                        <thead>
                        <tr class="table-success">
                            <th>Метода</th>
                            <th>Урл</th>
                            <th>Опис</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/komentarapi</td>
                            <td>Враћа све коментаре који су постављени на производима.</td>
                        </tr>

                        </tbody>
                    </table>

                    <h5>Категорија АПИ</h5>
                    <table class="table">
                        <thead>
                        <tr class="table-success">
                            <th>Метода</th>
                            <th>Урл</th>
                            <th>Опис</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/kategorijaapi</td>
                            <td>Враћа све категорије производа.</td>
                        </tr>

                        <tr>
                            <td class="table-info">GET</td>
                            <td>http://iteh-sajt.com/kategorijaapi/id/{num}</td>
                            <td>Враћа категорију производа чији је ИД једнак {num} вредности.</td>
                        </tr>

                        <tr>
                            <td class="table-info">POST</td>
                            <td>http://iteh-sajt.com/kategorijaapi/create</td>
                            <td>Креира нову категорију производа чији подаци су смештени у телу захтева.</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


<?php

$content = ob_get_clean();

ob_start();
?>
    <script>
        $(function () {
            $('.table tbody tr td:first-child').css({
                'width': '80',
                'max-width': '80',
                'text-align': 'center'
            });

            $('.table tbody tr td:nth-child(2)').css({
                'width': '400',
                'max-width': '400'
            });
        });
    </script>
<?php

$js = ob_get_clean();

echo render('template.php',
    array_merge($data, array(
        'content' => $content,
        'title' => $title,
        'headerTitle' => 'Апи документација',
        'headerDescription' => 'Документација за коришћење веб сервиса',
        'js' => $js,
    )));
