<?php


ob_start();

?>

    <div class="row">
        <div class="col-md-1"></div>

        <div class="col-md-5">
            <div id="potrosen-iznos-po-mesecima"></div>
        </div>

        <div class="col-md-5">
            <div id="broj-porucenih-proizvoda-po-kategoriji"></div>
        </div>

        <div class="col-md-1"></div>
    </div>


    <div class="row">
        <div class="col-md-1"></div>


        <div class="col-md-10">


            <?php
            if (empty($porudzbine)) {
                echo 'Тренутно немате поруџбина.';
            } else {
                ?>
                <div class="alert alert-info" role="alert">
                    Имате укупно <?= (isset($paginacija)) ? $paginacija['count'] : '0' ?> поруџбина.
                </div>


                <div class="card-deck">


                    <?php
                    foreach ($porudzbine as $porudzbina) {
                        /** @var Porudzbina $porudzbina */
                        ?>
                        <div class="col-md-4">
                            <div class="card border-dark" style="width: auto;margin-bottom: 10px;">
                                <a href="/proizvod/show/<?= $porudzbina->getProizvod()->getNaziv() ?>" target="_blank">
                                    <img class="card-img-top" src="<?= $porudzbina->getProizvod()->getSlika() ?>"
                                         width="300"
                                         height="300px;" alt=".<?= $porudzbina->getProizvod()->getNaziv() ?> . slika">
                                </a>

                                <div class="card-body">
                                    <h5 class="card-title"><?= $porudzbina->getProizvod()->getNaziv() ?></h5>
                                    <p class="card-text">
                                        Датум поруџбине:
                                        <?= $porudzbina->getDatum()->format('d.m.Y.') ?>
                                    </p>

                                    <p class="card-text">
                                        Цена:
                                        <span class="cena">
                                            <?= $porudzbina->getCena() ?>
                                        </span>

                                    </p>
                                    <p class="card-text">
                                        Количина: <?= $porudzbina->getKolicina() ?>
                                    </p>
                                    <p class="card-text">
                                        Укупна цена:
                                        <span class="ukupna-cena">
                                            <?= $porudzbina->getKolicina() * $porudzbina->getCena() ?>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div> <!-- card deck kraj-->

                <?= (isset($paginacija)) ? $paginacija['html'] : '' ?>

                <?php
            } // else kraj
            ?>
        </div>

        <div class="col-md-1"></div>
    </div>

<?php

$content = ob_get_clean();

ob_start();

?>


    <script>
        $(function () {
            var potrosenIznosPoMesecima = new FusionCharts({
                    type: 'column2d',
                    renderAt: 'potrosen-iznos-po-mesecima',
                    width: '100%',
                    height: '300',
                    dataFormat: 'json',
                    dataSource: {
                        "chart": {
                            "theme": "fint",
                            "caption": "Износ куповине по месецима за " + new Date().getFullYear() + ". годину",
                            "xAxisName": "Месец",
                            "yAxisName": "Износ",
                            "numberSuffix": " RSD",
                            "formatNumberScale": "0",
                            "thousandSeparator": ".",
                            "rotateValues": "0",
                            "exportEnabled": "1",
                            "subCaptionFontSize": "11",
                            "highColor": "#6baa01",
                            "lowColor": "#e44a00",
                            "chartBottomMargin": "30",
                            "plotSpacePercent": "50"
                        },
                        "data": <?= $iznosKupovinePoMesecimaData ?>
                    }
                }
            );
            potrosenIznosPoMesecima.render();


            var brojPorucenihProizvodaPoKategoriji = new FusionCharts({
                type: 'pie2d',
                renderAt: 'broj-porucenih-proizvoda-po-kategoriji',
                width: '100%',
                height: '300',
                dataFormat: 'json',
                dataSource: {
                    "chart": {
                        "caption": "Број поручених производа по категорији",
                        "xAxisName": "Категорија",
                        "yAxisName": "Број поручених производа",
                        "exportEnabled": "1",
                        "numberPrefix": "",
                        "numberSuffix": " комада",
                        "rotateValues": "0",
                        "thousandSeparator": ".",
                        "formatNumberScale": "0",
                        "showLegend": "1",
                        "legendPosition": "right",
                        "theme": "fint"
                    },

                    "data": <?= $brojPorucenihProizvodaPoKategoriji ?>
                }
            });
            brojPorucenihProizvodaPoKategoriji.render();
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template.php', array(
    'headerTitle' => 'Поруџбине',
    'title' => 'Поруџбине',
    'content' => $content,
    'js' => $js,
));
