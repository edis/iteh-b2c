<?php


ob_start();


?>


    <h1 class="text-center text-danger">Страница коју тражите не постоји</h1>


<?php

$content = ob_get_clean();

echo render('template.php', array_merge($data, array(
    'content' => $content,
)));
