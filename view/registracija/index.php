<?php

$title = 'Регистрација';

ob_start();

?>

    <div class="container col-md-4">

        <div class="alert alert-info alert-dismissible fade show" role="alert" id="poruka-info">
            <div class="poruka">

            </div>
        </div>

        <div align="center" class="card container">

            <form action="#" method="POST" novalidate autocomplete="off" id="form-registracija">

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="ime" class="col-form-label">Име <span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="ime" name="ime" value="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="prezime" class="col-form-label">Презиме <span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="prezime" name="prezime" value="">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="email" class="col-form-label">Имејл <span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="email" name="email" value="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="sifra" class="col-form-label">Шифра <span class="text-danger">*</span></label>
                        <input type="password" class="form-control" id="sifra" name="sifra" value="">

                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-outline-info">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                            Региструјте се
                        </button>

                        <a href="/gituser/" class="btn btn-outline-info">
                            <img src="https://d301sr5gafysq2.cloudfront.net/06e6aed05468/img/icons/svg/github.svg"
                                 alt="Регистрација преко гит налога"
                                 width="20" height="20">
                            Гитхаб
                        </a>
                    </div>
                </div>
            </form>

        </div>
    </div>

<?php

$content = ob_get_clean();

ob_start();

?>

    <script>
        const $ime = $('#ime');
        const $prezime = $('#prezime');
        const $email = $('#email');
        const $sifra = $('#sifra');

        const $poruka = $('#poruka-info');

        function prikaziPoruku(poruka) {
            sakrijPoruku();
            $poruka.children('.poruka').html(poruka);
            $poruka.show();
        }

        function sakrijPoruku() {
            $poruka.hide();
        }

        $(function () {

            sakrijPoruku();

            $('#form-registracija').on({
                'submit': function (e) {
                    e.preventDefault();

                    ime = $ime.val().trim();
                    prezime = $prezime.val().trim();
                    email = $email.val().trim();
                    sifra = $sifra.val().trim();

                    if (ime.length == 0) {
                        prikaziPoruku('Нисте унели име.');
                        return;
                    }

                    if (prezime.length == 0) {
                        prikaziPoruku('Нисте унели презиме.');
                        return;
                    }

                    if (email.length < 10) {
                        prikaziPoruku('Имејл адреса мора имати бар 10 карактера.');
                        return;
                    }

                    if (sifra.length < 3) {
                        prikaziPoruku('Шифра мора имати бар 3 карактера.');
                        return;
                    }

                    $.post('/registracija/create', {
                        ime: ime,
                        prezime: prezime,
                        email: email,
                        sifra: sifra
                    }, function (data) {
                        prikaziPoruku(data);
                    });
                }
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template.php', array_merge($data, array(
    'content' => $content,
    'title' => $title,
    'headerTitle' => 'Регистрација',
    'js' => $js,
)));