<?php


ob_start();

?>


    <div class="row">
        <div class="col-md-1"></div>


        <div class="col-md-10">


            <?php
            if (empty($proizvodi)) {
                echo 'Ваша корпа је празна.';
            } else {
                ?>
                <div class="alert alert-info" role="alert">
                    Имате укупно <?= (isset($paginacija)) ? $paginacija['count'] : '0' ?> производа у корпи
                </div>


                <div class="card-deck">


                    <?php
                    foreach ($proizvodi as $proizvod) {
                        /** @var Korpa $proizvod */
                        ?>
                        <div class="col-md-4">
                            <div class="card border-dark" style="width: auto;margin-bottom: 10px;">
                                <a href="/proizvod/show/<?= $proizvod->getProizvod()->getNaziv() ?>" target="_blank">
                                    <img class="card-img-top" src="<?= $proizvod->getProizvod()->getSlika() ?>"
                                         width="300"
                                         height="300px;" alt=".<?= $proizvod->getProizvod()->getNaziv() ?> . slika">
                                </a>

                                <div class="card-body">
                                    <h5 class="card-title"><?= $proizvod->getProizvod()->getNaziv() ?></h5>
                                    <p class="card-text">
                                        Цена:
                                        <span class="cena" cena="<?= $proizvod->getCena() ?>">
                                            <?= $proizvod->getCena() ?>
                                        </span>

                                    </p>
                                    <p class="card-text">
                                        Количина:
                                        <input type="number" value="<?= $proizvod->getKolicina() ?>" class="kolicina"
                                               min="1" max="20"
                                               data-link="/korpa/promenaKolicine/<?= $proizvod->getKorisnik()->getId() ?>/<?= $proizvod->getProizvod()->getId() ?>">
                                    </p>
                                    <p class="card-text">
                                        Укупна цена:
                                        <span class="ukupna-cena">
                                            <?= $proizvod->getKolicina() * $proizvod->getCena() ?>
                                        </span>
                                    </p>

                                    <a href="/porudzbina/create/<?= $proizvod->getProizvod()->getId() ?>"
                                       class="btn btn-success btn-block naruci">
                                        Наручи
                                    </a>

                                    <a href="/korpa/obrisi/<?= $proizvod->getProizvod()->getId() ?>"
                                       class="btn btn-danger btn-block obrisi">
                                        Уклони из корпе
                                    </a>


                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div> <!-- card deck kraj-->


                <?= (isset($paginacija)) ? $paginacija['html'] : '' ?>

                <?php
            } // else kraj
            ?>
        </div>

        <div class="col-md-1"></div>
    </div>


    <div class="modal" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Порука</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="modal-poruka"></p>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>


<?php

$content = ob_get_clean();

ob_start();

?>


    <script>

        const $naruci = $('.naruci');
        const $obrisi = $('.obrisi');
        const $modal = $('#modal');
        const $kolicina = $('.kolicina');

        function redirect() {
            setTimeout(function () {
                location.href = '/korpa';
            }, 1000);
        }

        $(function () {

            $naruci.click(function (e) {
                e.preventDefault();

                const link = $(this).attr('href');

                $.get(link, function (data) {
                    $modal.modal({show: true, backdrop: 'static'});
                    $modal.find('#modal-poruka').html(data);
                    if (data.toLowerCase().indexOf('успешно') > -1) {
                        redirect();
                    }
                });

            });

            $obrisi.click(function (e) {
                e.preventDefault();

                const link = $(this).attr('href');

                $.get(link, function (data) {
                    $modal.modal({show: true, backdrop: 'static'});
                    $modal.find('#modal-poruka').html(data);
                    redirect();
                });
            });

            $kolicina.change(function () {
                const cena = parseInt($(this).parent().parent().children('.card-text').children('.cena').attr('cena'));
                const link = $(this).attr('data-link');
                const novaKolicina = $(this).val();


                if (novaKolicina > 0 && novaKolicina < 20) {
                    $(this).parent().parent().children('.card-text').children('.ukupna-cena').html((cena * novaKolicina));

                    $.post(link, {
                        nova_kolicina: novaKolicina
                    }, function (data) {
                        if (data == false) {
                            location.href = '';
                        }
                    });
                }

            });

        });
    </script>


<?php

$js = ob_get_clean();

echo render('template.php', array(
    'headerTitle' => 'Производи у корпи',
    'title' => 'Производи у корпи',
    'content' => $content,
    'js' => $js,
));
