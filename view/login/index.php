<?php

$title = 'Пријава';

ob_start();

?>

    <div class="container col-md-4">

        <div class="alert alert-info alert-dismissible fade show" role="alert" id="poruka-info">
            <div class="poruka">

            </div>
        </div>

        <div align="center" class="card container">

            <form action="#" method="POST" novalidate autocomplete="off" id="form-prijava">

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="email" class="col-form-label">Имејл <span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="email" name="email" value="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="sifra" class="col-form-label">Шифра <span class="text-danger">*</span></label>
                        <input type="password" class="form-control" id="sifra" name="sifra" value="">

                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-outline-info">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                            Пријава
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

<?php

$content = ob_get_clean();

ob_start();

?>

    <script>

        const $email = $('#email');
        const $sifra = $('#sifra');

        const $poruka = $('#poruka-info');

        function prikaziPoruku(poruka) {
            sakrijPoruku();
            $poruka.children('.poruka').html(poruka);
            $poruka.show();
        }

        function sakrijPoruku() {
            $poruka.hide();
        }

        $(function () {

            sakrijPoruku();

            $('#form-prijava').on({
                'submit': function (e) {
                    e.preventDefault();

                    email = $email.val().trim();
                    sifra = $sifra.val().trim();

                    if (email.length < 10) {
                        prikaziPoruku('Имејл адреса мора имати бар 10 карактера.');
                        return;
                    }

                    if (sifra.length < 3) {
                        prikaziPoruku('Шифра мора имати бар 3 карактера.');
                        return;
                    }

                    $.post('/login/login', {
                        email: email,
                        sifra: sifra
                    }, function (data) {
                        prikaziPoruku(data);
                        if (data.toLowerCase().indexOf('успеш') > -1) {
                            setTimeout(function () {
                                location.href = '/';
                            }, 800);
                        }
                    });
                }
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template.php', array_merge($data, array(
    'content' => $content,
    'title' => $title,
    'headerTitle' => 'Пријава',
    'js' => $js,
)));