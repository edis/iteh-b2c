<?php

$title = 'Производи';
ob_start();


?>

    <div class="alert alert-info" role="alert">
        <span>Изаберите категорију:</span>
        <select name="kategorija" id="kategorija" class="form-control" style="width: 300px;display: inline;">
            <option value="-1" selected>Све категорије</option>
            <?php
            /** @var Kategorija $kategorija */
            foreach ($kategorije as $kategorija) {
                ?>
                <option value="<?= $kategorija->getId() ?>">
                    <?= $kategorija->getNaziv() ?>
                </option>
                <?php
            }
            ?>
        </select>

        <p style="display: inline"> или претражите по називу</p>
        <input type="text" name="search_by_name" id="search_by_name" class="form-control"
               style="display: inline;width: 300px;" placeholder="Претрага по називу">
    </div>


    <p></p>


    <div class="row" id="prikaz-proizvoda">

        <?php
        if (isset($proizvodi) && !is_null($proizvodi) && !empty($proizvodi)) {

            foreach ($proizvodi as $proizvod) {
                ?>
                <div class="col-md-3">
                    <div class="card border-dark" style="width: auto;margin-bottom: 10px;">
                        <img class="card-img-top" src="<?= $proizvod->getSlika() ?>" width="300"
                             height="300px;" alt=".<?= $proizvod->getNaziv() ?> . slika">
                        <div class="card-body">
                            <h5 class="card-title"><?= $proizvod->getNaziv() ?></h5>
                            <p class="card-text">Категорија: <?= $proizvod->getKategorija()->getNaziv() ?></p>
                            <p class="card-text">Произвођач: <?= $proizvod->getProizvodjac()->getNaziv() ?></p>
                            <p class="card-text">Боја: <?= $proizvod->getBoja() ?></p>
                            <p class="card-text">Цена: <?= $proizvod->getCena() ?></p>

                            <a href="/proizvod/show/<?= $proizvod->getNaziv() ?>"
                               class="btn btn-primary btn-block">Више детаља...</a>
                        </div>
                    </div>
                </div>

                <?php
            }

        } // if kraj
        ?>

    </div>

<?= (isset($paginacija)) ? $paginacija['html'] : '' ?>


<?php

$content = ob_get_clean();

ob_start();

?>

    <script>
        const $kategorija = $('#kategorija');
        const $proizvodiPrikaz = $('#prikaz-proizvoda');
        const $search = $('#search_by_name');

        function prikaziProizvode(proizvodiJson) {
            $proizvodiPrikaz.html('');

            if (proizvodiJson.total == 0) {
                $proizvodiPrikaz.html('Нема производа који задовољавају критеријум.');
                return;
            }

            $.each(proizvodiJson.data, function (i, p) {
                $proizvodiPrikaz.append('<div class="col-md-3">\n' +
                    '                    <div class="card border-dark" style="width: auto;margin-bottom: 10px;">\n' +
                    '                        <img class="card-img-top" src="' + p.slika + '" width="300"\n' +
                    '                             height="300px;" alt=".<?= $proizvod->getNaziv() ?> . slika">\n' +
                    '                        <div class="card-body">\n' +
                    '                            <h5 class="card-title">' + p.naziv + '</h5>\n' +
                    '                            <p class="card-text">Категорија: ' + p.kategorija.naziv + '</p>\n' +
                    '                            <p class="card-text">Произвођач:' + p.proizvodjac.naziv + ' </p>\n' +
                    '                            <p class="card-text">Боја: ' + p.boja + '</p>\n' +
                    '                            <p class="card-text">Цена: ' + p.cena + '</p>\n' +
                    '\n' +
                    '                            <a href="/proizvod/show/' + p.naziv + '"\n' +
                    '                               class="btn btn-primary btn-block">Више детаља...</a>\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                </div>');
            });
        }

        $(function () {
            $kategorija.on({
                change: function () {
                    const kategorijaId = parseInt($(this).val());
                    if (kategorijaId == -1) {
                        location.href = '/proizvod';
                    }

                    $.get('/proizvodapi/kategorija/' + kategorijaId, function (data) {
                        prikaziProizvode(data);
                    });

                }
            });

            $search.on({
                keyup: function () {
                    var txt = $(this).val().trim();
                    if (txt == '') {
                        $.get('/proizvodapi/' + txt, function (data) {
                            prikaziProizvode(data);
                        });
                    }
                    else {
                        $.get('/proizvodapi/nazivLike/' + txt, function (data) {
                            prikaziProizvode(data);
                        });
                    }
                }
            });
        });

    </script>

<?php

$js = ob_get_clean();

echo render('template.php', array_merge($data, array(
    'content' => $content,
    'headerTitle' => 'Производи',
    'title' => $title,
    'headerDescription' => '',
    'js' => $js,
)));

