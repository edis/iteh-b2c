<?php

/** @var Proizvod $proizvod */

$title = (!empty($proizvod->getNaziv())) ? $proizvod->getNaziv() : '';
ob_start();

?>
    <div class="container">
        <div class="row">


            <?php
            if (!empty($proizvod) && is_object($proizvod))
            {
            ?>

            <div class="col-sm" style="margin-left:-50px;">
                <img class="card-img-top" src="<?= $proizvod->getSlika() ?>" width="300px"
                     height="500px;" alt="<?= $proizvod->getNaziv() ?> slika">

            </div>
            <div class="col-sm">
                <h5 class="card-title" style="font-size: 20px"><?= $proizvod->getNaziv() ?></h5>
                <p class="card-text"><b>Категорија:</b> <?= $proizvod->getKategorija()->getNaziv() ?></p>
                <p class="card-text"><b>Произвођач:</b> <?= $proizvod->getProizvodjac()->getNaziv() ?></p>
                <p class="card-text"><b>Боја:</b> <?= $proizvod->getBoja() ?></p>
                <p class="card-text">
                    <b>Цена:</b>
                    <span id="cena">
                        <?= $proizvod->getCena() ?>
                    </span>
                    <span id="valuta-prikaz">
                        RSD
                    </span>
                </p>
                <p class="card-text"><b>Спецификација:</b> <?= $proizvod->getSpecifikacija() ?></p>

                <p class="card-text">
                    <b>Валута:</b>
                    <select name="valuta" id="valuta">
                        <option value="rsd">RSD</option>
                        <option value="eur">EUR</option>
                        <option value="usd">USD</option>
                    </select>
                </p>

                <button id="stavi-u-korpu" data-id="<?= $proizvod->getId() ?>" type="button"
                        class="btn btn-primary btn-lg" style=" margin-top: 30px">Стави у корпу
                </button>

                <BR>

                <div class="alert alert-info" role="alert" id="poruka">

                </div>
            </div>
        </div>
    </div>
    </div>


    <?php

    if (isset($_COOKIE['id']) || isset($_COOKIE['admin_id'])) {
        ?>

        <form action="/komentar/addComment/<?= $proizvod->getId() ?>" method="POST">
            <div class="form-group">
                <label for="formGroupExampleInput" style="font-size: 25px; font-weight: bold;margin-left: 100px">Оставите
                    коментар</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="komentar" rows="5"
                          style="width: 600px;height: 100px; margin-left: 100px"></textarea>


            </div>
            <button type="submit" class="btn btn-primary" style="margin-left: 100px">Пошаљи</button>
        </form>
        <?php
    }
    ?>

    <?php

    if (isset($komentari)) {

        /** @var Komentar $komentar */
        foreach ($komentari as $komentar) {
            ?>
            <div class="container" style="margin-top: 20px">
                <div class="row">
                    <div class="col-sm-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1">
                        <div class="thumbnail" style="padding: 0px">
                            <?php
                            $slika = $komentar->getKorisnik()->getSlika();
                            if (empty($slika)) {
                                ?>
                                <img class="img-responsive user-photo"
                                     src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png"
                                     style="height: 75px;width:75px;">

                                <?php
                            } else if (filter_var($slika, FILTER_VALIDATE_URL)) {
                                ?>
                                <img class="img-responsive user-photo"
                                     src="<?= $slika ?>"
                                     style="height: 75px;width:75px;">

                                <?php
                            } else {
                                ?>
                                <img class="img-responsive user-photo"
                                     src="<?= '/slike/' . $slika ?>"
                                     style="height: 75px;width:75px;">

                                <?php
                            }
                            ?>

                        </div>
                    </div>

                    <div class="col-sm-7">
                        <div class="panel panel-default"
                             style=" border-radius: 5px; border-color: lightgray; border-style: solid solid ">
                            <div class="panel-heading" style="background: #f7f7f7">
                                <strong>
                                    <?= $komentar->getKorisnik()->getIme() ?>
                                    <?php
                                    if ($komentar->getKorisnik()->getUloga()->getRang() == 111) {
                                        ?>
                                        <b class="text-danger" style="font-size: 8pt;">Администратор</b>
                                        <?php
                                    }
                                    ?>
                                </strong>

                                <span class="text-muted" style="margin-left: 5px">
                                    <?= Util::compareDates($komentar->getDatumIVreme()) ?>
                                </span>
                            </div>
                            <div class="panel-body" style="height: auto; min-height: 50px;word-wrap: break-word">
                                <?= $komentar->getKomentarSaSmajlijima() ?>
                            </div>

                        </div>

                    </div>
                    <?php
                    if (isset($_COOKIE['admin_id'])) {
                        ?>
                        <a href="/komentar/edit/<?= $komentar->getId() ?>" class="btn btn-outline-primary"
                           style="height: 40px;margin-right:10px ;">Измени</a>
                        <a href="/komentar/delete/<?= $komentar->getId() ?>" class="btn btn-danger"
                           style="height: 40px">Обриши</a>
                        <?php
                    }
                    ?>

                </div>


            </div>


            <?php
        }
    }

    ?>

    <?php
} else {
    echo "Нема резултата";
}
?>


<?php


$content = ob_get_clean();


ob_start();

?>

    <script>

        const $staviUKorpu = $('#stavi-u-korpu');
        const $poruka = $('#poruka');
        const $valuta = $('#valuta');
        const $cena = $('#cena');
        const $valutaPrikaz = $('#valuta-prikaz');
        const cenaRSD = parseFloat($cena.html());

        $(function () {

            $poruka.hide();

            $poruka.click(function () {
                $(this).hide();
            });

            $staviUKorpu.click(function (e) {
                const proizvodId = $(this).attr('data-id');

                $.get('/korpa/dodajUKorpu/' + proizvodId, function (data) {
                    $poruka.html(data);
                    $poruka.show();
                });
            });

            $valuta.on({
                'change': function () {
                    const valutaVal = $(this).val();
                    const uri = 'https://api.kursna-lista.info/f53ec1381124cf3ac11a0ac413c7ee76/konvertor/rsd/' + valutaVal + '/' + cenaRSD;

                    $.ajax({
                        method: 'GET',
                        dataType: 'jsonp',
                        url: uri
                    }).done(function (data) {
                        const kolicina = data.result.amount * cenaRSD;
                        const cena = data.result.value;
                        $cena.html((Math.round(cena * kolicina * 100) / Math.pow(10, 2)));
                        $valutaPrikaz.html(valutaVal.toUpperCase());
                    });
                }
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template.php', array_merge($data, array(
    'content' => $content,
    'headerTitle' => $title,
    'title' => $title,
    'headerDescription' => '',
    'js' => $js,
)));

