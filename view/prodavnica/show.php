<?php

ob_start();


?>


    <div class="row">
        <div class="col-lg-3"></div>

        <div class="col-lg-6">

            <iframe width="100%" height="450" frameborder="0" style="border:0"
                    src="https://www.google.com/maps/embed/v1/place?q=fon&key=AIzaSyDY2AnfzApvef4sCiarzBjY5ES_J2wN74w"
                    allowfullscreen></iframe>


            <?php

            /** @var Prodavnica $prodavnica */
            if (!empty($prodavnica)) {
                ?>
                <h1><?= $prodavnica->getNaziv(); ?></h1>

                <table class="table table-responsive">
                    <tr>
                        <td>Имејл адреса:</td>
                        <td><?= $prodavnica->getEmail(); ?></td>
                    </tr>

                    <tr>
                        <td>Телефон:</td>
                        <td><?= $prodavnica->getTelefon(); ?></td>
                    </tr>

                    <tr>
                        <td>Адреса:</td>
                        <td><?= $prodavnica->getAdresa(); ?></td>
                    </tr>

                    <tr>
                        <td>Радно време:</td>
                        <td><?= $prodavnica->getRadnoVreme(); ?></td>
                    </tr>


                </table>
                <?php
            } else {
                ?>
                Продавница није пронађена.
                <?php
            }
            ?>
        </div>

        <div class="col-lg-3"></div>
    </div>


<?php

$content = ob_get_clean();


echo render('template.php', array_merge($data, array(
    'content' => $content,
    'title' => 'Приказ свих продавница',
    'headerTitle' => (!empty($prodavnica)) ? $prodavnica->getNaziv() : 'Продавница није пронађена',
)));
