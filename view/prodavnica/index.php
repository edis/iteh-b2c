<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-2"></div>

        <div class="col-lg-8">

            <div class="card-deck">

                <?php
                foreach ($prodavnice as $prodavnica) {
                    /** @var Prodavnica $prodavnica */
                    ?>
                    <div class="col-lg-4">
                        <div class="card" style="margin-bottom: 20px;">
                            <div class="card-body">
                                <h5 class="card-title"><?= $prodavnica->getNaziv() ?></h5>
                                <p class="card-text"><?= $prodavnica->getEmail() ?></p>
                                <a class="btn btn-info" href="/prodavnica/show/<?= $prodavnica->getId() ?>">
                                    Више детаља...
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div> <!-- card deck kraj-->

            <!--PAGINACIJA-->
            <?= (isset($paginacija)) ? $paginacija['html'] : '' ?>


        </div> <!-- col lg-6 kraj -->

        <div class="col-lg-2"></div>


    </div>


<?php

$content = ob_get_clean();


echo render('template.php', array_merge($data, array(
    'content' => $content,
    'title' => 'Приказ свих продавница',
    'headerTitle' => 'Наше продавнице',
)));
