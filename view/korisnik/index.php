<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 28-Jan-18
 * Time: 02:15
 */

ob_start();


?>

<?php
/** @var Korisnik $korisnik */
if (isset($korisnik)) {
    ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <?php
                if (empty($korisnik->getSlika())) {
                    ?>
                    <img src="http://www.personalbrandingblog.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640-300x300.png"
                         class="img-responsive" style="width:100%"
                         alt="Image">

                    <?php
                } else if (filter_var($korisnik->getSlika(), FILTER_VALIDATE_URL)) {
                    ?>
                    <img src="<?= $korisnik->getSlika() ?>" width="150" height="150"
                         class="img-responsive img-thumbnail" style="width:100%"
                         alt="Image">
                    <?php
                } else {
                    ?>
                    <img src="<?= '/slike/' . $korisnik->getSlika() ?>" width="150" height="150"
                         class="img-responsive img-thumbnail" style="width:100%"
                         alt="Image">

                    <?php
                }
                ?>

            </div>
            <div class="col-sm-4">
                <div class="well">
                    <p class="card-text">Име: <?= $korisnik->getIme() ?></p>
                </div>
                <div class="well">
                    <p class="card-text">Презиме: <?= $korisnik->getPrezime() ?></p>
                </div>
                <div class="well">
                    <p class="card-text">Имејл: <?= $korisnik->getEmail() ?></p>
                </div>

                <div class="well">
                    <p class="card-text">
                        <?= ($korisnik->getUloga()->getRang() == 111) ? '<b class="text-danger">Администратор</b>' : '' ?>
                    </p>
                </div>
                <div class="well" style=" height: 50px; width: 50px;">
                    <a href="/korisnik/edit">
                        <img style=" height: 50px; width: 50px;"
                             src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Edit_font_awesome.svg/120px-Edit_font_awesome.svg.png"
                             alt="Измени">
                    </a>
                </div>
            </div>
        </div>
        <hr>
    </div>

    <?php
}
?>

<?php

$content = ob_get_clean();


echo render('template.php', array(
    'content' => $content,

    'title' => 'Профил',
    'headerTitle' => 'Профил',
));
