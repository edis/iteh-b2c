<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 30-Jan-18
 * Time: 01:10
 */

$title = "Измена профила";

ob_start();


/** @var Korisnik $korisnik */
if (isset($korisnik)) {
    ?>
    <div class="container">


        <div class="row">


            <!-- left column -->
            <div class="col-md-3">
                <div class="text-center">
                    <?php
                    if (empty($korisnik->getSlika())) {
                        ?>
                        <img src="//placehold.it/150" class="avatar img-circle" alt="Слика">
                        <?php
                    } else if (filter_var($korisnik->getSlika(), FILTER_VALIDATE_URL)) {
                        ?>
                        <img src="<?= $korisnik->getSlika() ?>" class="img-fluid rounded-circle"
                             width="150" height="150" alt="Слика">
                        <?php
                    } else {
                        ?>
                        <img src="<?= '/slike/' . $korisnik->getSlika() ?>" class="img-fluid rounded-circle"
                             width="150" height="150" alt="Слика">
                        <?php
                    }
                    ?>

                    <form action="/korisnik/uploadSlika" method="POST" enctype="multipart/form-data">
                        <input type="file" class="form-control" name="slika">
                        <button type="submit" class="btn btn-outline-success">Постави слику</button>
                    </form>


                </div>
            </div>

            <!-- edit form column -->
            <div class="col-md-9 personal-info">

                <input type="button" class="btn btn-info" id="btnSifra" value="Промени шифру"
                       onclick="promeniSifru()">
                <input type="button" class="btn btn-info" id="btnProfil" value="Промени профил"
                       onclick="promeniProfil()">


                <div class="alert alert-info alert-dismissable fade show" role="alert" id="poruka-info">
                    <div class="poruka">

                    </div>
                </div>

                <h3>Подаци корисника</h3>

                <form action="" method="POST" novalidate autocomplete="off" id="form-izmenaprofila"
                      class="form-horizontal">
                    <div id="izmenaProfila">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Име:</label>
                            <div class="col-lg-8">
                                <input id="ime" class="form-control" type="text" value=<?= $korisnik->getIme() ?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Презиме:</label>
                            <div class="col-lg-8">
                                <input id="prezime" class="form-control" type="text"
                                       value=<?= $korisnik->getPrezime() ?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Имејл:</label>
                            <div class="col-lg-8">
                                <input id="email" class="form-control" type="text" value=<?= $korisnik->getEmail() ?>>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-primary" value="Сачувај измене">
                            <span></span>
                            <input type="button" class="btn btn-default" value="Одустани" onclick="odustani()">
                        </div>
                    </div>
                </form>
                <form action="" method="POST" novalidate autocomplete="off" id="form-izmenasifre"
                      class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Шифра:</label>
                        <div class="col-md-8">
                            <input id="sifra" class="form-control" type="password" value=<?= $korisnik->getSifra() ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Потврди шифру:</label>
                        <div class="col-md-8">
                            <input id="sifra1" class="form-control" type="password" value=<?= $korisnik->getSifra() ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-primary" value="Сачувај измене">

                            <span></span>
                            <input type="button" class="btn btn-default" value="Одустани" onclick="odustani()">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <hr>

    <?php
}
?>

<?php
$content = ob_get_clean();

ob_start();
?>
    <script>
        const $poruka = $('#poruka-info');
        const $formSifra = $('#form-izmenasifre');
        const $formProfil = $('#form-izmenaprofila');
        const $btnSifra = $('#btnSifra');
        const $btnProfil = $('#btnProfil');


        $btnProfil.hide();
        $btnSifra.show();

        $formProfil.show();
        $formSifra.hide();

        const $ime = $('#ime');
        const $prezime = $('#prezime');
        const $email = $('#email');
        const $sifra = $('#sifra');
        const $sifra1 = $('#sifra1');


        function prikaziPoruku(poruka) {
            sakrijPoruku();
            $poruka.children('.poruka').html(poruka);
            $poruka.show();
        }

        function sakrijPoruku() {
            $poruka.hide();
        }

        function promeniSifru() {
            sakrijPoruku();
            $formProfil.hide();
            $formSifra.show();

            $btnProfil.show();
            $btnSifra.hide();

            $('#form-izmenasifre').on({
                'submit': function (e) {
                    e.preventDefault();

                    sifra = $sifra.val().trim();
                    sifra1 = $sifra1.val().trim();

                    if (sifra.length < 3) {
                        prikaziPoruku('Шифра мора имати бар 3 карактера.');
                        $('#sifra').focus();
                        return;
                    }

                    if (sifra != sifra1) {
                        prikaziPoruku('Шифре се не поклапају. Покушајте поново.');
                        $('#sifra1').focus();
                        return;
                    }
                    $.post('/korisnik/updateSifra', {
                        sifra: sifra,
                        sifra1: sifra1
                    }, function (data) {
                        prikaziPoruku(data);
                    });
                }
            });
        }

        function promeniProfil() {
            sakrijPoruku();
            $formProfil.show();
            $formSifra.hide();

            $btnProfil.hide();
            $btnSifra.show();

            $('#form-izmenaprofila').on({
                'submit': function (e) {
                    e.preventDefault();

                    ime = $ime.val().trim();
                    prezime = $prezime.val().trim();
                    email = $email.val().trim();

                    if (ime.length == 0) {
                        prikaziPoruku('Нисте унели име.');
                        $('#ime').focus();
                        return;
                    }

                    if (prezime.length == 0) {
                        prikaziPoruku('Нисте унели презиме.');
                        $('#prezime').focus();
                        return;
                    }

                    if (email.length < 10) {
                        prikaziPoruku('Имејл адреса мора имати бар 10 карактера.');
                        $('#email').focus();
                        return;
                    }

                    $.post('/korisnik/updateProfil', {
                        ime: ime,
                        prezime: prezime,
                        email: email
                    }, function (data) {
                        prikaziPoruku(data);
                    });
                }
            });
        }

        function odustani() {
            window.location.href = '/korisnik';
        }


        $(function () {
            sakrijPoruku();

            promeniSifru();
            promeniProfil();

        });

    </script>
<?php
$js = ob_get_clean();
echo render('template.php', array_merge($data, array(
    'content' => $content,
    'title' => $title,
    'headerTitle' => 'Измена профила',
    'js' => $js,
)));