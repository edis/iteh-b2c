<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= isset($title) ? $title : 'Веб сајт' ?></title>
    <?= requireFile('inc/css.php'); ?>
    <script src="/bootstrap/jquery.js"></script>
</head>
<body>


<?= renderInc('nav.php'); ?>

<?= renderInc('header.php', [
    'headerTitle' => isset($headerTitle) ? $headerTitle : 'Тест',
    'headerDescription' => isset($headerDescription) ? $headerDescription : '',
]); ?>


<?= renderInc('breadcrumb.php') ?>


<div class="container-fluid">
    <?= renderInc('view_message.php') ?>
    <?= isset($content) ? $content : ''; ?>
</div>


<?= requireFile('inc/js.php'); ?>

<?= (isset($js)) ? $js : ''; ?>


</body>
</html>