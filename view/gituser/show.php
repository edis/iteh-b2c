<?php


ob_start();


?>

    <div class="row">
        <div class="col-lg-4"></div>


        <div class="col-lg-4">
            <?php

            /** @var $user GitUser */
            if (isset($user) && !is_null($user) && is_object($user)) {
                ?>

                <div class="card" style="width: auto;">
                    <img class="card-img-top" src="<?= $user->getAvatarUrl() ?>" alt="Слика">
                    <div class="card-body">
                        <h4 class="card-title"><?= (is_null($user->getName())) ? $user->getUserName() : $user->getName() ?></h4>
                        <p class="card-text"><?= $user->getBio() ?></p>

                        <div class="list-group">
                            <a href="#" class="list-group-item list-group-item-action list-group-item-success btn"
                               data-toggle="modal" data-target="#registracijaModal">
                                Региструј се преко гит налога
                            </a>

                            <a href="<?= $user->getReposApiUrl() ?>" class="list-group-item list-group-item-action btn">
                                Прикажи репозиторијуме
                                <span class="badge badge-primary badge-pill"><?= $user->getNumberOfRepositories() ?></span>
                            </a>
                        </div>

                    </div>
                </div>


                <BR>

                <?php

            } else {
                ?>
                Није могуће учитати корисника
                <?php
            }
            ?>
        </div>


        <div class="col-lg-4"></div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="registracijaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <img src="<?= $user->getAvatarUrl() ?>" alt="Регистрација" id="slika">


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="alert alert-info alert-dismissible fade show" role="alert" id="poruka-info">
                        <div class="poruka">

                        </div>
                    </div>


                    <form action="#" method="POST" id="registracija-git">

                        <?php
                        $gitFullName = $user->getName();
                        if (strpos($gitFullName, ' ')) {
                            $splitted = explode(' ', $gitFullName);
                            $firstName = $splitted[0];
                            $lastName = $splitted[1];
                        }
                        ?>

                        <div class="form-group">
                            <label for="ime">Име</label>
                            <input type="text" autocomplete="off"
                                   value="<?= (isset($firstName)) ? $firstName : $user->getName() ?>"
                                   name="ime" id="ime" class="form-control" placeholder="Име">
                        </div>

                        <div class="form-group">
                            <label for="prezime">Презиме</label>
                            <input type="text" autocomplete="off" value="<?= (isset($lastName)) ? $lastName : '' ?>"
                                   name="prezime" id="prezime" class="form-control" placeholder="Презиме">
                        </div>

                        <div class="form-group">
                            <label for="email">Имејл</label>
                            <input type="text" autocomplete="off" value="<?= $user->getEmail() ?>"
                                   name="email" id="email" class="form-control" placeholder="Имејл">
                        </div>

                        <div class="form-group">
                            <label for="sifra">Шифра</label>
                            <input type="password"
                                   name="sifra" id="sifra" class="form-control" placeholder="Шифра">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Одустани</button>
                    <button type="submit" class="btn btn-success" form="registracija-git">Региструј се</button>
                </div>
            </div>
        </div>
    </div>


<?php

$content = ob_get_clean();

ob_start();

?>

    <script>

        const $registracijaModal = $('#registracijaModal');
        const $registracijaGit = $('#registracija-git');

        const $ime = $('#ime');
        const $prezime = $('#prezime');
        const $email = $('#email');
        const $sifra = $('#sifra');
        const $slika = $('#slika');

        const $poruka = $('#poruka-info');

        function prikaziPoruku(poruka) {
            sakrijPoruku();
            $poruka.children('.poruka').html(poruka);
            $poruka.show();
        }

        function sakrijPoruku() {
            $poruka.hide();
        }

        $(function () {

            sakrijPoruku();

            $registracijaModal.on('shown.bs.modal', function (e) {

            });

            $registracijaGit.submit(function (e) {
                e.preventDefault();

                ime = $ime.val().trim();
                prezime = $prezime.val().trim();
                email = $email.val().trim();
                sifra = $sifra.val().trim();
                slika = $slika.attr('src');

                if (ime.length == 0) {
                    prikaziPoruku('Нисте унели име.');
                    return;
                }

                if (prezime.length == 0) {
                    prikaziPoruku('Нисте унели презиме.');
                    return;
                }

                if (email.length < 10) {
                    prikaziPoruku('Имејл адреса мора имати бар 10 карактера.');
                    return;
                }

                if (sifra.length < 3) {
                    prikaziPoruku('Шифра мора имати бар 3 карактера.');
                    return;
                }

                $.post('/registracija/create', {
                    ime: ime,
                    prezime: prezime,
                    email: email,
                    sifra: sifra,
                    slika: slika
                }, function (data) {
                    prikaziPoruku(data);
                });
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template.php', array_merge($data, array(
    'content' => $content,
    'js' => $js,
    'title' => 'Приказ корисника',
    'headerTitle' => 'Приказ корисника',
)));
