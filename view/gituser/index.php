<?php


ob_start();

?>

    <div class="row">
        <div class="col-lg-4"></div>


        <div class="col-lg-4">

            <div>
                <p>
                    У поље за претрагу унесите корисничко име гитхаб корисника
                </p>

                <form action="#" method="POST" id="find-user-form">

                    <div class="form-group">
                        <input type="text" name="username" id="search-term" autocomplete="off" class="form-control"
                               placeholder="Корисничко име:" value="">
                    </div>

                    <button class="btn btn-default">Пронађи</button>
                </form>

                <BR>

                <div class="alert alert-danger" role="alert">
                    <div class="poruka">fwewfw</div>
                </div>


                <div class="prikaz">

                </div>
            </div>


        </div>


        <div class="col-lg-4"></div>
    </div>


<?php

$content = ob_get_clean();

ob_start();

?>


    <script>

        const $poruka = $('.poruka');

        function prikaziPoruku(poruka) {
            $poruka.html(poruka);
            $poruka.parent().show();
        }

        $(function () {

            $poruka.parent().hide();

            $poruka.on({
                click: function () {
                    $(this).html('');
                    $(this).parent().hide();
                    $('#search-term').focus();
                }
            });

            $('#find-user-form').submit(function (e) {
                e.preventDefault();

                var username = $("#search-term").val();

                if (username.trim() === '') {
                    prikaziPoruku('Морате унети корисничко име.');
                    return;
                }

                $.get('https://api.github.com/users/' + username, function (data) {
                    location.href = '/gituser/show/' + username;
                }).fail(function (data) {
                    prikaziPoruku('Унели сте непостојеће корисничко име');
                });
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template.php', array(
    'content' => $content,
    'js' => $js,
    'title' => 'Пронађи корисника на гиту',
    'headerTitle' => 'Претрага гитхаб корисника',
));
