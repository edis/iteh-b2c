<?php

ob_start();

?>


    <div class="row">


        <div class="col-lg-2"></div>


        <div class="col-lg-8" style="text-align: justify">


        </div>

    </div>

    <div class="col-lg-2"></div>

    </div>


<?php

$content = ob_get_clean();

ob_start();

?>


    <!--carousel pocetak-->
<?php
if (!empty($proizvodi)) {
    ?>

    <div id="proizvodi" class="carousel slide" data-ride="carousel" style="background: #e9ecef;">
        <ol class="carousel-indicators">
            <?php
            for ($i = 0; $i < count($proizvodi); $i++) {
                ?>

                <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i ?>"></li>
                <?php
            }
            ?>
        </ol>
        <div class="carousel-inner">
            <?php
            /** @var Proizvod $proizvod */
            for ($i = 0; $i < count($proizvodi); $i++) {
                ?>

                <div class="carousel-item text-center <?php if ($i == 0) echo 'active'; ?>">
                    <img class="w-50 text-center" src="<?= $proizvodi[$i]->getSlika() ?>" height="800"
                         alt="First slide">
                    <div class="carousel-caption d-md-block" style="color: black;">
                        <h1><?= $proizvodi[$i]->getNaziv() ?></h1>
                        <h5><?= $proizvodi[$i]->getCena() ?> динара</h5>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <a class="carousel-control-prev" href="#proizvodi" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#proizvodi" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <?php
}
?>
    <!--carousel kraj-->


<?php

$headerCarousel = ob_get_clean();


echo render('template.php', array_merge($data,
    [
        'content' => $content,
        'headerTitle' => $headerCarousel,
    ]
));
