<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6">


            <form action="/proizvod/create/" method="POST">
                <div class="form-group">
                    <label for="naziv">Назив производа</label>
                    <input type="text" name="naziv" id="naziv" class="form-control" placeholder="Назив"
                           value="" autocomplete="off" minlength="3" required>
                </div>

                <div class="form-group">
                    <label for="proizvodjac">Произвођач</label>

                    <select class="form-control" name="proizvodjac_id" id="proizvodjac" required style="display: block">
                        <option value="" selected disabled>Изаберите произвођача</option>
                        <?php
                        if (isset($proizvodjaci)) {

                            /** @var Proizvodjac $proizvodjac */
                            foreach ($proizvodjaci as $proizvodjac) {
                                ?>
                                <option value="<?= $proizvodjac->getId() ?>"> <?= $proizvodjac->getNaziv() ?></option>
                                <?php
                            }
                        } else {
                            ?>
                            <p> Нема доступних произвођача</p>
                            <?php
                        }

                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="kategorija">Категорија</label>
                    <select class="form-control" name="kategorija_id" id="kategorija" required style="display: block">
                        <option value="" selected disabled>Изаберите категорију</option>
                        <?php

                        if (isset($kategorije)) {

                            /** @var Kategorija $kategorija */
                            foreach ($kategorije as $kategorija) {
                                ?>
                                <option value="<?= $kategorija->getId() ?>"> <?= $kategorija->getNaziv() ?></option>
                                <?php
                            }
                        } else {
                            ?>
                            <p> Нема доступних категорија</p>
                            <?php
                        }
                        ?>
                    </select>

                </div>


                <div class="form-group">
                    <label for="slika">Слика</label>
                    <input type="text"
                           name="slika" id="slika" class="form-control" placeholder="Слика"
                           value="" autocomplete="off" required minlength="5"
                </div>


                <div class="form-group">
                    <label for="boja">Боја</label>
                    <input type="text"
                           name="boja" id="boja" class="form-control" placeholder="Боја"
                           value="" autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="kolicina_na_stanju">Количина на стању</label>
                    <input type="text"
                           name="kolicina_na_stanju" id="kolicina_na_stanju" class="form-control"
                           placeholder="Количина на стању"
                           value="" autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="specifikacija">Спецификација</label>
                    <textarea name="specifikacija" id="specifikacija" class="form-control"
                              placeholder="Спецификација"></textarea>
                </div>

                <div class="form-group">
                    <label for="cena">Цена</label>
                    <input type="text"
                           name="cena" id="cena" class="form-control" placeholder="Цена"
                           value="" autocomplete="off" required>
                </div>

                <div class="float-right">
                    <button type="submit" class="btn btn-success">Креирај производ</button>
                    <button type="button" id="odustani" class="btn btn-danger">Одустани</button>
                </div>


            </form>

        </div>

        <div class="col-lg-3"></div>


    </div>


<?php

$content = ob_get_clean();


ob_start();

?>


    <script>
        const $odustani = $('#odustani');

        $(function () {
            $odustani.click(function () {
                location.href = '/admin/proizvodi';
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => '',
    'headerTitle' => 'Унос података о производу',
    'js' => $js,
)));
