<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6">

            <?php
            if (empty($proizvod)) {

            } else {
                // ako nije empty($proizvod)
                /** @var Proizvod $proizvod */
                ?>

                <form action="/proizvod/update/<?= $proizvod->getId() ?>" method="POST">
                    <div class="form-group">
                        <label for="naziv">Назив производа</label>
                        <input type="text"
                               name="naziv" id="naziv" class="form-control" placeholder="Назив"
                               value="<?= $proizvod->getNaziv() ?>">
                    </div>

                    <div class="form-group">
                        <label for="slika">Слика</label>
                        <input type="text"
                               name="slika" id="slika" class="form-control" placeholder="Слика"
                               value="<?= $proizvod->getSlika() ?>">
                    </div>

                    <div class="form-group">
                        <label for="proizvodjac">Произвођач</label>
                        <select class="form-control" name="proizvodjac_id" id="proizvodjac" required>
                            <option value="<?= $proizvod->getProizvodjac()->getId(); ?>" selected>
                                <?= $proizvod->getProizvodjac()->getNaziv() ?>
                            </option>
                            <?php
                            if (isset($proizvodjaci)) {

                                /** @var Proizvodjac $proizvodjac */
                                foreach ($proizvodjaci as $proizvodjac) {
                                    if ($proizvodjac->getId() == $proizvod->getProizvodjac()->getId()) {
                                        continue;
                                    }
                                    ?>
                                    <option value="<?= $proizvodjac->getId() ?>"><?= $proizvodjac->getNaziv() ?>  </option>
                                    <?php
                                }

                            }
                            ?>
                        </select>

                    </div>


                    <div class="form-group">
                        <label for="adresa">Категорија</label>
                        <select class="form-control" name="kategorija_id" id="kategorija" required>
                            <option value="<?= $proizvod->getKategorija()->getId(); ?>" selected>
                                <?= $proizvod->getKategorija()->getNaziv() ?>
                            </option>
                            <?php
                            if (isset($kategorije)) {
                                /** @var Kategorija $kategorija */
                                foreach ($kategorije as $kategorija) {
                                    if ($kategorija->getId() == $proizvod->getKategorija()->getId()) {
                                        continue;
                                    }
                                    ?>
                                    <option value="<?= $kategorija->getId() ?>"><?= $kategorija->getNaziv() ?>  </option>
                                    <?php
                                }

                            }
                            ?>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="radno_vreme">Цена</label>
                        <input type="text"
                               name="cena" id="cena" class="form-control" placeholder="Цена"
                               value="<?= $proizvod->getCena() ?>">
                    </div>

                    <div class="form-group">
                        <label for="boja">Боја</label>
                        <input type="text"
                               name="boja" id="boja" class="form-control" placeholder="Боја"
                               value="<?= $proizvod->getBoja() ?>">
                    </div>

                    <div class="form-group">
                        <label for="kolicina_na_stanju">Количина на стању</label>
                        <input type="text"
                               name="kolicina_na_stanju" id="kolicina_na_stanju" class="form-control"
                               placeholder="Количина на стању"
                               value="<?= $proizvod->getKolicinaNaStanju() ?>">
                    </div>

                    <div class="form-group">
                        <label for="boja">Спецификација</label>
                        <input type="text"
                               name="specifikacija" id="specifikacija" class="form-control" placeholder="Спецификација"
                               value="<?= $proizvod->getSpecifikacija() ?>">
                    </div>

                    <div class="float-right">
                        <button type="submit" class="btn btn-success">Потврди измене</button>
                        <button type="button" id="odustani" class="btn btn-danger">Одустани</button>
                    </div>


                </form>

                <?php
            }
            ?>

        </div>

        <div class="col-lg-3"></div>


    </div>


<?php

$content = ob_get_clean();


ob_start();

?>


    <script>
        const $odustani = $('#odustani');

        $(function () {
            $odustani.click(function () {
                location.href = '/admin/proizvodi';
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => 'Измена података о производу',
    'headerTitle' => 'Измена података о производу',
    'js' => $js,
)));
