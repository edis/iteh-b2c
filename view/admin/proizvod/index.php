<?php

ob_start();


?>


    <div class="row">


        <div class="col-lg-1"></div>

        <div class="col-lg-10">

            <p>
                <a href="/proizvod/insert/" class="btn btn-outline-success">
                    Додајте производ
                </a>
            </p>


            <?php
            if (empty($proizvodi)) {
                ?>
                Тренутно нема производа.
                <?php
            } else {
                ?>

                <table class="table table-responsive data-table">
                    <thead>
                    <tr>
                        <th>Назив производа</th>
                        <th>Категорија</th>
                        <th>Произвођач</th>
                        <th>Цена</th>
                        <th>Спецификација</th>
                        <th>Количина на стању</th>
                        <th>Прикажи</th>
                        <th>Измени</th>
                        <th>Обриши</th>
                    </tr>
                    </thead>

                    <?php
                    foreach ($proizvodi as $proizvod) {
                        /** @var Proizvod $proizvod */
                        ?>

                        <tr>
                            <td><?= $proizvod->getNaziv() ?></td>
                            <td><?= $proizvod->getKategorija()->getNaziv() ?></td>
                            <td><?= $proizvod->getProizvodjac()->getNaziv() ?></td>
                            <td><?= $proizvod->getCena() ?></td>
                            <td><?= $proizvod->getSpecifikacija() ?></td>
                            <td><?= $proizvod->getKolicinaNaStanju() ?></td>

                            <td><a href="/proizvod/show/<?= $proizvod->getNaziv() ?>" class="btn btn-outline-secondary"
                                   target="_blank">Прикажи </a></td>
                            <td><a href="/proizvod/edit/<?= $proizvod->getId() ?>" class="btn btn-outline-primary">Измени</a>
                            </td>
                            <td><a href="/proizvod/delete/<?= $proizvod->getId() ?>"
                                   class="btn btn-danger">Обриши</a></td>
                        </tr>

                        <?php
                    }
                    ?>


                </table>

                <?php
            }// else kraj ( ako ima proizvoda)
            ?>
        </div> <!-- col lg-6 kraj -->

        <div class="col-lg-1"></div>


    </div>


<?php

$content = ob_get_clean();

ob_start();


?>

    <script>
        $(function () {

        });
    </script>

<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => 'Приказ свих производа',
    'headerTitle' => 'Производи',
    'js' => $js,
)));
