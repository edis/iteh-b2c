<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6">

            <?php
            if (empty($komentar)) {
                echo 'Грешка';
            } else {
                // ako nije empty($komentar)
                /** @var Komentar $komentar */
                ?>

                <form action="/komentar/update/<?= $komentar->getId() ?>" method="POST">
                    <div class="form-group">
                        <label for="korisnik">Корисник</label>
                        <input type="text"
                               name="korisnik" id="korisnik" class="form-control" placeholder="Корисник"
                               value="<?= $komentar->getKorisnik()->getIme() . ' ' . $komentar->getKorisnik()->getPrezime() ?>"
                               readonly>
                    </div>

                    <div class="form-group">
                        <label for="proizvod">Производ</label>
                        <input type="text"
                               name="proizvod" id="proizvod" class="form-control" placeholder="Производ"
                               value="<?= $komentar->getProizvod()->getNaziv() ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label for="komentar">Коментар</label>
                        <textarea name="komentar" id="komentar" class="form-control" placeholder="Коментар"
                                  style="min-height: 180px"><?= $komentar->getKomentar() ?></textarea>
                    </div>

                    <div class="float-right">
                        <button class="btn btn-success" type="submit">Измени</button>
                        <button class="btn btn-danger" id="odustani" type="button">Одустани</button>
                    </div>


                </form>

                <?php
            }
            ?>

        </div>

        <div class="col-lg-3"></div>


    </div>


<?php

$content = ob_get_clean();


ob_start();

?>


    <script>
        const $odustani = $('#odustani');

        $(function () {
            $odustani.click(function () {
                location.href = '/admin';
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => '',
    'headerTitle' => 'Измена коментара',
    'js' => $js,
)));
