<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6">

            <?php
            if (empty($korisnik)) {

            } else {
                // ako nije empty($korisnik)
                /** @var Korisnik $korisnik */
                ?>

                <form action="/korisnik/update/<?= $korisnik->getId() ?>" method="POST">
                    <div class="form-group">
                        <label for="ime">Име</label>
                        <input type="text"
                               name="ime" id="ime" class="form-control" placeholder="Име"
                               value="<?= $korisnik->getIme() ?>">
                    </div>

                    <div class="form-group">
                        <label for="prezime">Презиме</label>
                        <input type="text"
                               name="prezime" id="prezime" class="form-control" placeholder="Презиме"
                               value="<?= $korisnik->getPrezime() ?>">
                    </div>

                    <div class="form-group">
                        <label for="email">Имејл</label>
                        <input type="text"
                               name="email" id="email" class="form-control" placeholder="Имејл"
                               value="<?= $korisnik->getEmail() ?>">
                    </div>


                    <div class="form-group">
                        <label for="sifra">Шифра</label>
                        <input type="text"
                               name="sifra" id="sifra" class="form-control" placeholder="Шифра"
                               value="<?= $korisnik->getSifra() ?>">
                    </div>

                    <div class="form-group">
                        <label for="uloga">Улога</label>
                        <select name="uloga_id" id="uloga" class="form-control">
                            <option value="<?= $korisnik->getUloga()->getId() ?>">
                                <?= $korisnik->getUloga()->getNazivUloge() ?>
                            </option>

                            <?php
                            if (isset($uloge)) {
                                /** @var Uloga $uloga */
                                foreach ($uloge as $uloga) {
                                    if ($korisnik->getUloga()->getId() == $uloga->getId()) {
                                        continue;
                                    }
                                    ?>
                                    <option value="<?= $uloga->getId() ?>">
                                        <?= $uloga->getNazivUloge() ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>


                    <div class="float-right">
                        <button type="submit" class="btn btn-success">Потврди измене</button>
                        <button type="button" id="odustani" class="btn btn-danger">Одустани</button>
                    </div>


                </form>

                <?php
            }
            ?>

        </div>

        <div class="col-lg-3"></div>


    </div>


<?php

$content = ob_get_clean();


ob_start();

?>


    <script>
        const $odustani = $('#odustani');

        $(function () {
            $odustani.click(function () {
                location.href = '/admin/korisnici';
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => '',
    'headerTitle' => 'Измена података о кориснику',
    'js' => $js,
)));
