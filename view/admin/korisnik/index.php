<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-1"></div>

        <div class="col-lg-10">

            <p>
                <a href="/korisnik/insert" class="btn btn-outline-success">
                    Додајте корисника
                </a>
            </p>


            <?php
            if (empty($korisnici)) {
                ?>
                Тренутно нема корисника.
                <?php
            } else {
                ?>

                <table class="table table-responsive data-table">
                    <thead>
                    <tr>
                        <th>Име</th>
                        <th>Презиме</th>
                        <th>Имејл</th>
                        <th>Админ</th>
                        <th>Датум регистрације</th>
                        <th>Прикажи</th>
                        <th>Измени</th>
                        <th>Обриши</th>
                    </tr>
                    </thead>

                    <?php
                    foreach ($korisnici as $korisnik) {
                        /** @var Korisnik $korisnik */
                        ?>

                        <tr>
                            <td><?= $korisnik->getIme() ?></td>
                            <td><?= $korisnik->getPrezime() ?></td>
                            <td><?= $korisnik->getEmail() ?></td>
                            <td><?= ($korisnik->getUloga()->getRang() == 111) ? '<b class="text-danger">Администратор</b>' : ''; ?></td>
                            <td><?= Util::DateObjectToString($korisnik->getDatumRegistracije()) ?></td>


                            <td><a href="/korisnik/showCustomer/<?= $korisnik->getId() ?>"
                                   class="btn btn-outline-secondary" target="_blank">Прикажи </a></td>
                            <td><a href="/korisnik/adminEdit/<?= $korisnik->getId() ?>" class="btn btn-outline-primary">Измени</a>
                            </td>
                            <td><a href="/korisnik/delete/<?= $korisnik->getId() ?>"
                                   class="btn btn-danger">Обриши</a></td>
                        </tr>

                        <?php
                    }
                    ?>


                </table>

                <?php
            }// else kraj ( ako ima korisnika)
            ?>
        </div> <!-- col lg-6 kraj -->


        <div class="col-lg-1"></div>

    </div>


<?php

$content = ob_get_clean();

ob_start();


?>

    <script>
        $(function () {

        });
    </script>

<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => 'Приказ свих корисника',
    'headerTitle' => 'Корисници',
    'js' => $js,
)));
