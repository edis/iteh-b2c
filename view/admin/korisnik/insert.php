<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6">


            <form action="/korisnik/create/" method="POST">
                <div class="form-group">
                    <label for="ime">Име </label>
                    <input type="text" name="ime" id="ime" class="form-control" placeholder="Име "
                           value="" autocomplete="off" minlength="3" required>
                </div>

                <div class="form-group">
                    <label for="prezime">Презиме </label>
                    <input type="text" name="prezime" id="prezime" class="form-control" placeholder="Презиме "
                           value="" autocomplete="off" minlength="3" required>

                </div>

                <div class="form-group">
                    <label for="email">Имејл</label>
                    <input type="text" name="email" id="email" class="form-control" placeholder="Имејл"
                           value="" autocomplete="off" minlength="3" required>

                </div>

                <div class="form-group">
                    <label for="sifra">Шифра </label>
                    <input type="text"
                           name="sifra" id="sifra" class="form-control" placeholder="Шифра "
                           value="" autocomplete="off">
                </div>


                <div class="float-right">
                    <button type="submit" class="btn btn-success">Креирај корисника</button>
                    <button type="button" id="odustani" class="btn btn-danger">Одустани</button>
                </div>


            </form>

        </div>

        <div class="col-lg-3"></div>


    </div>


<?php

$content = ob_get_clean();


ob_start();

?>


    <script>
        const $odustani = $('#odustani');

        $(function () {
            $odustani.click(function () {
                location.href = '/admin/korisnici';
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => '',
    'headerTitle' => 'Унос података о кориснику',
    'js' => $js,
)));
