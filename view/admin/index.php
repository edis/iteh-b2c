<?php


ob_start();


?>

<div style="margin-bottom: 20px;">
    Пикажи графикон за
    <select name="godina" id="godina" class="form-control" style="width: 100px;display: inline-block">
        <?php
        $tekucaGodina = intval(date('Y'));

        for ($i = $tekucaGodina; $i >= $tekucaGodina - 5; $i--) {
            ?>
            <option value="<?= $i ?>" <?= (isset($godina) && $i == $godina) ? 'selected' : '' ?>><?= $i ?></option>
            <?php
        }
        ?>
    </select>
    годину
</div>


<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#menu1">Приход по месецима</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu2">Број поручених производа по категорији</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu3">Приход по производу</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu4">Број поруџбина по месецима</a>
    </li>

</ul>


<p></p>
<p></p>

<div class="row">
    <div class="col-md-10 offset-1">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active container" id="menu1">
                <div id="prihod-po-mesecima">

                </div>
            </div>

            <div class="tab-pane container" id="menu2">
                <div id="broj-porucenih-proizvoda-po-kategoriji">

                </div>
            </div>

            <div class="tab-pane container" id="menu3">
                <div id="prihod-po-proizvodu">

                </div>
            </div>


            <div class="tab-pane container" id="menu4">
                <div id="broj-porudzbina-po-mesecima">

                </div>
            </div>
        </div>
    </div>

</div>


<?php

$content = ob_get_clean();

ob_start();

?>

<script>

    const $godina = $('#godina');

    $(function () {

        $godina.on({
            change: function () {
                location.href = '/admin/index/' + $(this).val();
            }
        });

        var brojPorudzbinaPoMesecima = new FusionCharts({
            type: 'column2d',
            renderAt: 'broj-porudzbina-po-mesecima',
            width: '100%',
            height: '500',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "caption": "Број поруџбина по месецима за " + <?= $godina ?> +". годину",
                    "xAxisName": "Месец",
                    "yAxisName": "Број поруџбина",
                    "exportEnabled": "1",
                    "rotateValues": "0",
                    "decimals": "2",
                    "plotSpacePercent": "20",
                    "theme": "fint"
                },

                "data": <?= $brojPorudzbinaPoMesecimaData ?>
            }
        });
        brojPorudzbinaPoMesecima.render();

        var brojPorucenihProizvodaPoKategoriji = new FusionCharts({
            type: 'pie2d',
            renderAt: 'broj-porucenih-proizvoda-po-kategoriji',
            width: '100%',
            height: '500',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "caption": "Број поручених производа по категорији за " + <?= $godina ?> +". годину",
                    "xAxisName": "Категорија",
                    "yAxisName": "Број поручених производа",
                    "exportEnabled": "1",
                    "numberPrefix": "",
                    "numberSuffix": " комада",
                    "rotateValues": "0",
                    "thousandSeparator": ".",
                    "formatNumberScale": "0",
                    "showLegend": "1",
                    "legendPosition": "right",
                    "theme": "fint"
                },

                "data": <?= $brojPorucenihProizvodaPoKategoriji ?>
            }
        });
        brojPorucenihProizvodaPoKategoriji.render();

        var prihodPoMesecima = new FusionCharts({
            type: 'line',
            renderAt: 'prihod-po-mesecima',
            width: '100%',
            height: '500',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "caption": "Приход по месецима за " + <?= $godina ?> +". годину",
                    "xAxisName": "Месец",
                    "yAxisName": "Приход",
                    "exportEnabled": "1",
                    "numberPrefix": "",
                    "numberSuffix": " RSD",
                    "rotateValues": "0",
                    "thousandSeparator": ".",
                    "formatNumberScale": "0",
                    "paletteColors": "#81BB76",
                    "anchorRadius": "5",
                    "anchorHoverRadius": "8",
                    "theme": "fint"
                },

                "data": <?= $prihodPoMesecimaData ?>
            }
        });
        prihodPoMesecima.render();


        var prihodPoProizvodu = new FusionCharts({
            type: 'column2d',
            renderAt: 'prihod-po-proizvodu',
            width: '100%',
            height: '500',
            dataFormat: 'json',
            dataSource: {
                "chart": {
                    "caption": "Производи са највећим приходом за " + <?= $godina ?> +". годину",
                    "xAxisName": "Назив производа",
                    "yAxisName": "Приход",
                    "exportEnabled": "1",
                    "numberPrefix": "",
                    "numberSuffix": " RSD",
                    "rotateValues": "0",
                    "thousandSeparator": ".",
                    "formatNumberScale": "0",
                    "paletteColors": "#81BB76",
                    "anchorRadius": "5",
                    "anchorHoverRadius": "8",
                    "theme": "fint",
                    "plotSpacePercent": "1"
                },

                "data": <?= $prihodPoProizvodu ?>
            }
        });
        prihodPoProizvodu.render();
    });


</script>

<?php
$js = ob_get_clean();

echo render('template_admin.php', array(
    'content' => $content,
    'title' => 'Администрација',
    'headerTitle' => 'Администрација',
    'js' => $js,
));


?>
