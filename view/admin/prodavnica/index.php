<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-2"></div>

        <div class="col-lg-8">

            <p>
                <a href="/prodavnica/insert/" class="btn btn-outline-success">
                    Додајте продавницу
                </a>
            </p>


            <?php
            if (empty($prodavnice)) {
                ?>
                Тренутно нема продавница.
                <?php
            } else {
                ?>

                <table class="table table-responsive data-table">
                    <thead>
                    <tr>
                        <th>Назив продавнице</th>
                        <th>Адреса</th>
                        <th>Телефон</th>
                        <th>Прикажи</th>
                        <th>Измени</th>
                        <th>Обриши</th>
                    </tr>
                    </thead>

                    <?php
                    foreach ($prodavnice as $prodavnica) {
                        /** @var Prodavnica $prodavnica */
                        ?>

                        <tr>
                            <td><?= $prodavnica->getNaziv() ?></td>
                            <td><?= $prodavnica->getAdresa() ?></td>
                            <td><?= $prodavnica->getTelefon() ?></td>
                            <td><a href="/prodavnica/show/<?= $prodavnica->getId() ?>" class="btn btn-outline-secondary"
                                   target="_blank">Прикажи</a></td>
                            <td><a href="/prodavnica/edit/<?= $prodavnica->getId() ?>" class="btn btn-outline-primary">Измени</a>
                            </td>
                            <td><a href="/prodavnica/delete/<?= $prodavnica->getId() ?>"
                                   class="btn btn-danger">Обриши</a></td>
                        </tr>

                        <?php
                    }
                    ?>


                </table>

                <?php
            }// else kraj ( ako ima prodavnica)
            ?>
        </div> <!-- col lg-6 kraj -->

        <div class="col-lg-2"></div>


    </div>


<?php

$content = ob_get_clean();

ob_start();


?>

    <script>
        $(function () {

        });
    </script>

<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => 'Приказ свих продавница',
    'headerTitle' => 'Продавнице',
    'js' => $js,
)));
