<?php

ob_start();


?>


    <div class="row">

        <div class="col-lg-3"></div>

        <div class="col-lg-6">

            <?php
            if (empty($prodavnica)) {

            } else {
                // ako nije empty($prodavnica)
                /** @var Prodavnica $prodavnica */
                ?>

                <form action="/prodavnica/update/<?= $prodavnica->getId() ?>" method="POST">
                    <div class="form-group">
                        <label for="naziv">Назив продавнице</label>
                        <input type="text"
                               name="naziv" id="naziv" class="form-control" placeholder="Назив"
                               value="<?= $prodavnica->getNaziv() ?>">
                    </div>

                    <div class="form-group">
                        <label for="email">Имејл</label>
                        <input type="text"
                               name="email" id="email" class="form-control" placeholder="Имејл"
                               value="<?= $prodavnica->getEmail() ?>">
                    </div>

                    <div class="form-group">
                        <label for="telefon">Телефон</label>
                        <input type="text"
                               name="telefon" id="telefon" class="form-control" placeholder="Телефон"
                               value="<?= $prodavnica->getTelefon() ?>">
                    </div>


                    <div class="form-group">
                        <label for="adresa">Адреса</label>
                        <input type="text"
                               name="adresa" id="adresa" class="form-control" placeholder="Адреса"
                               value="<?= $prodavnica->getAdresa() ?>">
                    </div>


                    <div class="form-group">
                        <label for="radno_vreme">Радно време</label>
                        <input type="text"
                               name="radno_vreme" id="radno_vreme" class="form-control" placeholder="Радно време"
                               value="<?= $prodavnica->getRadnoVreme() ?>">
                    </div>

                    <div class="float-right">
                        <button type="submit" class="btn btn-success">Потврди измене</button>
                        <button type="button" id="odustani" class="btn btn-danger">Одустани</button>
                    </div>


                </form>

                <?php
            }
            ?>

        </div>

        <div class="col-lg-3"></div>


    </div>


<?php

$content = ob_get_clean();


ob_start();

?>


    <script>
        const $odustani = $('#odustani');

        $(function () {
            $odustani.click(function () {
                location.href = '/admin/prodavnica';
            });
        });
    </script>


<?php

$js = ob_get_clean();

echo render('template_admin.php', array_merge($data, array(
    'content' => $content,
    'title' => '',
    'headerTitle' => 'Измена података о продавници',
    'js' => $js,
)));
