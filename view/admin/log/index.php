<?php

ob_start();


?>

<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#menu1">EventLog</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu2">ErrorLog</a>
    </li>
</ul>


<div class="row">
    <div class="col-md-10 offset-1">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active container" id="menu1">
                <div id="eventLog">
                    <h1 style="text-align: center">Event log</h1>
                    <textarea style="width: 100%; height:500px" readonly><?= Log::Read(Log::$eventLog) ?></textarea>
                </div>
            </div>

            <div class="tab-pane container" id="menu2">
                <div id="errorLog">
                    <h1 style="text-align: center">Error log</h1>
                    <textarea style="width: 100%; height:500px" readonly><?= Log::Read(Log::$errorLog) ?></textarea>
                </div>
            </div>
        </div>
    </div>

</div>


<?php

$content = ob_get_clean();

echo render('template_admin.php', array(
    'content' => $content,
    'title' => 'Лог',
    'headerTitle' => 'Лог',

));


?>


