<?php

ob_start();

$title = 'Аутори сајта';
?>


    <div class="row">
        <div class="col-lg-2"></div>


        <div class="col-lg-8">

            <div class="card-deck">


                <?php

                if (isset($studenti) && !is_null($studenti) && !empty($studenti)) {
                    /** @var Autor $s */
                    foreach ($studenti as $s) {
                        ?>


                        <div class="card border-dark">
                            <img class="card-img-top"
                                 src="https://ece.nl/wp-content/uploads/person-dummy.jpg"
                                 width="200px;" height="300px;" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title"><?= $s->getIme() . ' ' . $s->getPrezime(); ?></h5>
                                <p class="card-text"><?= $s->getIndeks(); ?></p>
                            </div>
                        </div>

                        <?php
                    }
                } else {
                    ?>
                    Није могуће учитати ауторе сајта.
                    <?php
                }


                ?>

            </div>
        </div>

    </div>

    <div class="col-lg-2"></div>

    </div>


<?php

$content = ob_get_clean();

echo render('template.php', array_merge($data,
    [
        'content' => $content,
        'title' => $title,
        'headerTitle' => 'Аутори сајта',
        'headerDescription' => '',
    ]
));
