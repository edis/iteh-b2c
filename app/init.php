<?php

define('CONTROLLER_FOLDER', ROOT_URI . '/controller/');
define('ROOT_VIEW', ROOT_URI . '/view/');
define('APP_FOLDER', ROOT_URI . '/app/');

require_once APP_FOLDER . 'Autoloader.php';
require_once ROOT_URI . '/common/common_functions.php';
//require_once APP_FOLDER . 'Route.php';
//require_once APP_FOLDER . 'routes.php';
