<?php


class FrontController
{
    private static $defaultController = 'Index';
    private static $defaultMethod = 'index';

    private $uri = '';
    private $splitedUri = [];

    private $controller;
    private $method;
    private $params = [];


    public function __construct()
    {
        $this->uri = self::parseUri();
        $this->splitedUri = self::splitUri($this->uri, '/');


        $ctrl = (isset($this->splitedUri[0]) && !empty($this->splitedUri[0])) ? ucfirst(strtolower($this->splitedUri[0])) : self::$defaultController;
        if (!class_exists($ctrl . 'Controller')) {
            redirectTo('/404.php');
            exit();
        } else {
            $this->controller = $ctrl;
        }

        $ctrl .= 'Controller';
        $controllerObject = new $ctrl;

        $function = (isset($this->splitedUri[1]) && !empty($this->splitedUri[1])) ? strtolower($this->splitedUri[1]) : self::$defaultMethod;
        if (method_exists($controllerObject, $function . 'Action')) {
            $this->method = $function;
        } else {
            redirectTo('/404.php');
            exit();
        }


        if (count($this->splitedUri) >= 3) {
            $this->params = explode('/', $this->splitedUri[2]);
        }

    }

    private function parseUri(): string
    {
        $uri = $_SERVER['REQUEST_URI'];
        $uri = trim($uri, '/');
        return $uri;
    }

    private function splitUri(string $uri, string $delimiter = '/'): array
    {
        return explode($delimiter, $uri, 3);
    }

    public function run(): void
    {
//        if (
//            ($_SERVER['REQUEST_METHOD'] === "GET"
//                && Route::getRouteExists(strtolower($this->controller . '/' . $this->method)))
//            ||
//            ($_SERVER['REQUEST_METHOD'] === "POST"
//                && Route::postRouteExists(strtolower($this->controller . '/' . $this->method)))
//            ) {
//            $this->controller .= 'Controller';
//            $this->method .= 'Action';

        $this->controller .= 'Controller';
            call_user_func_array([new $this->controller, $this->method . 'Action'], $this->params);
//        } else {
//            redirectTo('/404.php');
//        }
    }
}