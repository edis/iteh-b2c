<?php

class Autoloader
{
    private $folders = [];

    public function __construct(array $folders = [])
    {
        $this->folders = $folders;
    }

    public function run()
    {
        if (empty($this->folders)) {
            $this->folders = array(
                'controller',
                'controller/api',
                'app',
                'model',
                'model/izvestaji',
                'common',
                'common/base',
                'factory',
                'factory/apiModelBuilder',
                'factory/dbModelBuilder',
                'factory/validator',
                'db',
                'db/dao',
                'db/dao/daoImpl',
                'util',
                'log',
            );
        }

        $function = 'self::autoloader';
        spl_autoload_register($function);
    }

    private function autoloader($className)
    {
        foreach ($this->folders as $folder) {
            $fileName = ROOT_URI . '/' . $folder . '/' . $className . '.php';

            if (file_exists($fileName)) {
                require_once $fileName;
                return;
            }
        }
    }
}