<?php

class Route
{
    private static $getRoutes = [];
    private static $postRoutes = [];

    public static function get(string $route, $controller): void
    {
        if (!array_key_exists($route, self::$getRoutes)) {
            self::$getRoutes[$route] = $controller;
        }
    }

    public static function post(string $route, $controller): void
    {
        if (!array_key_exists($route, self::$postRoutes)) {
            self::$postRoutes[$route] = $controller;
        }
    }

    public static function getRouteExists(string $route): bool
    {
        $route = trim($route, '/');
        if (array_key_exists($route, self::$getRoutes)) {
            $controllerGetRoute = trim(explode('@', self::$getRoutes[$route])[0], '/');
            $methodGetRoute = trim(explode('@', self::$getRoutes[$route])[1] . 'Action', '/');

            $controllerRealRoute = ucfirst(explode('/', $route)[0]) . 'Controller';
            $methodRealRoute = explode('/', $route)[1] . 'Action';

            if ($controllerGetRoute === $controllerRealRoute && $methodGetRoute === $methodRealRoute) {
                return true;
            }
            return false;
        }

        return false;

    }

    public static function postRouteExists(string $route): bool
    {
        $route = trim($route, '/');
        if (array_key_exists($route, self::$postRoutes)) {
            $controllerGetRoute = trim(explode('@', self::$postRoutes[$route])[0], '/');
            $methodGetRoute = trim(explode('@', self::$postRoutes[$route])[1] . 'Action', '/');

            $controllerRealRoute = ucfirst(explode('/', $route)[0]) . 'Controller';
            $methodRealRoute = explode('/', $route)[1] . 'Action';

            if ($controllerGetRoute === $controllerRealRoute && $methodGetRoute === $methodRealRoute) {
                return true;
            }
            return false;
        }

        return false;

    }
}