/*
SQLyog Community v12.3.2 (64 bit)
MySQL - 10.1.30-MariaDB : Database - iteh
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`iteh` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `iteh`;

/*Data for the table `autor` */


insert  into `autor`(`id`,`ime`,`prezime`,`indeks`) values (1,'Едис','Шарда','174/14'),(2,'Петар','Црнчевић','19/14'),(3,'Душан','Трескавица','1029/15');


/*Data for the table `proizvodjac` */

insert  into `proizvodjac`(`id`,`naziv`) values 

(1,'ASUS'),

(2,'HP'),

(3,'ACER'),

(4,'TOSHIBA'),

(5,'JETION'),

(6,'DELL'),

(7,'LENOVO'),

(8,'ANEEX');

/*Data for the table `uloga` */

insert  into `uloga`(`id`,`naziv_uloge`,`rang`) values 

(1,'регистрован корисник',1),
(2,'администратор',111);

/*Data for the table `kategorija` */

insert  into `kategorija`(`id`,`naziv`) values 

(1,'laptop'),

(2,'mis'),

(3,'punjac'),

(4,'baterija'),

(5,'tastatura');

/*Data for the table `komentar` */

/*Data for the table `korisnik` */

insert  into `korisnik`(`id`,`email`,`ime`,`prezime`,`sifra`,`uloga_id`,`datum_registracije`,`slika`) values 

(1,'dusan@yahoo.com','Душан','Трескавица','dusan',2,'2018-01-29 15:04:03',NULL),

(2,'edis@email.com','Едис','Шарда','edis',2,'2018-01-29 15:04:23',NULL),

(3,'petar@gmail.com','Petar','Crncevic','petar',2,'2018-01-29 22:55:28',NULL),

(4,'marko@gmail.com','Marko','Markovic','marko',1,'2018-02-13 19:25:55',''),

(5,'ana@gmail.com','Ana','Anic','ana',1,'2018-02-13 21:38:25','');




/*Data for the table `prodavnica` */

insert  into `prodavnica`(`id`,`email`,`adresa`,`naziv`,`radno_vreme`,`telefon`) values 

(1,'prodavnicaa@email.com','Цвијићева бр. 23','Продавница А','пон-суб: 8-22h, нед: 9-17h','011 7896 435'),

(2,'prodavnicanb@email.com','Јурија Гагарина бр. 122','Продавница Б','пон-пет: 8-22h, суб: 9-17h','011 1234 567'),

(3,'prodavnicav@email.com','Балканска бр. 54','Продавница В','пон-пет: 8-22h, суб: 9-17h','011 555 333'),

(4,'prodavnicag@email.com','Првомајска бр. 158','Продавница Г','пон-пет: 8-22h, суб: 9-17h','011 2648 456');

/*Data for the table `proizvod` */

insert  into `proizvod`(`id`,`naziv`,`boja`,`cena`,`kolicina_na_stanju`,`kategorija_id`,`proizvodjac_id`,`slika`,`specifikacija`) values 

(1,'ASUS 2245','siva',42000,32,1,1,'https://www.gigatron.rs/img/products/large/image5a1541381bc2a.png','Model procesora: Intel® Core™ i7 6700HQ...Dijagonala ekrana: 17.3\",HDD1: 128GB SSD,RAM memorija: 16GB'),

(2,'HP 250G6 ','crna',39000,79,1,2,'https://www.gigatron.rs/img/products/large/image5995089fbfd5d.png','Model procesora: Intel® Celeron® N3060...Dijagonala ekrana: 15.6\",HDD1: 500GB HDD,RAM memorija: 4GB'),

(3,'JETION ','crna',490,37,2,5,'https://www.gigatron.rs/img/products/large/image5847cb48b8e7b.png','Rezolucija: 1000dpi,Konekcija: USB,Boja: Crna'),

(4,'ACER Aspire ','crno-crvena',52000,9,1,3,'https://www.gigatron.rs/img/products/large/image5a6f392f78e8d.png','Model procesora: Intel® Pentium® N4200...      Dijagonala ekrana: 15.6\",      HDD1: 500GB HDD,    RAM memorija: 4GB'),

(5,'TOSHIBA Tecra ','crno-siva',77000,24,1,4,'https://www.gigatron.rs/img/products/large/image5a3955f30d4cf.png','Model procesora: Intel® Core™ i5 6200U...     Dijagonala ekrana: 14\",      HDD1: 128GB SSD,      RAM memorija: 4GB'),

(6,'DELL XPS ','zlatna',277000,22,1,6,'https://www.gigatron.rs/img/products/large/image5a5e9904a7ba2.png','Model procesora: Intel® Core™ i7 8550U...  Dijagonala ekrana: 13.3\",  HDD1: 256GB SSD,  RAM memorija: 8GB'),

(7,'LENOVO V310 ','crna',89000,22,1,7,'https://www.gigatron.rs/img/products/large/image593526cfe891f.png','Model procesora: Intel® Core™ i7 7500U...  Dijagonala ekrana: 15.6\", HDD1: 1TB HDD,  RAM memorija: 8GB'),

(8,'HP tastatura Conferencing - K8P74AA','crna',4390,38,5,2,'https://www.gigatron.rs/img/products/large/image5880873c286d8.png','Numerički deo: Da\r\nNisko-profilni tasteri: Da\r\nMultimedijalni tasteri: preko Fn tastera\r\nMIŠ: Ne'),

(9,'HP tastatura Classic Wired - WZ972AA','crna',2390,46,5,2,'https://www.gigatron.rs/img/products/large/image5885c7b80dd50.png','Numerički deo: Da\r\nMultimedijalni tasteri: 4 multimedijalna tastera\r\nVodootporna (manje količine tečnosti): Da\r\nMIŠ: Ne'),

(10,'DELL žična tastatura Multimedia KB216 ','crna',2090,56,5,6,'https://www.gigatron.rs/img/products/large/image58ca4eac9a99e.png','Slovni raspored: SRB (YU)\r\nNumerički deo: Da\r\nNisko-profilni tasteri: Da\r\nMultimedijalni tasteri: preko Fn tastera + 3'),

(11,'ANEEX optički miš - E-M234','crna',390,23,2,8,'https://www.gigatron.rs/img/products/large/image585ce0b1b0873.png','Tip senzora: Optički\r\nRezolucija: 800dpi\r\nDizajn: Simetričan (pogodan za...\r\nKonekcija: USB'),

(12,'JETION žični miš JT-DMS068','siva/crna',390,14,2,5,'https://www.gigatron.rs/img/products/large/image59ce22b6de29a.png','Tip senzora: Optički\r\nRezolucija: 1000dpi\r\nKonekcija: USB\r\nDužina kabla: 1.2m'),

(13,'LENOVO bežični miš N100','crna',1990,28,2,7,'https://www.gigatron.rs/img/products/large/image5a2a6c3650d1b.png','Tip senzora: Optički\r\nRezolucija: 1000dpi\r\nBroj tastera: 3\r\nDizajn: Simetričan'),

(14,'LENOVO optički miš (Crni) - 06P4069','crna',1490,32,2,7,'https://www.gigatron.rs/img/products/medium/image5a2a6881bd691.png','Tip senzora: Optički\r\nRezolucija: 400dpi\r\nBroj tastera: 3\r\nDizajn: Simetričan (pogodan za.'),

(15,'LENOVO bežični miš ThinkPad Precision','crna/siva',2990,20,2,7,'https://www.gigatron.rs/img/products/medium/image58624ab999d5b.png','Tip senzora: Optički\r\nFrekventni opseg: 2.4GHz\r\nRezolucija: 1200dpi\r\nČetvorosmerni točkić (scroll): Da'),

(16,'LENOVO gejmerski miš M600','crna/crvena',3990,26,2,7,'https://www.gigatron.rs/img/products/medium/image5a2a7fb8e73a9.png','Senzor: Optički\r\nTip senzora: Avago 9800\r\nRezolucija: 3200dpi\r\nPodesiva rezolucija: Da, u četiri nivoa'),

(17,'LENOVO gejmerski miš Y - GX30L02674','crna',3990,46,2,7,'https://www.gigatron.rs/img/products/medium/image59c1323251bba.png','Senzor: Optički\r\nRezolucija: 4000dpi\r\nPodesiva rezolucija: Da\r\nOdziv (polling rate): 1ms/1000Hz'),

(18,'ASUS gejmerski miš ROG STRIX IMPACT','crna',6990,33,2,1,'https://www.gigatron.rs/img/products/medium/image58d3b56814f59.png','Senzor: Optički\r\nRezolucija: 5000dpi\r\nPodesiva rezolucija: Da\r\nBrzina praćenja: 130 IPS'),

(19,'ASUS gejmerski miš CERBERUS ARCTIC','bela',3990,14,2,1,'https://www.gigatron.rs/img/products/medium/image57d6c3b4d9709.png','Senzor: Optički\r\nRezolucija: 2500dpi\r\nPodesiva rezolucija: Da, u četiri nivoa \r\nBroj tastera: 6'),

(20,'ASUS gejmerski optički miš CERBERUS','crna',3990,40,2,1,'https://www.gigatron.rs/img/products/medium/image5a327532086ea.png','Senzor: Optički\r\nRezolucija: 2500dpi\r\nPodesiva rezolucija: Da, u četiri nivoa \r\nBroj tastera: 5'),

(21,'HP 45W Smart AC Adapter - H6Y88AA','crna',3590,17,3,2,'https://www.gigatron.rs/img/products/medium/image557ff88745481.png','Strujni adapter jačine 45W. \r\nKonektor: 4.5mm + 7.4mm kabl konektor\r\nDimenzije: 96x49.5x22.4 mm (ŠxDxV) \r\nTežina: 201g\r\nBoja: crna'),

(22,'HP 65W Smart AC Adapter - H6Y89AA','crna',3790,27,3,2,'https://www.gigatron.rs/img/products/medium/image566944bfd16e4.png','Tip: Strujni adapter za laptop\r\nSnaga: 65W\r\nKonektori: 4.5 mm i 7.4 mm\r\nDimenzije: 47 x 28 x 106 mm\r\nTežina: 280 g'),

(23,'HP 90W Smart AC Adapter - H6Y90AA','crna',4990,10,3,2,'https://www.gigatron.rs/img/products/medium/image56694ad7e5305.png','Tip: Strujni adapter za laptop\r\nSnaga: 90W\r\nKonektori: 4.5 mm i 7.4 mm\r\n'),

(24,'LENOVO ThinkPad 65W AC Adapter - 0A36262','crna',4990,14,3,7,'https://www.gigatron.rs/img/products/medium/image59117f8aa1c17.png','Tip: Punjač za laptop\r\nSnaga: 65W\r\nBoja: Crna'),

(25,'JETION Universal AC adaptor 70W - JT-NPW018','crna',1790,30,3,5,'https://www.gigatron.rs/img/products/medium/image53e4c5e55a3fe.jpg','Tip : Eksterni strujni adapter\r\nSnaga: 70 W \r\nIzlazna struja: 12V/15V/16V/4.5A MAX; 18V/19V/20V/3.5A MAX; 24V/3A MAX \r\nRučno podešavanje voltaže.\r\n8 izmenjivih nastavaka za povezivanje sa uređajima.\r\nZaštita od preoterećenja, zaštita od kratkog spoja.'),

(26,'JETION Univerzalni punjač za laptop JT-NPW016','crna',1990,19,3,5,'https://www.gigatron.rs/img/products/medium/image568a30771f59e.png','Tip: Punjač za laptop\r\nKompatibilnost: Univerzalni\r\nSnaga: 90 W'),

(27,'Baterija za laptop Lenovo IdeaPad S10e S9e','crna',2499,13,4,7,'http://www.itrade.rs/media/catalog/product/cache/1/image/810x/040ec09b1e35df139433887a97daa66f/l/e/les10-3.jpg','Kompatibilnost:	LENOVO\r\nKapacitet	2600mAh'),

(29,'Baterija za laptop DELL Inspiron Mini 10 1010','crna',2499,10,4,6,'http://www.itrade.rs/media/catalog/product/cache/1/image/810x/040ec09b1e35df139433887a97daa66f/i/n/inspiron11z-1_3.jpg','Kompatibilnost:	DELL\r\nKapacitet	2200mAh'),

(30,'Baterija za laptop Asus N61 M50 ','crna',2799,15,4,1,'http://www.itrade.rs/media/catalog/product/cache/1/image/810x/040ec09b1e35df139433887a97daa66f/b/a/baterija-za-laptop-asus-n61-m50-111v-5200mah-59197-60397.png','Kompatibilnost:	ASUS\r\nKapacitet	: 5200mAh'),

(31,'Baterija za laptop Asus ASUS A42 A52 A62 B53 ','crna',2999,23,4,1,'http://www.itrade.rs/media/catalog/product/cache/1/image/810x/040ec09b1e35df139433887a97daa66f/l/a/laptop-battery-asus-a52-series_1.jpg','Kompatibilnost:	ASUS\r\nKapacitet	4400mAh'),

(32,'Baterija za laptop Dell Inspiron 1440 1500 15','crna',2999,25,4,6,'http://www.itrade.rs/media/catalog/product/cache/1/image/810x/040ec09b1e35df139433887a97daa66f/7/1/717rpaakhwl._sl1500__1.jpg','Kompatibilnost:	DELL\r\nKapacitet	4400mAh'),

(33,'Laptop LENOVO IdeaPad 320-15IAP','crna',37890,20,1,7,'https://www.gigatron.rs/img/products/medium/image5997bfb5cd0f4.png','RAM memorija	4GB\r\nDijagonala ekrana	15.6\"\r\nEkran	15.6\" HD LED backlight, anti-glare\r\nRezolucija: 1366 x 768 piksela\r\nEkran osetljiv na dodir	Ne\r\nBroj jezgara	4 (Quad Core)\r\nProcesor	Intel® Pentium® Quad Core Processor N4200\r\nBrzina: 1.10Ghz (Burst do 2.50GHz)\r\nKeš memorija:2MB\r\nČipset	Intel® Apollo Lake SOC\r\nGrafička karta	Integrisana Intel HD 505 sa deljenom sistemskom memorijom\r\nMemorija	4GB (4GB x 1) DDR3L 1600MHz SDRAM, 1 x SODIMM socket maksimalno 8GB SDRAM\r\nHDD1	Kapacitet: 500GB\r\nRotacija:'),

(34,'Laptop LENOVO IdeaPad 320-15IAP','siva',41990,25,1,7,'https://www.gigatron.rs/img/products/medium/image5a30383ea0fab.jpg','RAM memorija	4GB\r\nDijagonala ekrana	15.6\"\r\nEkran	15.6\" HD LED backlight, anti-glare\r\nRezolucija: 1366 x 768 piksela\r\nBroj jezgara	4 (Quad Core)\r\nProcesor	Intel® Pentium® Quad Core Processor N4200\r\nBrzina: 1.10Ghz (Burst do 2.50GHz)\r\nKeš memorija:2MB\r\nČipset	Intel® Apollo Lake SOC\r\nGrafička karta	Integrisana Intel HD 505 sa deljenom sistemskom memorijom\r\nMemorija	4GB (4GB x 1) DDR3L 1600MHz SDRAM, 1 x SODIMM socket maksimalno 8GB SDRAM\r\nHDD1	Kapacitet: 500GB\r\nRotacija: 5400 rpm\r\nTip HDD1	SATA HDD'),

(35,'Laptop LENOVO IdeaPad 320-15IKB','crna',73590,23,1,7,'https://www.gigatron.rs/img/products/medium/image59fb6015b29b3.jpg','RAM memorija	8GB\r\nDijagonala ekrana	15.6\"\r\nEkran	15.6\" FHD LED backlight, anti-glare\r\nRezolucija: 1920 x 1080 piksela\r\nEkran osetljiv na dodir	Ne\r\nBroj jezgara	2 (Dual Core)\r\nProcesor	Intel® Core™ i5 Dual Core Processor 7200U\r\nBrzina: 2.5GHz (Turbo do 3.1GHz)\r\nKeš memorija: 3MB\r\nČipset	Intel® Kaby Lake-U SOC\r\nGrafička karta	AMD Radeon 530 sa 2GB DDR3 sopstvene memorije\r\nMemorija	8GB (4GB + 4GB) DDR4 2133MHz SDRAM, (4GB na ploci i ne moze se menjati), 1 x SODIMM socket maksimalno 12GB SDRAM\r\nHDD1'),

(36,'Laptop ACER Aspire 3 A315-31-P9RE','crna',35990,31,1,3,'https://www.gigatron.rs/img/products/medium/image5a210b5d084b5.png','RAM memorija	4GB\r\nDijagonala ekrana	15.6\"\r\nEkran	15.6\" HD LED backlight, ComfyView\r\nRezolucija: 1366 x 768 piksela\r\nEkran osetljiv na dodir	Ne\r\nBroj jezgara	4 (Quad Core)\r\nProcesor	Intel® Pentium® Quad Core Processor N4200\r\nBrzina: 1.10Ghz (Burst do 2.50GHz)\r\nKeš memorija:2MB\r\nČipset	Intel® Apollo Lake SOC\r\nGrafička karta	Integrisana Intel HD 505 sa deljenom sistemskom memorijom\r\nMemorija	4GB (4GB x 1) DDR3L 1600MHz SDRAM, na ploci i ne moze se nadograditi\r\nHDD1	Kapacitet: 500GB\r\nRotacija: 5400 '),

(37,'Laptop ACER Aspire 3 A315-51-562S','crna',59990,35,1,3,'https://www.gigatron.rs/img/products/medium/image5a4b2c215af19.png','RAM memorija	4GB\r\nDijagonala ekrana	15.6\"\r\nEkran	15.6\" FHD LED backlight, ComfyView\r\nRezolucija: 1920 x 1080 piksela\r\nEkran osetljiv na dodir	Ne\r\nBroj jezgara	2 (Dual Core)\r\nProcesor	Intel® Core™ i5 Dual Core Processor 7200U\r\nBrzina: 2.5GHz (Turbo do 3.1GHz)\r\nKeš memorija: 3MB\r\nČipset	Intel® Kaby Lake-U SOC\r\nGrafička karta	Integrisana Intel HD 620 sa deljenom sistemskom memorijom\r\nMemorija	4GB (4GB x 1) DDR4 2133MHz SDRAM, 1 x SODIMM socket maksimalno 12GB SDRAM (4GB na ploci i ne moze se menjat'),

(38,'Laptop ACER Aspire 5 A515-51G-7349','crna',95990,41,1,3,'https://www.gigatron.rs/img/products/medium/image5a5779e580939.jpg','RAM memorija	8GB\r\nDijagonala ekrana	15.6\"\r\nEkran	15.6\" FHD LED backlight, ComfyView\r\nRezolucija: 1920 x 1080 piksela\r\nBroj jezgara	2 (Dual Core)\r\nProcesor	Intel® Core™ i7 Dual Core Processor 7500U\r\nBrzina: 2.7GHz (Turbo do 3.5GHz)\r\nKeš memorija: 4MB\r\nČipset	Intel® Kaby Lake-U SOC\r\nGrafička karta	nVidia GeForce 940MX sa 2GB DDR5 sopstvene memorije\r\nMemorija	8GB (4GB + 4GB) DDR4 2133MHz SDRAM, 1 x SODIMM socket maksimalno 20GB SDRAM (4GB na ploci i ne moze se menjati)\r\nHDD1	Kapacitet: 256GB\r\nRotac'),

(39,'Laptop TOSHIBA Satellite Pro A50-C-207','crna',123990,14,1,4,'https://www.gigatron.rs/img/products/medium/image59f529325b589.png','RAM memorija	8GB\r\nDijagonala ekrana	15.6\"\r\nEkran	15.6\" FHD IPS ekran velike svetline sa odnosom širina/visina 16:9 i LED pozadinskim osvetljenjem \r\nRezolucija: 1920 x 1080 piksela\r\nEkran osetljiv na dodir	Ne\r\nBroj jezgara	2 (Dual Core)\r\nProcesor	Intel® Core™ i7 Dual Core Processor 6500U\r\nBrzina: 2.5GHz (Turbo do 3.1GHz)\r\nKeš memorija: 4MB\r\nČipset	Intel® HM170\r\nGrafička karta	Integrisana Intel HD 520 sa deljenom sistemskom memorijom\r\nMemorija	8GB (8GB x 1) DDR3L 1600MHz SDRAM, 2 x SODIMM socket m'),

(40,'Laptop TOSHIBA Tecra A50-C-1ZR','crna',96990,16,1,4,'https://www.gigatron.rs/img/products/medium/image59f5256d4b6cb.png','RAM memorija	4GB\r\nDijagonala ekrana	15.6\"\r\nEkran	15.6\" HD LED backlit, anti-glare \r\nRezolucija : 1366 x 768 piksela\r\nEkran osetljiv na dodir	Ne\r\nBroj jezgara	2 (Dual Core)\r\nProcesor	Intel® Core™ i5 Dual Core Processor 6200U\r\nBrzina: 2.3GHz (Turbo do 2.8GHz)\r\nKeš memorija: 3MB\r\nČipset	Intel® HM170\r\nGrafička karta	Integrisana Intel HD 520 sa deljenom sistemskom memorijom\r\nMemorija	4GB (4GB x 1) DDR3L 1600MHz SDRAM, 2 x SODIMM socket maksimalno 16GB SDRAM\r\nHDD1	Kapacitet: 500GB\r\nRotacija: 7200 rpm\r'),

(41,'Laptop TOSHIBA Portege X30-D-10J','plava',199990,21,1,4,'https://www.gigatron.rs/img/products/medium/image59f53ef30918a.png','RAM memorija	8GB\r\nDijagonala ekrana	13.3\"\r\nEkran	13.3\" FHD LED backlit, anti-glare\r\nRezolucija : 1920 x 1080 piksela\r\nEkran osetljiv na dodir	Ne\r\nBroj jezgara	2 (Dual Core)\r\nProcesor	Intel® Core™ i5 Dual Core Processor 7200U\r\nBrzina: 2.5GHz (Turbo do 3.1GHz)\r\nKeš memorija: 3MB\r\nČipset	Intel® Kaby Lake-U SOC\r\nGrafička karta	Integrisana Intel HD 620 sa deljenom sistemskom memorijom\r\nMemorija	8GB (8GB x 1) DDR4 2133MHz SDRAM, 2 x SODIMM socket maksimalno 32GB SDRAM\r\nHDD1	Kapacitet: 256GB\r\nRotacija:');

/*Data for the table `korpa` */

insert  into `korpa`(`korisnik_id`,`proizvod_id`,`cena`,`datum`,`kolicina`) values 

(1,20,3990,'2018-02-12 20:37:54',1),

(4,23,4990,'2018-01-24 20:20:07',2);

/*Data for the table `porudzbina` */

insert  into `porudzbina`(`id`,`korisnik_id`,`kolicina`,`cena`,`datum`,`proizvod_id`) values 

(1,1,1,96990,'2018-02-12 20:38:13',40),

(3,4,1,2999,'2018-02-13 20:12:03',31),

(4,2,1,4390,'2018-01-24 20:12:42',8),

(5,2,2,3990,'2018-02-02 20:15:11',16),

(6,3,1,123990,'2018-01-07 20:21:19',39),

(8,1,1,4990,'2017-08-01 20:23:58',24),

(9,1,2,37890,'2017-11-17 20:25:19',33),

(10,3,1,1990,'2017-04-20 20:26:06',13),

(11,2,3,2090,'2017-08-28 20:29:49',10),

(12,2,1,1790,'2017-05-25 20:30:33',25),

(13,4,2,35990,'2017-03-23 20:31:28',36),

(14,1,1,59990,'2017-06-01 20:33:22',37),

(15,3,4,3990,'2017-05-01 20:35:17',16),

(16,1,2,390,'2016-09-01 20:49:01',11),

(17,1,2,4390,'2016-07-22 20:49:37',8),

(18,3,1,6990,'2016-04-22 20:50:36',18),

(19,2,1,42000,'2016-02-26 20:51:43',1),

(20,3,2,37890,'2016-05-01 21:17:36',33),

(21,2,1,73590,'2016-09-01 21:18:36',35),

(23,4,4,1990,'2016-04-01 21:19:50',13),

(24,2,1,39000,'2016-08-04 21:20:45',2),

(26,1,2,2799,'2016-09-22 21:23:21',30),

(27,3,1,390,'2015-07-24 21:29:04',11),

(28,4,2,2999,'2015-02-26 21:29:33',32),

(29,4,1,2499,'2015-10-13 21:30:13',29),

(30,2,1,123990,'2015-10-25 21:30:58',39),

(31,1,4,1490,'2015-07-19 21:32:15',14),

(32,1,2,6990,'2015-11-25 21:33:43',18),

(33,3,1,77000,'2015-05-04 21:34:18',5),

(34,2,3,6990,'2015-04-05 21:35:39',18),

(35,3,1,1490,'2014-07-13 21:46:44',14),

(36,3,2,3990,'2014-07-22 21:47:05',20),

(37,4,3,2499,'2014-01-28 21:48:04',29),

(38,1,1,41990,'2014-03-26 21:48:55',34),

(39,2,1,96990,'2014-02-27 02:49:52',40),

(41,4,1,52000,'2014-12-02 21:51:20',4),

(42,1,2,4990,'2014-03-22 21:52:14',24),

(43,5,4,6990,'2014-07-06 21:55:01',18);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
