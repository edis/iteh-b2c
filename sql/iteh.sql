  /*
  SQLyog Ultimate v11.11 (64 bit)
  MySQL - 5.5.5-10.1.28-MariaDB : Database - iteh
  *********************************************************************
  */


  /*!40101 SET NAMES utf8 */;

  /*!40101 SET SQL_MODE=''*/;

  /*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
  /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
  /*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
  CREATE DATABASE /*!32312 IF NOT EXISTS*/`iteh` /*!40100 DEFAULT CHARACTER SET utf8 */;

  USE `iteh`;

  /*Table structure for table `kategorija` */

  DROP TABLE IF EXISTS `kategorija`;

  CREATE TABLE `kategorija` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `naziv` varchar(45) NOT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


  /*Table structure for table `prodavnica` */

  DROP TABLE IF EXISTS `prodavnica`;

  CREATE TABLE `prodavnica` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `email` varchar(100) DEFAULT NULL,
    `adresa` varchar(150) DEFAULT NULL,
    `naziv` varchar(45) DEFAULT NULL,
    `radno_vreme` varchar(100) DEFAULT NULL,
    `telefon` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

  /*Table structure for table `proizvodjac` */

  DROP TABLE IF EXISTS `proizvodjac`;

  CREATE TABLE `proizvodjac` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `naziv` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


  /*Table structure for table `proizvod` */

  DROP TABLE IF EXISTS `proizvod`;

  CREATE TABLE `proizvod` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `naziv` varchar(45) DEFAULT NULL,
    `boja` varchar(45) DEFAULT NULL,
    `cena` double DEFAULT NULL,
    `kolicina_na_stanju` int(11) DEFAULT NULL,
    `kategorija_id` int(11) DEFAULT NULL,
    `proizvodjac_id` int(11) DEFAULT NULL,
    `slika` varchar(200) DEFAULT NULL,
    `specifikacija` varchar(500) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_1_idx` (`kategorija_id`),
    KEY `FK_2_idx` (`proizvodjac_id`),
    CONSTRAINT `FK_1` FOREIGN KEY (`kategorija_id`) REFERENCES `kategorija` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_2` FOREIGN KEY (`proizvodjac_id`) REFERENCES `proizvodjac` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


  /*Table structure for table `uloga` */

  DROP TABLE IF EXISTS `uloga`;

  CREATE TABLE `uloga` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `naziv_uloge` varchar(45) DEFAULT NULL,
    `rang` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

  /*Table structure for table `korisnik` */

  DROP TABLE IF EXISTS `korisnik`;

  CREATE TABLE `korisnik` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `email` varchar(100) DEFAULT NULL,
    `ime` varchar(55) DEFAULT NULL,
    `prezime` varchar(55) DEFAULT NULL,
    `sifra` varchar(75) DEFAULT NULL,
    `uloga_id` int(11) NOT NULL DEFAULT '1',
    `datum_registracije` timestamp NULL DEFAULT NULL,
    `slika` varchar(150) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `uloga_id` (`uloga_id`),
    CONSTRAINT `korisnik_ibfk_1` FOREIGN KEY (`uloga_id`) REFERENCES `uloga` (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `autor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(55) NULL,
  `prezime` VARCHAR(55) NULL,
  `indeks` VARCHAR(55) NULL,
  PRIMARY KEY (`id`));


  /*Table structure for table `porudzbina` */

  DROP TABLE IF EXISTS `porudzbina`;

  CREATE TABLE `porudzbina` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `korisnik_id` int(11) NOT NULL,
    `kolicina` int(5) DEFAULT NULL,
    `cena` double DEFAULT NULL,
    `datum` timestamp NULL DEFAULT NULL,
    `proizvod_id` int(11) NOT NULL,
    PRIMARY KEY (`id`,`korisnik_id`),
    KEY `korisnik_id` (`korisnik_id`),
    KEY `proizvod_id` (`proizvod_id`),
    CONSTRAINT `porudzbina_ibfk_1` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`),
    CONSTRAINT `porudzbina_ibfk_2` FOREIGN KEY (`proizvod_id`) REFERENCES `proizvod` (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  /*Data for the table `porudzbina` */


  /*Table structure for table `komentar` */

  DROP TABLE IF EXISTS `komentar`;

  CREATE TABLE `komentar` (
    `id` int(11) NOT NULL,
    `korisnik_id` int(11) NOT NULL,
    `proizvod_id` int(11) NOT NULL,
    `datum` timestamp NULL DEFAULT NULL,
    `komentar` varchar(600) DEFAULT NULL,
    PRIMARY KEY (`id`,`korisnik_id`,`proizvod_id`),
    KEY `FK_12_idx` (`proizvod_id`),
    CONSTRAINT `FK_11` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_12` FOREIGN KEY (`proizvod_id`) REFERENCES `proizvod` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;



  /*Table structure for table `korpa` */

  DROP TABLE IF EXISTS `korpa`;

  CREATE TABLE `korpa` (
    `korisnik_id` int(11) NOT NULL,
    `proizvod_id` int(11) NOT NULL,
    `cena` double DEFAULT NULL,
    `datum` timestamp NULL DEFAULT NULL,
    `kolicina` int(11) DEFAULT NULL,
    PRIMARY KEY (`korisnik_id`,`proizvod_id`),
    KEY `FK_22_idx` (`proizvod_id`),
    CONSTRAINT `FK_21` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_22` FOREIGN KEY (`proizvod_id`) REFERENCES `proizvod` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  /*Data for the table `korpa` */







  /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
  /*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
  /*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
