<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/26/2018
 * Time: 11:16 PM
 */

class Kategorija extends BaseModel
{
    private $id;
    private $naziv;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param mixed $naziv
     */
    public function setNaziv($naziv): void
    {
        $this->naziv = $naziv;
    }

    public static function getTableName(): string
    {
        return 'kategorija';
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'naziv' => $this->naziv,
        );
    }
}