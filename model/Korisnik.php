<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 28-Jan-18
 * Time: 02:05
 */

class Korisnik extends BaseModel
{
    private $id;
    private $ime;
    private $prezime;
    private $email;
    private $sifra;

    /**
     * @var DateTime
     */
    private $datumRegistracije;
    private $slika;

    /**
     * @var Uloga
     */
    private $uloga;

    public static function getTableName(): string
    {
        return strtolower(Korisnik::class);
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'ime' => $this->ime,
            'prezime' => $this->prezime,
            'email' => $this->email,
            'datum_registracije' => $this->datumRegistracije->format('d.m.Y.'),
            'uloga' => $this->uloga->getAsArray(),
        );
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIme()
    {
        return $this->ime;
    }

    /**
     * @param mixed $ime
     */
    public function setIme($ime)
    {
        $this->ime = $ime;
    }

    /**
     * @return mixed
     */
    public function getPrezime()
    {
        return $this->prezime;
    }

    /**
     * @param mixed $prezime
     */
    public function setPrezime($prezime)
    {
        $this->prezime = $prezime;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSifra()
    {
        return $this->sifra;
    }

    /**
     * @param mixed $sifra
     */
    public function setSifra($sifra)
    {
        $this->sifra = $sifra;
    }

    /**
     * @return mixed
     */
    public function getDatumRegistracije()
    {
        return $this->datumRegistracije;
    }

    /**
     * @param mixed $datumRegistracije
     */
    public function setDatumRegistracije($datumRegistracije): void
    {
        $this->datumRegistracije = $datumRegistracije;
    }

    /**
     * @return mixed
     */
    public function getSlika()
    {
        return $this->slika;
    }

    /**
     * @param mixed $slika
     */
    public function setSlika($slika): void
    {
        $this->slika = $slika;
    }

    /**
     * @return Uloga
     */
    public function getUloga()
    {
        return $this->uloga;
    }

    /**
     * @param mixed $uloga
     */
    public function setUloga($uloga): void
    {
        $this->uloga = $uloga;
    }

}