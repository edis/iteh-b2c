<?php

class Autor extends BaseModel
{
    private $id;
    private $ime;
    private $prezime;
    private $indeks;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIme()
    {
        return $this->ime;
    }

    /**
     * @param mixed $ime
     */
    public function setIme($ime): void
    {
        $this->ime = $ime;
    }

    /**
     * @return mixed
     */
    public function getPrezime()
    {
        return $this->prezime;
    }

    /**
     * @param mixed $prezime
     */
    public function setPrezime($prezime): void
    {
        $this->prezime = $prezime;
    }

    /**
     * @return mixed
     */
    public function getIndeks()
    {
        return $this->indeks;
    }

    /**
     * @param mixed $brojIndeksa
     */
    public function setIndeks($indeks): void
    {
        $this->indeks = $indeks;
    }

    public static function getTableName(): string
    {
        return strtolower(Autor::class);
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'ime' => $this->ime,
            'prezime' => $this->prezime,
            'indeks' => $this->indeks
        );
    }
}