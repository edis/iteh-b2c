<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/26/2018
 * Time: 11:14 PM
 */

class Proizvod extends BaseModel
{
    private $id;
    private $kategorija;
    private $naziv;
    private $kolicinaNaStanju;
    private $cena;
    private $boja;

    /**
     * @var Proizvodjac
     */
    private $proizvodjac;
    private $slika;
    private $specifikacija;

    /**
     * @return mixed
     */
    public function getSpecifikacija()
    {
        return $this->specifikacija;
    }

    /**
     * @param mixed $specifikacija
     */
    public function setSpecifikacija($specifikacija): void
    {
        $this->specifikacija = $specifikacija;
    }

    /**
     * @return mixed
     */
    public function getSlika()
    {
        return $this->slika;
    }

    /**
     * @param mixed $slika
     */
    public function setSlika($slika): void
    {
        $this->slika = $slika;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Kategorija
     */
    public function getKategorija()
    {
        return $this->kategorija;
    }

    /**
     * @param mixed $kategorija
     */
    public function setKategorija($kategorija): void
    {
        $this->kategorija = $kategorija;
    }

    /**
     * @return mixed
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param mixed $naziv
     */
    public function setNaziv($naziv): void
    {
        $this->naziv = $naziv;
    }

    /**
     * @return mixed
     */
    public function getKolicinaNaStanju()
    {
        return $this->kolicinaNaStanju;
    }

    /**
     * @param mixed $kolicinaNaStanju
     */
    public function setKolicinaNaStanju($kolicinaNaStanju): void
    {
        $this->kolicinaNaStanju = $kolicinaNaStanju;
    }

    /**
     * @return mixed
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * @param mixed $cena
     */
    public function setCena($cena): void
    {
        $this->cena = $cena;
    }

    /**
     * @return mixed
     */
    public function getBoja()
    {
        return $this->boja;
    }

    /**
     * @param mixed $boja
     */
    public function setBoja($boja): void
    {
        $this->boja = $boja;
    }

    /**
     * @return Proizvodjac
     */
    public function getProizvodjac()
    {
        return $this->proizvodjac;
    }

    /**
     * @param mixed $proizvodjac
     */
    public function setProizvodjac($proizvodjac): void
    {
        $this->proizvodjac = $proizvodjac;
    }


    public static function getTableName(): string
    {
        return 'proizvod';
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'naziv' => $this->naziv,
            'cena' => $this->cena,
            'kolicina_na_stanju' => $this->kolicinaNaStanju,
            'boja' => $this->boja,
            'slika' => $this->slika,
            'kategorija' => $this->kategorija->getAsArray(),
            'proizvodjac' => $this->proizvodjac->getAsArray(),
            'specifikacija' => $this->specifikacija,
        );
    }
}