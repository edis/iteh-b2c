<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 28-Jan-18
 * Time: 02:36
 */

class Uloga extends BaseModel
{
    private $id;
    private $nazivUloge;
    private $rang;


    public static function getTableName(): string
    {
        return strtolower(Uloga::class);
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'naziv' => $this->nazivUloge,
            'rang' => $this->rang,
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $uloga_id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNazivUloge()
    {
        return $this->nazivUloge;
    }

    /**
     * @param mixed $nazivUloge
     */
    public function setNazivUloge($nazivUloge): void
    {
        $this->nazivUloge = $nazivUloge;
    }

    /**
     * @return mixed
     */
    public function getRang()
    {
        return $this->rang;
    }

    /**
     * @param mixed $rang
     */
    public function setRang($rang): void
    {
        $this->rang = $rang;
    }


}