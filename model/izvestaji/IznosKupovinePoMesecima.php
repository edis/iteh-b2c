<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 10-Feb-18
 * Time: 21:18
 */

class IznosKupovinePoMesecima
{
    private $mesec;
    private $iznos;

    public static function getData()
    {
        $data = array();

        $rs = DBBroker::getInstance()->select("SELECT EXTRACT(MONTH FROM p.`datum`) AS mesec, SUM(p.`cena` * p.`kolicina`) AS iznos
                                                    FROM `porudzbina` p
                                                    JOIN `korisnik` k
                                                    ON k.`id` = p.`korisnik_id`
                                                    JOIN proizvod pr
                                                    ON pr.`id` = p.`proizvod_id`
                                                    WHERE k.`id` = " . ((isset($_COOKIE['admin_id'])) ? $_COOKIE['admin_id'] : $_COOKIE['id']) . " AND EXTRACT(YEAR FROM p.`datum`) = EXTRACT(YEAR FROM CURRENT_DATE)
                                                    GROUP BY EXTRACT(MONTH FROM p.`datum`)");

        foreach ($rs as $row) {
            array_push($data, array(
                'label' => Resolver::resolveMonth($row['mesec']),
                'value' => $row['iznos'],
            ));
        }

        return json_encode($data);
    }

    /**
     * @return mixed
     */
    public function getMesec()
    {
        return $this->mesec;
    }

    /**
     * @param mixed $mesec
     */
    public function setMesec($mesec): void
    {
        $this->mesec = $mesec;
    }

    /**
     * @return mixed
     */
    public function getIznos()
    {
        return $this->iznos;
    }

    /**
     * @param mixed $iznos
     */
    public function setIznos($iznos): void
    {
        $this->iznos = $iznos;
    }


}