<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 09-Feb-18
 * Time: 21:59
 */

class Resolver
{
    public static function resolveMonth(int $month)
    {
        switch ($month) {
            case 1:
                return 'јануар';
            case 2:
                return 'фебруар';
            case 3:
                return 'март';
            case 4:
                return 'април';
            case 5:
                return 'мај';
            case 6:
                return 'јун';
            case 7:
                return 'јул';
            case 8:
                return 'август';
            case 9:
                return 'септембар';
            case 10:
                return 'октобар';
            case 11:
                return 'новембар';
            case 12:
                return 'децембар';
            default:
                return 'непознат месец';
        }
    }
}