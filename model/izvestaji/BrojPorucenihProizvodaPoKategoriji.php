<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 09-Feb-18
 * Time: 23:11
 */

class BrojPorucenihProizvodaPoKategoriji
{
    private $kategorija;
    private $brojPorucenihProizvoda;


    public static function getData(int $godina, $userID = false)
    {
        $data = array();

        $where = "WHERE EXTRACT(YEAR FROM datum) = $godina";

        if (!empty($userID)) {
            $where .= " AND kor.`id` = " . $userID;
        }

        $rs = DBBroker::getInstance()->select("SELECT k.`naziv` AS kategorija, COUNT(*) AS broj_porucenih_proizvoda_za_kategoriju
                                                FROM porudzbina p
                                                JOIN proizvod pr
                                                ON p.`proizvod_id` = pr.`id`
                                                JOIN kategorija k
                                                ON k.`id` = pr.`kategorija_id`
                                                JOIN `korisnik` kor
                                                ON p.`korisnik_id` = kor.`id` " . $where . "
                                                GROUP BY k.`naziv`");

        foreach ($rs as $row) {
            $obj = new BrojPorucenihProizvodaPoKategoriji();
            $obj->setBrojPorucenihProizvoda($row['broj_porucenih_proizvoda_za_kategoriju']);
            $obj->setKategorija($row['kategorija']);

            array_push($data, array(
                'label' => $obj->getKategorija(),
                'value' => $obj->getBrojPorucenihProizvoda(),
            ));
        }

        return json_encode($data);
    }

    /**
     * @return mixed
     */
    public function getKategorija()
    {
        return $this->kategorija;
    }

    /**
     * @param mixed $kategorija
     */
    public function setKategorija($kategorija): void
    {
        $this->kategorija = $kategorija;
    }

    /**
     * @return mixed
     */
    public function getBrojPorucenihProizvoda()
    {
        return $this->brojPorucenihProizvoda;
    }

    /**
     * @param mixed $brojPorucenihProizvoda
     */
    public function setBrojPorucenihProizvoda($brojPorucenihProizvoda): void
    {
        $this->brojPorucenihProizvoda = $brojPorucenihProizvoda;
    }


}