<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 09-Feb-18
 * Time: 21:59
 */

class PrihodPoMesecima
{
    private $mesec;
    private $prihod;

    public static function getData(int $godina)
    {
        $rs = DBBroker::getInstance()->select("SELECT EXTRACT(MONTH FROM datum) mesec, COUNT(*) broj_prodatih_proizvoda, SUM(cena * kolicina) prihod FROM porudzbina p WHERE EXTRACT(YEAR from datum) = $godina GROUP BY EXTRACT(MONTH FROM datum)");

        $data = array();

        foreach ($rs as $row) {
            $prihodPoMesecu = new PrihodPoMesecima();
            $prihodPoMesecu->setMesec($row['mesec']);
            $prihodPoMesecu->setPrihod($row['prihod']);

            array_push($data, array(
                'value' => $prihodPoMesecu->getPrihod(),
                'label' => Resolver::resolveMonth($prihodPoMesecu->getMesec()),
            ));
        }

        return json_encode($data);
    }

    /**
     * @return mixed
     */
    public function getMesec()
    {
        return $this->mesec;
    }

    /**
     * @param mixed $mesec
     */
    public function setMesec($mesec): void
    {
        $this->mesec = $mesec;
    }

    /**
     * @return mixed
     */
    public function getPrihod()
    {
        return $this->prihod;
    }

    /**
     * @param mixed $prihod
     */
    public function setPrihod($prihod): void
    {
        $this->prihod = $prihod;
    }


}