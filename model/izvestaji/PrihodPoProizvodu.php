<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 10-Feb-18
 * Time: 14:51
 */

class PrihodPoProizvodu
{
    private $proizvod;
    private $prihod;

    public static function getData(int $godina)
    {
        $data = array();

        $rs = DBBroker::getInstance()->select("SELECT p.`naziv` AS naziv, SUM(po.`cena` * po.`kolicina`) AS prihod
                                                    FROM `porudzbina` po
                                                    JOIN proizvod p 
                                                    ON p.`id` = po.`proizvod_id`
                                                    WHERE EXTRACT(YEAR FROM datum) = $godina
                                                    GROUP BY p.`naziv`
                                                    ORDER BY SUM(po.`cena` * po.`kolicina`) DESC
                                                    LIMIT 0, 5");

        foreach ($rs as $row) {
            $ppp = new PrihodPoProizvodu();
            $ppp->setPrihod($row['prihod']);
            $ppp->setProizvod($row['naziv']);
            array_push($data, array(
                'label' => $ppp->getProizvod(),
                'value' => $ppp->getPrihod(),
            ));
        }

        return json_encode($data);
    }

    /**
     * @return mixed
     */
    public function getProizvod()
    {
        return $this->proizvod;
    }

    /**
     * @param mixed $proizvod
     */
    public function setProizvod($proizvod): void
    {
        $this->proizvod = $proizvod;
    }

    /**
     * @return mixed
     */
    public function getPrihod()
    {
        return $this->prihod;
    }

    /**
     * @param mixed $prihod
     */
    public function setPrihod($prihod): void
    {
        $this->prihod = $prihod;
    }


}