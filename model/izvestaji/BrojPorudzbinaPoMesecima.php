<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 09-Feb-18
 * Time: 18:09
 */

class BrojPorudzbinaPoMesecima
{
    private $mesec;
    private $brojPorudzbina;


    public static function getData(int $godina)
    {
        $data = array();

        $rs = DBBroker::getInstance()->select("SELECT EXTRACT(MONTH FROM datum) AS mesec, COUNT(*) AS broj_porudzbina FROM `porudzbina` WHERE EXTRACT(YEAR FROM datum) = " . $godina . "  GROUP BY EXTRACT(MONTH FROM datum)");

        foreach ($rs as $row) {
            $stat = new BrojPorudzbinaPoMesecima();
            $stat->setBrojPorudzbina($row['broj_porudzbina']);
            $stat->setMesec($row['mesec']);
            array_push($data, array(
                'value' => $stat->getBrojPorudzbina(),
                'label' => Resolver::resolveMonth($stat->getMesec()),
            ));
        }

        return json_encode($data);
    }

    /**
     * @return mixed
     */
    public function getBrojPorudzbina()
    {
        return $this->brojPorudzbina;
    }

    /**
     * @param mixed $brojPorudzbina
     */
    public function setBrojPorudzbina($brojPorudzbina): void
    {
        $this->brojPorudzbina = $brojPorudzbina;
    }

    /**
     * @return mixed
     */
    public function getMesec()
    {
        return $this->mesec;
    }

    /**
     * @param mixed $mesec
     */
    public function setMesec($mesec): void
    {
        $this->mesec = $mesec;
    }


}