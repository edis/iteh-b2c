<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 30-Jan-18
 * Time: 22:29
 */

class Porudzbina extends BaseModel
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var Korisnik
     */
    private $korisnik;

    /**
     * @var int
     */
    private $kolicina;

    /**
     * @var double
     */
    private $cena;

    /**
     * @var DateTime
     */
    private $datum;

    /**
     * @var Proizvod
     */
    private $proizvod;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Korisnik
     */
    public function getKorisnik(): Korisnik
    {
        return $this->korisnik;
    }

    /**
     * @param Korisnik $korisnik
     */
    public function setKorisnik(Korisnik $korisnik): void
    {
        $this->korisnik = $korisnik;
    }

    /**
     * @return int
     */
    public function getKolicina(): int
    {
        return $this->kolicina;
    }

    /**
     * @param int $kolicina
     */
    public function setKolicina(int $kolicina): void
    {
        $this->kolicina = $kolicina;
    }

    /**
     * @return float
     */
    public function getCena(): float
    {
        return $this->cena;
    }

    /**
     * @param double $cena
     */
    public function setCena($cena): void
    {
        $this->cena = $cena;
    }

    /**
     * @return DateTime
     */
    public function getDatum(): DateTime
    {
        return $this->datum;
    }

    /**
     * @param DateTime $datum
     */
    public function setDatum(DateTime $datum): void
    {
        $this->datum = $datum;
    }

    /**
     * @return Proizvod
     */
    public function getProizvod(): Proizvod
    {
        return $this->proizvod;
    }

    /**
     * @param Proizvod $proizvod
     */
    public function setProizvod(Proizvod $proizvod): void
    {
        $this->proizvod = $proizvod;
    }

    public static function getTableName(): string
    {
        return strtolower(Porudzbina::class);
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'korisnik' => $this->korisnik->getAsArray(),
            'proizvod' => $this->proizvod->getAsArray(),
            'cena' => $this->cena,
            'datum' => $this->datum->format('d.m.Y.'),
            'kolicina' => $this->kolicina,
        );
    }
}