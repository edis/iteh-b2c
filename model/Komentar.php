<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/29/2018
 * Time: 10:32 PM
 */

class Komentar extends BaseModel
{
    private $id;

    /**
     * @var Korisnik
     */
    private $korisnik;


    /**
     * @var Proizvod
     */
    private $proizvod;

    /**
     * @var DateTime
     */
    private $datumIVreme;
    private $komentar;


    public function getKomentarSaSmajlijima()
    {
        return str_replace(':)', '<img src="https://cdn.iconscout.com/public/images/icon/premium/png-256/happy-emoticons-smiley-man-smile-face-emoji-33ff964d4520ce4c-256x256.png" width="20" height="20" />', $this->komentar);
    }

    /**
     * @return mixed
     */
    public static function getTableName(): string
    {
        return strtolower(Komentar::class);
    }

    /**
     * @return Korisnik
     */
    public function getKorisnik()
    {
        return $this->korisnik;
    }

    /**
     * @param mixed $korisnik
     */
    public function setKorisnik($korisnik): void
    {
        $this->korisnik = $korisnik;
    }

    /**
     * @return Proizvod
     */
    public function getProizvod()
    {
        return $this->proizvod;
    }

    /**
     * @param mixed $proizvod
     */
    public function setProizvod($proizvod): void
    {
        $this->proizvod = $proizvod;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKomentar()
    {
        return $this->komentar;
    }

    /**
     * @param mixed $komentar
     */
    public function setKomentar($komentar): void
    {
        $this->komentar = $komentar;
    }

    /**
     * @return mixed
     */
    public function getDatumIVreme()
    {
        return $this->datumIVreme;
    }

    /**
     * @param mixed $datumIVreme
     */
    public function setDatumIVreme($datumIVreme): void
    {
        $this->datumIVreme = $datumIVreme;
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'korisnik' => $this->korisnik->getAsArray(),
            'proizvod' => $this->proizvod->getAsArray(),
            'datum' => $this->datumIVreme->format('d.m.Y.'),
            'komentar' => $this->komentar,
        );
    }
}