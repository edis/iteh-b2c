<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 29-Jan-18
 * Time: 20:05
 */

/**
 * Class Korpa
 */
class Korpa extends BaseModel
{

    /**
     * @var Korisnik $korisnik
     */
    private $korisnik;

    /**
     * @var Proizvod $proizvod
     */
    private $proizvod;

    /**
     * @var double $cena
     */
    private $cena;

    /**
     * @var DateTime $datum
     */
    private $datum;

    /**
     * @var int $kolicina
     */
    private $kolicina;

    /**
     * @return Korisnik
     */
    public function getKorisnik()
    {
        return $this->korisnik;
    }

    /**
     * @param Korisnik $korisnik
     */
    public function setKorisnik($korisnik): void
    {
        $this->korisnik = $korisnik;
    }

    /**
     * @return Proizvod
     */
    public function getProizvod()
    {
        return $this->proizvod;
    }

    /**
     * @param Proizvod $proizvod
     */
    public function setProizvod($proizvod): void
    {
        $this->proizvod = $proizvod;
    }

    /**
     * @return double
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * @param double $cena
     */
    public function setCena($cena): void
    {
        $this->cena = $cena;
    }

    /**
     * @return DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param DateTime $datum
     */
    public function setDatum($datum): void
    {
        $this->datum = $datum;
    }

    /**
     * @return int
     */
    public function getKolicina()
    {
        return $this->kolicina;
    }

    /**
     * @param int $kolicina
     */
    public function setKolicina($kolicina): void
    {
        $this->kolicina = $kolicina;
    }


    public static function getTableName(): string
    {
        return strtolower(Korpa::class);
    }

    public function getAsArray(): array
    {
        return array(
            'korisnik' => $this->korisnik->getAsArray(),
            'proizvod' => $this->proizvod->getAsArray(),
            'cena' => $this->cena,
            'datum' => $this->datum->format('d.m.Y.'),
            'kolicina' => $this->kolicina,
        );
    }
}