<?php

class GitUser
{
    private $id;
    private $userName;
    private $name;
    private $avatarUrl;
    private $reposApiUrl;
    private $email;
    private $followers;
    private $following;
    private $numberOfRepositories;
    private $bio;


    public static function getAuthQueryString(): ?string
    {
        $fileUrl = ROOT_URI . '/auth/git-auth.json';
        if (file_exists($fileUrl)) {
            $contentJson = file_get_contents($fileUrl);

            if ($contentJson === false) {
                return null;
            }

            $contentJson = json_decode($contentJson);

            $cId = $contentJson->cid;
            $csec = $contentJson->csec;

            return 'client_id=' . strip_tags($cId) . '&client_secret=' . strip_tags($csec);
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }

    /**
     * @param mixed $avatarUrl
     */
    public function setAvatarUrl($avatarUrl): void
    {
        $this->avatarUrl = $avatarUrl;
    }

    /**
     * @return mixed
     */
    public function getReposApiUrl()
    {
        return $this->reposApiUrl;
    }

    /**
     * @param mixed $reposApiUrl
     */
    public function setReposApiUrl($reposApiUrl): void
    {
        $this->reposApiUrl = $reposApiUrl;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @param mixed $followers
     */
    public function setFollowers($followers): void
    {
        $this->followers = $followers;
    }

    /**
     * @return mixed
     */
    public function getFollowing()
    {
        return $this->following;
    }

    /**
     * @param mixed $following
     */
    public function setFollowing($following): void
    {
        $this->following = $following;
    }

    /**
     * @return mixed
     */
    public function getNumberOfRepositories()
    {
        return $this->numberOfRepositories;
    }

    /**
     * @param mixed $numberOfRepositories
     */
    public function setNumberOfRepositories($numberOfRepositories): void
    {
        $this->numberOfRepositories = $numberOfRepositories;
    }

    /**
     * @return mixed
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * @param mixed $bio
     */
    public function setBio($bio): void
    {
        $this->bio = $bio;
    }


}