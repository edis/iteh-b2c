<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/29/2018
 * Time: 2:54 PM
 */

class Proizvodjac extends BaseModel
{

    private $id;
    private $naziv;

    /**
     * @return mixed
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param mixed $naziv
     */
    public function setNaziv($naziv): void
    {
        $this->naziv = $naziv;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    public static function getTableName(): string
    {
        return 'proizvodjac';
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'naziv' => $this->naziv,
        );
    }

}