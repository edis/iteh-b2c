<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 27-Jan-18
 * Time: 14:48
 */

class Prodavnica extends BaseModel
{

    private $id;
    private $naziv;
    private $email;
    private $adresa;
    private $telefon;
    private $radnoVreme;

    public static function getTableName(): string
    {
        return strtolower(Prodavnica::class);
    }

    public function getAsArray(): array
    {
        return array(
            'id' => $this->id,
            'naziv' => $this->naziv,
            'email' => $this->email,
            'adresa' => $this->adresa,
            'telefon' => $this->telefon,
            'radno_vreme' => $this->radnoVreme,
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param mixed $naziv
     */
    public function setNaziv($naziv): void
    {
        $this->naziv = $naziv;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAdresa()
    {
        return $this->adresa;
    }

    /**
     * @param mixed $adresa
     */
    public function setAdresa($adresa): void
    {
        $this->adresa = $adresa;
    }

    /**
     * @return mixed
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * @param mixed $telefon
     */
    public function setTelefon($telefon): void
    {
        $this->telefon = $telefon;
    }

    /**
     * @return mixed
     */
    public function getRadnoVreme()
    {
        return $this->radnoVreme;
    }

    /**
     * @param mixed $radnoVreme
     */
    public function setRadnoVreme($radnoVreme): void
    {
        $this->radnoVreme = $radnoVreme;
    }


}