<?php

class DatabaseFactory
{
    public function createProizvodDbBuilder(): ProizvodDbBuilder
    {
        return new ProizvodDbBuilder();
    }

    public function createKategorijaDbBuilder(): KategorijaDbBuilder
    {
        return new KategorijaDbBuilder();
    }

    public function createKorisnikDbBuilder(): KorisnikDbBuilder
    {
        return new KorisnikDbBuilder();
    }

    public function createProdavnicaDbBuilder(): ProdavnicaDbBuilder
    {
        return new ProdavnicaDbBuilder();
    }

    public function createProizvodjacDbBuilder(): ProizvodjacDBBuilder
    {
        return new ProizvodjacDBBuilder();
    }

    public function createKomentarDbBuilder(): KomentarDbBuilder
    {
        return new KomentarDbBuilder();
    }

    public function createKorpaDbBuilder(): KorpaDbBuilder
    {
        return new KorpaDbBuilder();
    }

    public function createUlogaDbBuilder(): UlogaDbBuilder
    {
        return new UlogaDbBuilder();
    }

    public function createPorudzbinaDbBuilder(): PorudzbinaDbBuilder
    {
        return new PorudzbinaDbBuilder();
    }

    public function createAdminDbBuilder(): AdminDbBuilder
    {
        return new AdminDbBuilder();
    }

    public function createAutorDbBuilder(): AutoriDbBuilder
    {
        return new AutoriDbBuilder();
    }
}