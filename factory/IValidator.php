<?php

interface IValidator
{
    public function validate(?BaseModel $model): bool;
}