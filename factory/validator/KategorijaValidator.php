<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 01-Feb-18
 * Time: 23:28
 */

class KategorijaValidator implements IValidator
{

    public function validate(?BaseModel $model): bool
    {
        if ($model instanceof Kategorija) {
            if (empty($model->getNaziv()) || mb_strlen($model->getNaziv(), 'UTF-8') < 3) {
                return false;
            }
            return true;
        }
        return false;
    }
}