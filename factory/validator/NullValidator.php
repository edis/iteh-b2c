<?php

class NullValidator implements IValidator
{
    public function validate(?BaseModel $model): bool
    {
        return false;
    }
}