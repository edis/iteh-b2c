<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 30-Jan-18
 * Time: 14:44
 */

class KorisnikValidator
{

    public static function validateProfil($ime, $prezime, $email)
    {
        $filteredVars = array();

        $ime = filterVar($ime);
        $prezime = filterVar($prezime);
        $email = filterVar($email);

        if (empty($ime)) {
            return 'Нисте унели име.';
        }

        if (empty($prezime)) {
            return 'Нисте унели презиме.';
        }

        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        if (empty($email)) {
            return 'Имејл адреса није валидна.';
        }

        if (mb_strlen($email, 'UTF-8') < 10) {
            return 'Имејл адреса не сме имати мање од 10 карактера.';
        }

        $filteredVars['email'] = $email;

        return $filteredVars;
    }

    public static function validateSifra($sifra, $sifra1)
    {
        $sifra = filterVar($sifra);
        $sifra1 = filterVar($sifra1);

        if (empty($sifra)) {
            return 'Морате унети шифру';
        }
        if (empty($sifra1)) {
            return 'Морате унети шифру за потврду';
        }
        if (mb_strlen($sifra, 'UTF-8') < 3) {
            return 'Шифра не сме имати мање од 3 карактера.';
        }

        if ($sifra != $sifra1) {
            return 'Шифре се не поклапају. Покушајте поново.';
        }

        return 'valid';
    }

}