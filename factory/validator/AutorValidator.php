4<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 2/7/2018
 * Time: 11:10 PM
 */

class AutorValidator implements IValidator
{

    public function validate(?BaseModel $model): bool
    {
        if ($model instanceof Autor) {
            if (empty($model->getIme()) || empty($model->getPrezime()) || empty($model->getIndeks())) {
                return false;
            }
            if (mb_strlen($model->getIme()) < 3 || mb_strlen($model->getPrezime()) < 3 || mb_strlen($model->getIndeks()) < 4) {
                return false;
            }
            return true;
        }
        return false;

    }
}