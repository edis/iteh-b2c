<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/28/2018
 * Time: 5:52 PM
 */

class ProizvodValidator implements IValidator
{
    public function validate(?BaseModel $model): bool
    {
        if ($model instanceof Proizvod) {
            if (empty($model->getNaziv()) ||
                empty($model->getBoja()) ||
                empty($model->getCena()) ||
                empty($model->getProizvodjac()) ||
                empty($model->getKategorija()) ||
                empty($model->getSlika()) ||
                empty($model->getSpecifikacija())
            ) {
                return false;
            }

            if (mb_strlen($model->getNaziv(), 'UTF-8') < 3 ||
                mb_strlen($model->getSlika()) < 3) {
                return false;
            }

            return true;
        }
        return false;
    }
}
