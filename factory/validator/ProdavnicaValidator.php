<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 28-Jan-18
 * Time: 15:06
 */

class ProdavnicaValidator implements IValidator
{
    public function validate(?BaseModel $model): bool
    {
        if ($model instanceof Prodavnica) {
            if (empty($model->getNaziv()) ||
                empty($model->getEmail()) ||
                empty($model->getAdresa())) {
                return false;
            }

            if (mb_strlen($model->getNaziv(), 'UTF-8') < 3 ||
                mb_strlen($model->getEmail(), 'UTF-8') < 10 ||
                mb_strlen($model->getAdresa()) < 5) {
                return false;
            }

            return true;
        }
        return false;
    }
}