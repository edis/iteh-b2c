<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 29-Jan-18
 * Time: 21:44
 */

class RegistracijaValidator
{


    public static function validate(string $ime, string $prezime, string $email, string $sifra)
    {
        $filteredVars = array();

        $ime = filterVar($ime);
        $prezime = filterVar($prezime);
        $email = filterVar($email);
        $sifra = filterVar($sifra);

        if (empty($ime)) {
            return 'Нисте унели име.';
        }

        if (empty($prezime)) {
            return 'Нисте унели презиме.';
        }

        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        if (empty($email)) {
            return 'Имејл адреса није валидна.';
        }

        if (empty($sifra)) {
            return 'Морате унети шифру';
        }

        if (mb_strlen($email, 'UTF-8') < 10) {
            return 'Имејл адреса не сме имати мање од 10 карактера.';
        }

        if (mb_strlen($sifra, 'UTF-8') < 3) {
            return 'Шифра не сме имати мање од 3 карактера.';
        }

        $filteredVars['email'] = $email;

        return $filteredVars;

    }
}