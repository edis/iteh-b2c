<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 29-Jan-18
 * Time: 00:44
 */

abstract class PrijavaValidator
{
    public static function validate(string $email, string $sifra)
    {
        $filteredVars = array();

        $email = filterVar($email);
        $sifra = filterVar($sifra);

        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        if (empty($email)) {
            return 'Имејл адреса није валидна.';
        }

        if (empty($sifra)) {
            return 'Морате унети шифру';
        }

        if (mb_strlen($email, 'UTF-8') < 10) {
            return 'Имејл адреса не сме имати мање од 10 карактера.';
        }

        if (mb_strlen($sifra, 'UTF-8') < 3) {
            return 'Шифра не сме имати мање од 3 карактера.';
        }

        $filteredVars['email'] = $email;
        $filteredVars['sifra'] = $sifra;

        return $filteredVars;
    }
}