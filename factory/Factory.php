<?php

abstract class Factory
{

    public static function createDatabaseFactory(): DatabaseFactory
    {
        return new DatabaseFactory();
    }

    public static function createApiFactory(): ApiFactory
    {
        return new ApiFactory();
    }

    public static function createValidator(string $validatorName): ?IValidator
    {
        $name = ucfirst(strtolower($validatorName)) . 'Validator';

        if (file_exists(ROOT_URI . '/factory/validator/' . $name . '.php')) {
            return new $name;
        }
        return new NullValidator();
    }

}