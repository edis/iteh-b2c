<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 29-Jan-18
 * Time: 20:11
 */

class KorpaDbBuilder
{
    public function createKorpaFromDb($resultSet): ?Korpa
    {
        if (!$resultSet) {
            return null;
        }

        $korpa = new Korpa();

        if (isset($resultSet['korisnik_id'])) {
            $korisnik = (new KorisnikDao())->getById($resultSet['korisnik_id']);
            $korpa->setKorisnik($korisnik);
        }

        if (isset($resultSet['proizvod_id'])) {
            $proizvod = (new ProizvodDao())->getById($resultSet['proizvod_id']);
            $korpa->setProizvod($proizvod);
        }

        if (isset($resultSet['cena'])) {
            $korpa->setCena($resultSet['cena']);
        }

        if (isset($resultSet['kolicina'])) {
            $korpa->setKolicina($resultSet['kolicina']);
        }

        if (isset($resultSet['datum'])) {
            $korpa->setDatum(Util::stringDateToObject($resultSet['datum']));
        }

        return $korpa;
    }
}