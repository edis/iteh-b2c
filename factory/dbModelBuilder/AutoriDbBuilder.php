<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 2/7/2018
 * Time: 9:56 PM
 */

class AutoriDbBuilder
{
    public function createAutorFromDb($resultSet): ?Autor
    {
        if (!$resultSet) {
            return null;
        }

        $autor = new Autor();

        if (isset($resultSet['id'])) {
            $autor->setId($resultSet['id']);
        }

        if (isset($resultSet['ime'])) {
            $autor->setIme($resultSet['ime']);
        }

        if (isset($resultSet['prezime'])) {
            $autor->setPrezime($resultSet['prezime']);
        }

        if (isset($resultSet['indeks'])) {
            $autor->setIndeks($resultSet['indeks']);
        }

        return $autor;
    }

    public function createAutorFromStdClass($autorSTD): Autor
    {
        if (empty($autorSTD) || !is_object($autorSTD)) {
            return null;
        }

        $autor = new Autor();

        if (isset($autorSTD->ime)) {
            $autor->setIme($autorSTD->ime);
        }
        if (isset($autorStd->prezime)) {
            $autor->setPrezime($autorSTD->prezime);
        }
        if (isset($autorSTD->indeks)) {
            $autor->setIndeks($autorSTD->indeks);
        }

        return $autor;
    }

}