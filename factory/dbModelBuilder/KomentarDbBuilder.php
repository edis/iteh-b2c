<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/29/2018
 * Time: 10:37 PM
 */

class KomentarDbBuilder
{
    public function createKomentarFromDB($resultSet)
    {
        if (!$resultSet) {
            return null;
        }

        $komentar = new Komentar();

        if (isset($resultSet['id'])) {
            $komentar->setId($resultSet['id']);
        }

        if (isset($resultSet['korisnik_id'])) {
            $korisnik = (new  KorisnikDao())->getById($resultSet['korisnik_id']);
            $komentar->setKorisnik($korisnik);
        }

        if (isset($resultSet['proizvod_id'])) {
            $proizvod = (new ProizvodDao())->getById($resultSet['proizvod_id']);
            $komentar->setProizvod($proizvod);
        }

        if (isset($resultSet['datum'])) {
            $komentar->setDatumIVreme(Util::stringDateToObject($resultSet['datum']));
        }

        if (isset($resultSet['komentar'])) {
            $komentar->setKomentar($resultSet['komentar']);
        }

        return $komentar;
    }
}