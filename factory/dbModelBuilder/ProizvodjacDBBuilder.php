<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/29/2018
 * Time: 3:02 PM
 */

class ProizvodjacDBBuilder
{
    public function createProizvodjacFromDB($resultSet): Proizvodjac
    {
        if (!$resultSet) {
            return null;
        }

        $proizvodjac = new Proizvodjac();

        if (isset($resultSet['id'])) {
            $proizvodjac->setId($resultSet['id']);
        }

        if (isset($resultSet['naziv'])) {
            $proizvodjac->setNaziv($resultSet['naziv']);
        }

        return $proizvodjac;
    }
}