<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 27-Jan-18
 * Time: 14:47
 */

class ProdavnicaDbBuilder
{
    public function createProdavnicaFromDb($resultSet): ?Prodavnica
    {
        if (!$resultSet) {
            return null;
        }

        $p = new Prodavnica();

        if (isset($resultSet['id'])) {
            $p->setId($resultSet['id']);
        }

        if (isset($resultSet['naziv'])) {
            $p->setNaziv($resultSet['naziv']);
        } else {
            $p->setNaziv('Непознат назив');
        }

        if (isset($resultSet['adresa'])) {
            $p->setAdresa($resultSet['adresa']);
        } else {
            $p->setAdresa('Непозната адреса');
        }

        if (isset($resultSet['email'])) {
            $p->setEmail($resultSet['email']);
        } else {
            $p->setEmail('Непозната имејл адреса');
        }

        if (isset($resultSet['telefon'])) {
            $p->setTelefon($resultSet['telefon']);
        } else {
            $p->setTelefon('Непознат телефон');
        }

        if (isset($resultSet['radno_vreme'])) {
            $p->setRadnoVreme($resultSet['radno_vreme']);
        } else {
            $p->setRadnoVreme('Непознато радно време');
        }

        return $p;
    }

    public function createProdavnicaFromStdClass($prodavnicaStd): ?Prodavnica
    {
        $p = new Prodavnica();

        if (empty($prodavnicaStd) || !is_object($prodavnicaStd)) {
            return null;
        }

        if (isset($prodavnicaStd->naziv)) {
            $p->setNaziv($prodavnicaStd->naziv);
        }

        if (isset($prodavnicaStd->email)) {
            $p->setEmail($prodavnicaStd->email);
        }

        if (isset($prodavnicaStd->adresa)) {
            $p->setAdresa($prodavnicaStd->adresa);
        }

        if (isset($prodavnicaStd->telefon)) {
            $p->setTelefon($prodavnicaStd->telefon);
        }

        if (isset($prodavnicaStd->radno_vreme)) {
            $p->setRadnoVreme($prodavnicaStd->radno_vreme);
        }

        return $p;
    }
}