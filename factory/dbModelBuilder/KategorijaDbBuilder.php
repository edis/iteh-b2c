<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/28/2018
 * Time: 12:34 PM
 */

class KategorijaDbBuilder
{
    public function createCategoryFromDb($resultSet): ?Kategorija
    {
        if (!$resultSet) {
            return null;
        }

        $cat = new Kategorija();

        if (isset($resultSet['id'])) {
            $cat->setId($resultSet['id']);
        }

        if (isset($resultSet['naziv'])) {
            $cat->setNaziv($resultSet['naziv']);
        }

        return $cat;
    }

    public function createKategorijaFromStdClass($kategorijaSTD): Kategorija
    {
        $kategorija = new Kategorija();
        $kategorija->setNaziv($kategorijaSTD->naziv);
        return $kategorija;
    }
}