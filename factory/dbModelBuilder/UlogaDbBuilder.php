<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/31/2018
 * Time: 9:11 PM
 */

class UlogaDbBuilder
{
    public function createUlogaFromDb($resultSet)
    {
        if (!$resultSet) {
            return null;
        }

        $uloga = new Uloga();

        if (isset($resultSet['id'])) {
            $uloga->setId($resultSet['id']);
        }

        if (isset($resultSet['naziv_uloge'])) {
            $uloga->setNazivUloge($resultSet['naziv_uloge']);
        }

        if (isset($resultSet['rang'])) {
            $uloga->setRang($resultSet['rang']);
        }

        return $uloga;
    }
}