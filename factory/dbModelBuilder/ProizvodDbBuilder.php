<?php

class ProizvodDbBuilder
{
    public function createProductFromDB($resultSet): ?Proizvod
    {
        if (!$resultSet) {
            return null;
        }

        $pr = new Proizvod();

        if (isset($resultSet['id'])) {
            $pr->setId($resultSet['id']);
        }

        if (isset($resultSet['naziv'])) {
            $pr->setNaziv($resultSet['naziv']);
        }

        if (isset($resultSet['kategorija_id'])) {
            $kategorija = (new KategorijaDao())->getById($resultSet['kategorija_id']);
            $pr->setKategorija($kategorija);
        }

        if (isset($resultSet['boja'])) {
            $pr->setBoja($resultSet['boja']);
        }

        if (isset($resultSet['cena'])) {
            $pr->setCena($resultSet['cena']);
        }

        if (isset($resultSet['proizvodjac_id'])) {
            $proizvodjac = (new ProizvodjacDao())->getById($resultSet['proizvodjac_id']);
            $pr->setProizvodjac($proizvodjac);
        }

        if (isset($resultSet['kolicina_na_stanju'])) {
            $pr->setKolicinaNaStanju($resultSet['kolicina_na_stanju']);
        }

        if (isset($resultSet['slika'])) {
            $pr->setSlika($resultSet['slika']);
        }

        if (isset($resultSet['specifikacija'])) {
            $pr->setSpecifikacija($resultSet['specifikacija']);
        }

        return $pr;
    }

    public function createProdavnicaFromStdClass($proizvodStd): ?Proizvod
    {
        $pr = new Proizvod();

        if (empty($proizvodStd) || !is_object($proizvodStd)) {
            return null;
        }

        if (isset($proizvodStd->naziv)) {
            $pr->setNaziv($proizvodStd->naziv);
        }

        if (isset($proizvodStd->slika)) {
            $pr->setSlika($proizvodStd->slika);
        }

        if (isset($proizvodStd->boja)) {
            $pr->setBoja($proizvodStd->boja);
        }

        if (isset($proizvodStd->kategorija_id)) {
            $kategorija = (new KategorijaDao())->getById($proizvodStd->kategorija_id);
            $pr->setKategorija($kategorija);
        }

        if (isset($proizvodStd->proizvodjac_id)) {
            $pr->setProizvodjac($proizvodStd->proizvodjac_id);
        }

        if (isset($proizvodStd->specifikacija)) {
            $pr->setSpecifikacija($proizvodStd->specifikacija);
        }

        if (isset($proizvodStd->kolicinaNaStanju)) {
            $pr->setKolicinaNaStanju($proizvodStd->kolicinaNaStanju);
        }
        return $pr;
    }

}

?>