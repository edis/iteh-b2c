<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 30-Jan-18
 * Time: 22:34
 */

class PorudzbinaDbBuilder
{
    public function createPorudzbinaFromDb($resultSet)
    {
        if (!$resultSet) {
            return null;
        }

        $p = new Porudzbina();

        if (isset($resultSet['id'])) {
            $p->setId($resultSet['id']);
        }

        if (isset($resultSet['korisnik_id'])) {
            $korisnik = (new KorisnikDao())->getById($resultSet['korisnik_id']);
            $p->setKorisnik($korisnik);
        }

        if (isset($resultSet['kolicina'])) {
            $p->setKolicina($resultSet['kolicina']);
        }

        if (isset($resultSet['cena'])) {
            $p->setCena($resultSet['cena']);
        }

        if (isset($resultSet['datum'])) {
            $datum = Util::stringDateToObject($resultSet['datum']);
            $p->setDatum($datum);
        }

        if (isset($resultSet['proizvod_id'])) {
            $proizvod = (new ProizvodDao())->getById($resultSet['proizvod_id']);
            $p->setProizvod($proizvod);
        }

        return $p;
    }
}