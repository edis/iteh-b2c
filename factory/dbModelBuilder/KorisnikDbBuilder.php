<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 28-Jan-18
 * Time: 02:43
 */

class KorisnikDbBuilder
{
    public function createKorisnikFromDb($resultSet): ?Korisnik
    {
        if (!$resultSet) {
            return null;
        }

        $k = new Korisnik();

        if (isset($resultSet['id'])) {
            $k->setId($resultSet['id']);
        }

        if (isset($resultSet['ime'])) {
            $k->setIme($resultSet['ime']);
        } else {
            $k->setIme('Непознато име');
        }

        if (isset($resultSet['prezime'])) {
            $k->setPrezime($resultSet['prezime']);
        } else {
            $k->setPrezime('Непознато презиме');
        }

        if (isset($resultSet['email'])) {
            $k->setEmail($resultSet['email']);
        }

        if (isset($resultSet['datum_registracije'])) {
            $k->setDatumRegistracije(Util::stringDateToObject($resultSet['datum_registracije']));
        }

        if (isset($resultSet['uloga_id'])) {
            $uloga = (new UlogaDao())->getById($resultSet['uloga_id']);
            $k->setUloga($uloga);
        }

        if (isset($resultSet['slika'])) {
            $k->setSlika($resultSet['slika']);
        }

        if (isset($resultSet['sifra'])) {
            $k->setSifra($resultSet['sifra']);
        }

        return $k;
    }

    public function createKorisnikFromRegistration($ime, $prezime, $email, $sifra, $slika): Korisnik
    {
        $k = new Korisnik();

        $k->setIme($ime);
        $k->setPrezime($prezime);
        $k->setEmail($email);
        $k->setSifra($sifra);
        $k->setSlika($slika);
        $k->setDatumRegistracije(date("Y-m-d H:i:s"));

        return $k;
    }

}