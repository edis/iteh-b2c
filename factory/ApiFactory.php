<?php

class ApiFactory
{
    public function createAutorProxy(): AutorProxy
    {
        $apiUrl = 'http://www.iteh-sajt.com/AutorApi';
        return new AutorProxy($apiUrl);
    }

    public function createGitHubUserProxy(string $username)
    {
        $apiUrl = 'https://api.github.com/users/' . $username;

        $authQueryString = GitUser::getAuthQueryString();

        $apiUrl .= (empty($authQueryString)) ? '' : '?' . $authQueryString;
        return new GitUserProxy($apiUrl);
    }
}