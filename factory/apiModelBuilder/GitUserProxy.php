<?php

class GitUserProxy extends AbstractModelProxy
{
    public function getOne()
    {
        if (!is_object($this->apiContent) || property_exists($this->apiContent, 'message')) {
            return null;
        }

        $gitUser = new GitUser();

        $gitUser->setId($this->apiContent->id);
        $gitUser->setAvatarUrl($this->apiContent->avatar_url);
        $gitUser->setEmail($this->apiContent->email);
        $gitUser->setFollowers($this->apiContent->followers);
        $gitUser->setFollowing($this->apiContent->following);
        $gitUser->setName($this->apiContent->name);
        $gitUser->setNumberOfRepositories($this->apiContent->public_repos);
        $gitUser->setReposApiUrl($this->apiContent->repos_url);
        $gitUser->setUserName($this->apiContent->login);
        $gitUser->setBio($this->apiContent->bio);

        return $gitUser;
    }

    protected function createModelFromStdClass($modelStd)
    {
        throw new Exception('Not implemented');
    }

    protected function getInstancesFromApi(): array
    {
        throw new Exception('Not implemented');
    }
}