<?php


use \GuzzleHttp\Client;

abstract class AbstractModelProxy
{
    protected $instances = array();
    protected $apiContent = null;

    private $apiUrl = null;
    private $requestMethod = 'GET';

    public function __construct(string $apiUrl)
    {
        $this->apiUrl = $apiUrl;
        $this->apiContent = $this->getDataFromJsonApi();
    }

    protected function getDataFromJsonApi()
    {
        require_once ROOT_URI . '/external_libraries/vendor/autoload.php';

        $client = new Client();

        try {
            $response = $client->request($this->requestMethod, $this->apiUrl, array(
                'timeout' => 3.5,
            ));
            if ($response->getStatusCode() != 200) {
                return null;
            }

            return json_decode($response->getBody()->getContents());
        } catch (Exception $e) {
            return null;
        }
    }

    public function resendRequest()
    {
        $this->apiContent = $this->getDataFromJsonApi();
        return $this;
    }

    public function first()
    {
        $this->createInstancesFromApi();
        return (!empty($this->instances)) ? $this->instances[0] : null;
    }

    private function createInstancesFromApi()
    {
        if (is_null($this->apiContent) || empty($this->apiContent)) {
            return [];
        }

        $this->instances = $this->getInstancesFromApi();
    }

    protected abstract function getInstancesFromApi(): array;

    public function getAll(): array
    {
        $this->createInstancesFromApi();
        return $this->instances;
    }

    public function last()
    {
        $this->createInstancesFromApi();
        return (empty($this->instances)) ? null : $this->instances[count($this->instances) - 1];
    }

    public function setApiContent(string $content)
    {
        $this->apiContent = $content;
        return $this;
    }

    public abstract function getOne();

    protected abstract function createModelFromStdClass($modelStd);

}