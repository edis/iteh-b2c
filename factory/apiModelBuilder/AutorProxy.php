<?php

class AutorProxy extends AbstractModelProxy
{
    public function __construct($apiUrl)
    {
        parent::__construct($apiUrl);
    }

    public function getOne()
    {
        if (is_null($this->apiContent) || empty($this->apiContent) || is_array($this->apiContent->data)) {
            return null;
        }

        $s = new Autor();
        $s->setIme($this->apiContent->data->ime);
        $s->setPrezime($this->apiContent->data->prezime);
        $s->setIndeks($this->apiContent->data->indeks);
        return $s;
    }

    protected function getInstancesFromApi(): array
    {
        if (is_object($this->apiContent->data)) {
            return [];
        }

        $ins = [];

        foreach ($this->apiContent->data as $autorFromApi) {
            $ins[] = $this->createModelFromStdClass($autorFromApi);
        }

        return $ins;
    }

    protected function createModelFromStdClass($studentStd)
    {
        $s = new Autor();
        $s->setIme($studentStd->ime);
        $s->setPrezime($studentStd->prezime);
        $s->setIndeks($studentStd->indeks);

        return $s;
    }
}