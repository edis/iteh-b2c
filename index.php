<?php
session_start();
define('ROOT_URI', trim(str_replace('\\', '/', __DIR__), '/'));

require_once ROOT_URI . '/app/init.php';

$autoloader = new Autoloader();
$autoloader->run();


$ctrl = new FrontController();
$ctrl->run();

