<?php


$types = array(
    MessageType::MESSAGE_INFO => 'info',
    MessageType::MESSAGE_DANGER => 'danger',
    MessageType::MESSAGE_SUCCESS => 'success',
);

foreach ($types as $messageType => $type) {
    $nizPoruka = ViewMessage::get($messageType);
    if (!empty($nizPoruka)) {
        foreach ($nizPoruka as $poruka) {
            ?>

            <div class="alert alert-<?= $type ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= $poruka ?>
            </div>
            <?php
        }
        unset($_SESSION[$type]);
    }
}

