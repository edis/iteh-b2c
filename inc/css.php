<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">

<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-grid.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-reboot.css">


<style type="text/css">
    body {
        margin-bottom: 200px;
    }

    button {
        cursor: pointer;
    }

    div {
        word-wrap: break-word;
        text-align: justify;
    }

</style>
