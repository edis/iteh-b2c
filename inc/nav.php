<div class="container-fluid">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Веб сајт</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">Почетна страна</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/proizvod">Производи</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/prodavnica">Наше продавнице</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/api">Апи</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/autori">Аутори сајта</a>
                </li>

                <?php
                if (isset($_COOKIE['admin_id'])) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin">Администрација</a>
                    </li>
                    <?php
                }
                ?>




                <?php
                if (isset($_COOKIE['id']) || isset($_COOKIE['admin_id'])) {
                    $id = (isset($_COOKIE['admin_id'])) ? $_COOKIE['admin_id'] : $_COOKIE['id'];
                    $korisnik = (new KorisnikDao())->getById($id);
                    ?>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false">
                            <?= $korisnik->getIme() . ' ' . $korisnik->getPrezime() ?>
                            <?php
                            $brojPorucenihProizvoda =
                                count((new PorudzbinaDao())->getBy(['korisnik_id' => $korisnik->getId()]));

                            $brojProizvodaUKorpi =
                                count((new KorpaDao())->getBy(['korisnik_id' => $korisnik->getId()]));
                            ?>
                        </a>

                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/korisnik">Профил</a>
                            <a class="dropdown-item" href="/korisnik/edit">Измена профила</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/korpa">Производи у корпи <span
                                        class="badge badge-info"><?= $brojProizvodaUKorpi ?></span></a>
                            <a class="dropdown-item" href="/porudzbina">Моје поруџбине <span
                                        class="badge badge-info"><?= $brojPorucenihProizvoda ?></span></a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/korisnik/logout">Одјави се</a>
                        </div>
                    </li>
                    <?php

                } else {
                    ?>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false">
                            Пријава
                        </a>

                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/login">Пријава</a>
                            <a class="dropdown-item" href="/registracija">Регистрација</a>
                        </div>
                    </li>

                    <?php
                }
                ?>


            </ul>
        </div>
    </nav>

</div>