<div class="container-fluid">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Администрација</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">

                <?php
                if (isset($_COOKIE['admin_id'])) {
                    ?>

                    <li class="nav-item active">
                        <a class="nav-link" href="/">Веб сајт</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" href="/admin">Почетна страна</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" href="/admin/prodavnica">Продавнице</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" href="/admin/proizvodi">Производи</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" href="/admin/korisnici">Корисници</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" href="/admin/log">Лог</a>
                    </li>

                    <?php
                }
                ?>


                <?php
                if (isset($_COOKIE['admin_id'])) {
                    $korisnik = (new KorisnikDao())->getById($_COOKIE['admin_id']);
                    ?>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false">
                            <?= $korisnik->getIme() . ' ' . $korisnik->getPrezime() ?>
                            <?php
                            $brojPorucenihProizvoda =
                                count((new PorudzbinaDao())->getBy(['korisnik_id' => $korisnik->getId()]));

                            $brojProizvodaUKorpi =
                                count((new KorpaDao())->getBy(['korisnik_id' => $korisnik->getId()]));
                            ?>
                        </a>

                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/korisnik">Профил</a>
                            <a class="dropdown-item" href="/korisnik/edit">Измена профила</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/korpa">Производи у корпи <span
                                        class="badge badge-info"><?= $brojProizvodaUKorpi ?></span></a>
                            <a class="dropdown-item" href="/porudzbina">Моје поруџбине <span
                                        class="badge badge-info"><?= $brojPorucenihProizvoda ?></span></a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/korisnik/logout">Одјави се</a>
                        </div>
                    </li>

                    <?php
                }


                ?>
            </ul>
        </div>
    </nav>

</div>