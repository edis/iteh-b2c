<nav>
    <ul class="pagination">

        <?php

        // ako stranica nije prva, prikazati link ka prvoj stranici
        if ($paginacija['currentPage'] != $paginacija['firstPage']) {
            ?>
            <li class="page-item">
                <a class="page-link" href="/<?= $url ?>/<?= $paginacija['previousPage'] ?>">
                    Претходна
                </a>
            </li>

            <?php
        }

        foreach ($paginacija['pages'] as $page) {

            if ($page == $paginacija['currentPage']) {
                ?>
                <li class="page-item active disabled">
                    <a class="page-link" href="/<?= $url ?>/<?= $page ?>">
                        <?= $page ?>
                    </a>
                </li>

                <?php
            } else {
                ?>
                <li class="page-item">
                    <a class="page-link" href="/<?= $url ?>/<?= $page ?>">
                        <?= $page ?>
                    </a>
                </li>

                <?php
            }
            ?>

            <?php
        }

        // ako stranica nije poslednja, prikazati link ka poslednjoj stranici
        if ($paginacija['currentPage'] != $paginacija['lastPage']) {
            ?>
            <li class="page-item">
                <a class="page-link" href="/<?= $url ?>/<?= $paginacija['nextPage'] ?>">
                    Следећа
                </a>
            </li>

            <?php
        }
        ?>

    </ul>
</nav>