<script src="/bootstrap/js/bootstrap.bundle.js"></script>
<script src="/bootstrap/js/bootstrap.js"></script>


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

<script src="/bootstrap/chart.js"></script>

<script>
    $(function () {
        $('.data-table').DataTable({
            "deferRender": true,
            "language": {
                "sProcessing": "Процесирање у току...",
                "sLengthMenu": "Број редова: _MENU_",
                "sZeroRecords": "Nije pronađen nijedan rezultat",
                "sInfo": "Приказ _START_ до _END_ од укупно _TOTAL_ елемената",
                "sInfoEmpty": "Приказ 0 до 0 од укупно 0 елемената",
                "sInfoFiltered": "(filtrirano od ukupno _MAX_ elemenata)",
                "sInfoPostFix": "",
                "sSearch": "Претрага",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Почетна",
                    "sPrevious": "Претходна",
                    "sNext": "Следећа",
                    "sLast": "Последња"
                }
            },
            "pageLength": 6
        });
    });
</script>

