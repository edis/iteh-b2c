<?php

$uri = trim($_SERVER['REQUEST_URI'], '/');
$splitedUri = (empty($uri)) ? array() : explode('/', $uri);
$splitedUri = array_slice($splitedUri, 0, 2);
?>

<div class="container-fluid">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="http://www.iteh-sajt.com/">Почетна страница</a></li>

            <?php

            $rootUri = 'http://www.iteh-sajt.com/';
            $fullUri = $rootUri;
            for ($i = 0; $i < count($splitedUri); $i++) {
                $fullUri .= trim($splitedUri[$i], '/') . '/';
                ?>

                <li class="breadcrumb-item"><a href="<?= $fullUri ?>"><?= $splitedUri[$i] ?></a></li>
                <?php
            }
            ?>
        </ol>
    </nav>
</div>

