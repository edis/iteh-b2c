<div class="container-fluid">
    <div class="jumbotron">
        <h2 class="display-4"><?= (isset($headerTitle)) ? $headerTitle : 'Тест'; ?></h2>
        <?php
        if (isset($headerDescription)) {
            echo '<p class="lead">' . $headerDescription . '</p>';
        }
        ?>
    </div>
</div>