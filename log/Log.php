<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 2/10/2018
 * Time: 7:44 PM
 */

class Log
{
    static $eventLog = 'log/EventLog.txt';
    static $errorLog = 'log/ErrorLog.txt';

    public static function Write(string $strFileName, string $strData)
    {
        $open = fopen($strFileName, 'a+');
        if ($open) {
            $data = "**** " . Util::DateObjectToString(Util::getCurrentDateTime()) . " **** " . $strData . "\n";
            fwrite($open, $data);
            fclose($open);
        }
    }

    public static function Read($strFileName)
    {
        if (file_exists(ROOT_URI . '/' . $strFileName)) {
            return file_get_contents($strFileName);
        }
        return 'Нема логова.';
    }
}

?>