<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 02-Feb-18
 * Time: 16:24
 */

abstract class ViewMessage
{
    public static function set(string $message, int $messageType = MessageType::MESSAGE_INFO)
    {
        $type = self::resolveMessageType($messageType);
        if (!isset($_SESSION[$type])) {
            $_SESSION[$type] = [];
        }

        $_SESSION[$type][] = $message;
    }

    public static function get(int $messageType = MessageType::MESSAGE_INFO)
    {
        $type = self::resolveMessageType($messageType);
        if (!isset($_SESSION[$type])) {
            $_SESSION[$type] = [];
        }
        return $_SESSION[$type];
    }

    private static function resolveMessageType(int $msgType): string
    {
        switch ($msgType) {
            case MessageType::MESSAGE_SUCCESS:
                return 'success';
            case MessageType::MESSAGE_INFO:
                return 'info';
            case MessageType::MESSAGE_DANGER:
                return 'danger';
            default:
                return 'info';
        }
    }
}

