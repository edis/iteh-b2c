<?php

use GuzzleHttp\Client;

abstract class BaseController
{
    protected function view(string $view, array $data = [])
    {
        extract($data);
        ob_start();
        require ROOT_VIEW . $view;
        return ob_get_clean();
    }

    protected function getDataFromApi(string $url)
    {
        require ROOT_URI . '/external_libraries/vendor/autoload.php';

        $client = new Client();

        try {
            $response = $client->request('GET', $url, array(
                'timeout' => 3.5,
            ));

            if ($response->getStatusCode() != 200) {
                return null;
            }

            return json_decode($response->getBody()->getContents());
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    protected function createArrayFromQueryString(string $queryString): array
    {
        $queryStringArray = [];
        if (strpos($queryString, '?') !== false) {
            $queryString = substr($queryString, 1);
        }

        parse_str($queryString, $queryStringArray);

        return $queryStringArray;
    }

    protected function setJsonContentHeader()
    {
        header('Content-type: application/json');
    }

    protected function getDataFromRequest()
    {
        $content = file_get_contents('php://input');
        if (empty($content)) {
            return null;
        }

        return json_decode($content);
    }

    protected function isUserLoggedIn(): bool
    {
        return (isset($_COOKIE['id']));
    }

    protected function isAdminLoggedIn(): bool
    {
        return (isset($_COOKIE['admin_id']));
    }

    protected function getUserId()
    {
        return ($this->isUserLoggedIn()) ? $_COOKIE['id'] : -1;
    }

    protected function getAdminId()
    {
        return ($this->isAdminLoggedIn()) ? $_COOKIE['admin_id'] : -1;
    }

    protected function getAdminOrUserId()
    {
        if (isset($_COOKIE['admin_id']) || isset($_COOKIE['id'])) {
            if (isset($_COOKIE['admin_id'])) {
                return $_COOKIE['admin_id'];
            }

            if (isset($_COOKIE['id'])) {
                return $_COOKIE['id'];
            }
        }
        return 0;
    }

    protected function getPagination(array $models, string $url, int $page = 1, int $perPage = 6)
    {
        $page = ($page > 0) ? $page : 1;
        $url = trim($url, '/');
        $perPage = ($perPage > 0) ? $perPage : 6;

        $paginacija = array();

        $count = count($models);
        $paginacija['count'] = $count;

        $modelsPaginate = array_slice($models, ($page - 1) * $perPage, $perPage);

        $lastPage = (ceil($count / $perPage) == 0) ? 1 : ceil($count / $perPage);

        $paginacija['data'] = $modelsPaginate;
        $paginacija['firstPage'] = 1;
        $paginacija['lastPage'] = $lastPage;
        $paginacija['currentPage'] = $page;
        $paginacija['pages'] = range(1, $lastPage);
        $paginacija['nextPage'] = ($page == $lastPage) ? $page : $page + 1;
        $paginacija['previousPage'] = ($page == 1) ? $page : $page - 1;

        if (empty($modelsPaginate)) {
            $paginacija['html'] = '';
            return $paginacija;
        }

        $paginacija['html'] = renderInc('paginacija.php', array(
            'paginacija' => $paginacija,
            'url' => $url,
        ));

        return $paginacija;
    }
}