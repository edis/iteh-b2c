<?php

abstract class BaseModel
{
    public abstract static function getTableName(): string;

    public abstract function getAsArray(): array;
}