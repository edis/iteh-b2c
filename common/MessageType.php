<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 02-Feb-18
 * Time: 16:50
 */

abstract class MessageType
{
    public const MESSAGE_SUCCESS = 1;
    public const MESSAGE_INFO = 2;
    public const MESSAGE_DANGER = 3;
}