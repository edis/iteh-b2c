<?php


function redirectTo(string $url)
{
    if (strpos($url, '404.php') !== false) {
        header('Location: /error');
        exit();
    }
    header('Location: ' . $url);
}

function render(string $view, array $params = [])
{
    extract($params);
    ob_start();
    require ROOT_VIEW . $view;
    return ob_get_clean();
}

function renderInc(string $fileName, array $params = [])
{
    extract($params);
    ob_start();
    require ROOT_URI . '/inc/' . $fileName;
    return ob_get_clean();
}

function requireFile(string $filePath, bool $required = true)
{
    ob_start();
    ($required === true) ? require_once ROOT_URI . '/' . $filePath : include_once ROOT_URI . '/' . $filePath;
    return ob_get_clean();
}

function filterVar(string $var)
{
    return strip_tags(htmlentities(trim($var)));
}