<?php

abstract class AbstractDao
{
    protected $db;

    public function __construct()
    {
        $this->db = DBBroker::getInstance();
    }

    public abstract function getById(int $id): ?BaseModel;

    public abstract function getBy(array $params): array;

    public abstract function getAll(): array;

    public abstract function insert(BaseModel $model): bool;

    public abstract function update(array $params, array $where): bool;

    public abstract function delete(BaseModel $model): bool;

    public abstract function count(array $params = null): int;

}