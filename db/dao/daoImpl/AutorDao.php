<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 2/7/2018
 * Time: 10:03 PM
 */

class AutorDao extends AbstractDao
{
    public function getById(int $id): ?BaseModel
    {
        try {
            $resultSet = $this->db->selectOne("SELECT * FROM " . Autor::getTableName() . " WHERE id=" . strip_tags(htmlentities($id)));

            return Factory::createDatabaseFactory()->createAutorDbBuilder()->createAutorFromDb($resultSet);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getBy(array $params): array
    {
        return [];
    }

    public function getAll(): array
    {
        try {
            $resultSet = $this->db->select("SELECT * FROM " . Autor::getTableName());

            if (!$resultSet) {
                return [];
            }

            $autori = array();
            $autorDbBuilder = Factory::createDatabaseFactory()->createAutorDbBuilder();

            foreach ($resultSet as $row) {
                $autori[] = $autorDbBuilder->createAutorFromDb($row);
            }

            return $autori;

        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }

    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Autor) {
            try {
                return $this->db->insert(Autor::getTableName(), array(
                    'ime' => $model->getIme(),
                    'prezime' => $model->getPrezime(),
                    'indeks' => $model->getIndeks()
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        }
        return false;
    }

    public function update(array $params, array $where): bool
    {
        return false;
    }

    public function delete(BaseModel $model): bool
    {
        return false;
    }

    public function count(array $params = null): int
    {
        return -1;
    }

}