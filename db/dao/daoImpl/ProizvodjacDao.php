<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/29/2018
 * Time: 3:08 PM
 */

class ProizvodjacDao extends AbstractDao
{

    public function getById(int $id): ?BaseModel
    {
        try {
            $result = $this->db->selectOne("SELECT * FROM " . Proizvodjac::getTableName() . " WHERE id=" . strip_tags(htmlentities($id)));

            if (!$result) {
                return null;
            }

            return Factory::createDatabaseFactory()->createProizvodjacDbBuilder()->createProizvodjacFromDB($result);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getBy(array $params): array
    {
        try {
            $resultSet = $this->db->selectBy(Proizvodjac::getTableName(), $params);

            if (!$resultSet) {
                return [];
            }

            $proizvodjaci = array();
            $proizvodjacDbBuilder = Factory::createDatabaseFactory()->createProizvodjacDbBuilder();

            foreach ($resultSet as $row) {
                $proizvodjaci[] = $proizvodjacDbBuilder->createProizvodjacFromDB($row);
            }

            return $proizvodjaci;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function getByName(string $name): ? BaseModel
    {
        try {
            $rs = $this->db->selectOne("SELECT * FROM " . Proizvodjac::getTableName() . " WHERE naziv='" . strtoupper(strip_tags(htmlentities($name))) . "'");

            if (!$rs) {
                return null;
            }

            return Factory::createDatabaseFactory()->createProizvodjacDbBuilder()->createProizvodjacFromDB($rs);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }


    public function getAll(): array
    {
        try {
            $result = $this->db->select("SELECT * FROM " . Proizvodjac::getTableName());

            if (!$result) {
                return [];
            }

            $proizvodjaci = [];
            $proizvodjacBuilder = Factory::createDatabaseFactory()->createProizvodjacDbBuilder();
            foreach ($result as $row) {
                $proizvodjaci[] = $proizvodjacBuilder->createProizvodjacFromDB($row);
            }

            return $proizvodjaci;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Proizvodjac) {
            try {
                return $this->db->insert(Proizvodjac::getTableName(), array(
                    'naziv' => $model->getNaziv()
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        }

        return false;
    }

    public function update(array $params, array $where): bool
    {
        try {
            return $this->db->update(Proizvodjac::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function delete(BaseModel $model): bool
    {

        if ($model instanceof Proizvodjac) {
            try {
                return $this->db->delete(Proizvodjac::getTableName(), $model->getId());
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        } else {
            return false;
        }
    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Proizvodjac::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }
}