<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/29/2018
 * Time: 10:51 PM
 */

class KomentarDao extends AbstractDao
{
    public function getById(int $id): ?BaseModel
    {
        try {
            $resultSet = $this->db->selectOne("SELECT * FROM " . Komentar::getTableName() . " WHERE id=" . strip_tags(htmlentities($id)));

            if (!$resultSet) {
                return null;
            }

            return Factory::createDatabaseFactory()->createKomentarDbBuilder()->createKomentarFromDB($resultSet);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getBy(array $params): array
    {
        try {
            $resultSet = $this->db->select("SELECT * FROM " . Komentar::getTableName() . " WHERE proizvod_id=" . strip_tags(htmlentities($params['proizvod_id'])) . " ORDER BY datum DESC");

            if (!$resultSet) {
                return array();
            }

            $komentari = array();

            $komentariDbBuilder = Factory::createDatabaseFactory()->createKomentarDbBuilder();

            foreach ($resultSet as $row) {
                $komentari[] = $komentariDbBuilder->createKomentarFromDB($row);

            }

            return $komentari;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function getAll(): array
    {
        try {
            $resultSet = $this->db->select("SELECT * FROM " . Komentar::getTableName() . " ORDER BY datum DESC");

            if (!$resultSet) {
                return [];
            }
            $komentari = array();
            $komentariDbBuilder = Factory::createDatabaseFactory()->createKomentarDbBuilder();

            foreach ($resultSet as $row) {
                $komentari[] = $komentariDbBuilder->createKomentarFromDB($row);
            }

            return $komentari;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }


    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Komentar) {
            try {
                return $this->db->insert(Komentar::getTableName(), array(
                    'korisnik_id' => $model->getKorisnik()->getId(),
                    'proizvod_id' => $model->getProizvod()->getId(),
                    'datum' => Util::DateObjectToString($model->getDatumIVreme()),
                    'komentar' => $model->getKomentar()

                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        } else {
            return false;
        }
    }


    public function update(array $params, array $where): bool
    {
        try {
            return $this->db->update(Komentar::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function delete(BaseModel $model): bool
    {
        try {
            if ($model instanceof Komentar) {
                return $this->db->delete(Komentar::getTableName(), $model->getId());
            }
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Komentar::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }

}