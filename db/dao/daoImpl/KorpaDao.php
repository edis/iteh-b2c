<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 29-Jan-18
 * Time: 20:08
 */

class KorpaDao extends AbstractDao
{

    public function getByIdAggr(int $korisnikId, int $proizvodId): ?BaseModel
    {
        try {
            $query = "SELECT * FROM " . Korpa::getTableName() . " WHERE korisnik_id = $korisnikId AND proizvod_id = $proizvodId";
            $rs = $this->db->selectOne($query);
            if (!$rs) {
                return null;
            }

            return Factory::createDatabaseFactory()->createKorpaDbBuilder()->createKorpaFromDb($rs);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getById(int $id): ?BaseModel
    {
        return null;
    }

    public function getBy(array $params): array
    {
        try {
            $resultSet = $this->db->selectBy(Korpa::getTableName(), $params, array('datum' => 'DESC'));

            if (!$resultSet) {
                return [];
            }

            $proizvodiUKorpi = array();
            $korpaBuilder = Factory::createDatabaseFactory()->createKorpaDbBuilder();

            foreach ($resultSet as $row) {
                $proizvodiUKorpi[] = $korpaBuilder->createKorpaFromDb($row);
            }

            return $proizvodiUKorpi;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function getAll(): array
    {
        try {
            $resultSet = $this->db->select("SELECT * FROM" . Korpa::getTableName());

            if (!$resultSet) {
                return [];
            }

            $korpe = array();
            $korpaDbBuilder = Factory::createDatabaseFactory()->createKorpaDbBuilder();

            foreach ($resultSet as $row) {
                $korpe[] = $korpaDbBuilder->createKorpaFromDb($row);
            }

            return $korpe;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Korpa) {
            try {
                return $this->db->insert(Korpa::getTableName(), array(
                    'korisnik_id' => $model->getKorisnik()->getId(),
                    'proizvod_id' => $model->getProizvod()->getId(),
                    'datum' => $model->getDatum()->format('Y-m-d H:i:s'),
                    'cena' => $model->getCena(),
                    'kolicina' => $model->getKolicina(),
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        }
        return false;
    }

    public function update(array $params, array $where): bool
    {
        try {
            return $this->db->update(Korpa::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function delete(BaseModel $model): bool
    {
        if ($model instanceof Korpa) {
            try {
                return $this->db->deleteBy(Korpa::getTableName(), array(
                    'korisnik_id' => $model->getKorisnik()->getId(),
                    'proizvod_id' => $model->getProizvod()->getId(),
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        }
        return false;
    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Korpa::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }
}