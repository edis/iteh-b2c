<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 28-Jan-18
 * Time: 21:29
 */

class KategorijaDao extends AbstractDao
{

    public function getById(int $id): ?BaseModel
    {
        try {
            $result = $this->db->selectOne("SELECT * FROM " . Kategorija::getTableName() . " WHERE id=" . strip_tags(htmlentities($id)));

            if (!$result) {
                return null;
            }

            return Factory::createDatabaseFactory()->createKategorijaDbBuilder()->createCategoryFromDb($result);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getBy(array $params): array
    {
        try {
            $resultSet = $this->db->selectBy(Kategorija::getTableName(), $params);

            if (!$resultSet) {
                return [];
            }

            $kategorije = array();
            $kategorijaDbBuilder = Factory::createDatabaseFactory()->createKategorijaDbBuilder();

            foreach ($resultSet as $row) {
                $kategorije[] = $kategorijaDbBuilder->createCategoryFromDb($row);
            }

            return $kategorije;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }

    }

    public function getByName(string $name): ?BaseModel
    {
        try {
            $rs = $this->db->selectOne("SELECT * FROM " . Kategorija::getTableName() . " WHERE naziv='" . strtolower(strip_tags(htmlentities($name))) . "'");

            if (!$rs) {
                return null;
            }

            return Factory::createDatabaseFactory()->createKategorijaDbBuilder()->createCategoryFromDb($rs);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getAll(): array
    {
        try {
            $result = $this->db->select("SELECT * FROM " . Kategorija::getTableName());

            if (!$result) {
                return [];
            }

            $kategorije = array();
            $kategorijaBuilder = Factory::createDatabaseFactory()->createKategorijaDbBuilder();

            foreach ($result as $row) {
                $kategorije[] = $kategorijaBuilder->createCategoryFromDb($row);
            }

            return $kategorije;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Kategorija) {
            try {
                return $this->db->insert(Kategorija::getTableName(), array(
                    'naziv' => $model->getNaziv()
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        } else {
            return false;
        }
    }

    public function update(array $params, array $where): bool
    {
        try {
            $this->db->update(Kategorija::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function delete(BaseModel $model): bool
    {
        try {
            if ($model instanceof Kategorija) {
                return $this->db->delete(Kategorija::getTableName(), $model->getId());
            } else {
                return false;
            }

        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Kategorija::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }
}