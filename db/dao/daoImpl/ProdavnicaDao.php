<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 27-Jan-18
 * Time: 14:46
 */

class ProdavnicaDao extends AbstractDao
{

    public function getById(int $id): ?BaseModel
    {
        try {
            $query = "SELECT * FROM " . Prodavnica::getTableName() . " WHERE id = " . strip_tags(htmlentities($id));
            $rs = $this->db->selectOne($query);

            return Factory::createDatabaseFactory()->createProdavnicaDbBuilder()->createProdavnicaFromDb($rs);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getBy(array $params): array
    {
        try {
            $resultSet = $this->db->selectBy(Prodavnica::getTableName(), $params);

            if (!$resultSet) {
                return [];
            }

            $prodavnice = array();
            $prodavnicaDbBuilder = Factory::createDatabaseFactory()->createProdavnicaDbBuilder();

            foreach ($resultSet as $row) {
                $prodavnice[] = $prodavnicaDbBuilder->createProdavnicaFromDb($row);
            }

            return $prodavnice;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function getAll(): array
    {
        try {
            $query = "SELECT * FROM " . Prodavnica::getTableName() . " ORDER BY id DESC";
            $resultSet = $this->db->select($query);

            $prodavnice = array();

            if (!$resultSet) {
                return $prodavnice;
            }

            $prodavnicaDbBuilder = Factory::createDatabaseFactory()->createProdavnicaDbBuilder();

            foreach ($resultSet as $row) {
                $prodavnice[] = $prodavnicaDbBuilder->createProdavnicaFromDb($row);
            }

            return $prodavnice;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Prodavnica) {
            try {
                return $this->db->insert(Prodavnica::getTableName(), array(
                    'naziv' => $model->getNaziv(),
                    'email' => $model->getEmail(),
                    'adresa' => $model->getAdresa(),
                    'telefon' => $model->getTelefon(),
                    'radno_vreme' => $model->getRadnoVreme(),
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        }
        return false;
    }

    public function update(array $params, array $where): bool
    {
        try {
            return $this->db->update(Prodavnica::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function delete(BaseModel $model): bool
    {
        if ($model instanceof Prodavnica) {
            try {
                return $this->db->delete(Prodavnica::getTableName(), $model->getId());
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        }
        return false;

    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Prodavnica::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }
}