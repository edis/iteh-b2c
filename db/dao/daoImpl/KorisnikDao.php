<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 28-Jan-18
 * Time: 02:53
 */

class KorisnikDao extends AbstractDao
{

    public function getById(int $id): ?BaseModel
    {
        try {
            $query = "SELECT * FROM " . Korisnik::getTableName() . " WHERE id = " . strip_tags(htmlentities($id));
            $rs = $this->db->selectOne($query);

            return Factory::createDatabaseFactory()->createKorisnikDbBuilder()->createKorisnikFromDb($rs);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }

    }

    public function getBy(array $params): array
    {
        try {
            $resultSet = $this->db->selectBy(Korisnik::getTableName(), $params);
            if (!$resultSet) {
                return [];
            }

            $korisnici = array();

            $korisnikBuilder = Factory::createDatabaseFactory()->createKorisnikDBBuilder();

            foreach ($resultSet as $row) {
                $korisnici[] = $korisnikBuilder->createKorisnikFromDb($row);
            }

            return $korisnici;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }


    }

    public function getAll(): array
    {
        try {
            $query = "SELECT * FROM " . Korisnik::getTableName();
            $resultSet = $this->db->select($query);

            if (!$resultSet) {
                return [];
            }

            $korisnici = array();

            $korisnikDbBuilder = Factory::createDatabaseFactory()->createKorisnikDbBuilder();

            foreach ($resultSet as $row) {
                $korisnici[] = $korisnikDbBuilder->createKorisnikFromDb($row);
            }

            return $korisnici;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }

    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Korisnik) {
            try {
                return $this->db->insert(Korisnik::getTableName(), array(
                    'ime' => $model->getIme(),
                    'prezime' => $model->getPrezime(),
                    'email' => $model->getEmail(),
                    'sifra' => $model->getSifra(),
                    'datum_registracije' => $model->getDatumRegistracije(),
                    'slika' => $model->getSlika(),
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        } else {
            return false;
        }
    }

    public function update(array $params, array $where): bool
    {
        try {
            return $this->db->update(Korisnik::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function delete(BaseModel $model): bool
    {

        try {
            if ($model instanceof Korisnik) {
                return $this->db->delete(Korisnik::getTableName(), $model->getId());
            } else {
                return false;
            }
        } catch (Exception $ex) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Korisnik::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }
}