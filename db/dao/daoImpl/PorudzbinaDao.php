<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 30-Jan-18
 * Time: 22:33
 */

class PorudzbinaDao extends AbstractDao
{

    public function getById(int $id): ?BaseModel
    {
        try {
            $rs = $this->db->selectOne("SELECT * FROM " . Porudzbina::getTableName() . " WHERE id = " . strip_tags($id));

            return Factory::createDatabaseFactory()->createPorudzbinaDbBuilder()->createPorudzbinaFromDb($rs);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getBy(array $params): array
    {
        try {
            $resultSet = $this->db->selectBy(Porudzbina::getTableName(), $params, ['datum' => 'DESC']);

            if (!$resultSet) {
                return [];
            }

            $porudzbine = [];
            $porudzbinaBuilder = Factory::createDatabaseFactory()->createPorudzbinaDbBuilder();

            foreach ($resultSet as $row) {
                $porudzbine[] = $porudzbinaBuilder->createPorudzbinaFromDb($row);
            }

            return $porudzbine;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function getAll(): array
    {
        try {
            $resultSet = $this->db->select("SELECT * FROM " . Porudzbina::getTableName());

            if (!$resultSet) {
                return [];
            }

            $porudzbine = array();
            $porudzbinaDbBuilder = Factory::createDatabaseFactory()->createPorudzbinaDbBuilder();

            foreach ($resultSet as $row) {
                $porudzbine[] = $porudzbinaDbBuilder->createPorudzbinaFromDb($row);
            }

            return $porudzbine;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Porudzbina) {
            try {
                return $this->db->insert(Porudzbina::getTableName(), array(
                    'korisnik_id' => $model->getKorisnik()->getId(),
                    'kolicina' => $model->getKolicina(),
                    'cena' => $model->getCena(),
                    'datum' => $model->getDatum()->format('Y-m-d H:i:s'),
                    'proizvod_id' => $model->getProizvod()->getId(),
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        }
        return false;
    }

    public function update(array $params, array $where): bool
    {
        try {
            $this->db->update(Porudzbina::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }

    }

    public function delete(BaseModel $model): bool
    {
        try {
            if ($model instanceof Porudzbina) {
                $this->db->delete(Porudzbina::getTableName(), $model->getId());
            } else {
                return false;
            }
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Porudzbina::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }
}