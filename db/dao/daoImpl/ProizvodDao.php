<?php

class ProizvodDao extends AbstractDao
{

    public function getById(int $id): ?BaseModel
    {
        try {
            $resultSet = $this->db->selectOne("SELECT  * FROM " . Proizvod::getTableName() . " WHERE proizvod.id=" . strip_tags(htmlentities($id)));
            if (!$resultSet) {
                return null;
            }

            return Factory::createDatabaseFactory()->createProizvodDbBuilder()->createProductFromDB($resultSet);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getBy(array $params): array
    {
        try {

            $resultSet = $this->db->selectBy(Proizvod::getTableName(), $params, array('id' => 'DESC'));
            if (!$resultSet) {
                return [];
            }

            $proizvodi = array();

            $proizvodBuilder = Factory::createDatabaseFactory()->createProizvodDbBuilder();

            foreach ($resultSet as $row) {
                $proizvodi[] = $proizvodBuilder->createProductFromDB($row);
            }

            return $proizvodi;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function getLike(string $naziv)
    {
        try {
            $resultSet = $this->db->select("SELECT * FROM " . Proizvod::getTableName() . " WHERE naziv LIKE '%" . strip_tags(htmlentities($naziv)) . "%'");

            if (!$resultSet) {
                return array();
            }

            $proizvodi = array();
            $proizvodDbBuilder = Factory::createDatabaseFactory()->createProizvodDbBuilder();

            foreach ($resultSet as $row) {
                $proizvodi[] = $proizvodDbBuilder->createProductFromDB($row);
            }

            return $proizvodi;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return array();
        }
    }


    public function getAll(): array
    {
        try {
            $resultSet = $this->db->select("SELECT * FROM " . Proizvod::getTableName() . " ORDER BY id DESC");

            if (!$resultSet) {
                return [];
            }

            $products = [];
            $proizvodiDBBuilder = Factory::createDatabaseFactory()->createProizvodDbBuilder();
            foreach ($resultSet as $row) {
                $products[] = $proizvodiDBBuilder->createProductFromDB($row);
            }

            return $products;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function insert(BaseModel $model): bool
    {
        try {
            if ($model instanceof Proizvod) {
                return $this->db->insert(Proizvod::getTableName(), array(
                    'naziv' => $model->getNaziv(),
                    'boja' => $model->getBoja(),
                    'cena' => $model->getCena(),
                    'kolicina_na_stanju' => $model->getKolicinaNaStanju(),
                    'proizvodjac_id' => $model->getProizvodjac()->getId(),
                    'kategorija_id' => $model->getKategorija()->getId(),
                    'slika' => $model->getSlika(),
                    'specifikacija' => $model->getSpecifikacija()
                ));
            } else {
                return false;
            }
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }

    }

    public function update(array $params, array $where): bool
    {
        try {
            return $this->db->update(Proizvod::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function delete(BaseModel $model): bool
    {
        if ($model instanceof Proizvod) {
            try {
                return $this->db->delete(Proizvod::getTableName(), $model->getId());
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        } else {
            return false;
        }
    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Proizvod::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }

}

?>