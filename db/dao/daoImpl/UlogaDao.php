<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/31/2018
 * Time: 9:07 PM
 */

class UlogaDao extends AbstractDao
{
    public function getById(int $id): ?BaseModel
    {
        try {
            $resultSet = $this->db->selectOne("SELECT * FROM " . Uloga::getTableName() . " WHERE id=" . strip_tags(htmlentities($id)));

            if (!$resultSet) {
                return null;
            }

            return Factory::createDatabaseFactory()->createUlogaDbBuilder()->createUlogaFromDb($resultSet);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return null;
        }
    }

    public function getBy(array $params): array
    {
        try {
            $resultSet = $this->db->selectBy(Uloga::getTableName(), $params);

            if (!$resultSet) {
                return [];
            }

            $uloge = array();
            $ulogaDbBuilder = Factory::createDatabaseFactory()->createUlogaDbBuilder();

            foreach ($resultSet as $row) {
                $uloge[] = $ulogaDbBuilder->createUlogaFromDb($row);
            }
            return $uloge;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }
    }

    public function getAll(): array
    {
        try {
            $resultSet = $this->db->select("SELECT * FROM " . Uloga::getTableName());

            if (!$resultSet) {
                return [];
            }

            $uloge = array();
            $ulogaDbBuilder = Factory::createDatabaseFactory()->createUlogaDbBuilder();

            foreach ($resultSet as $row) {
                $uloge[] = $ulogaDbBuilder->createUlogaFromDb($row);
            }

            return $uloge;
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return [];
        }

    }

    public function insert(BaseModel $model): bool
    {
        if ($model instanceof Uloga) {
            try {
                return $this->db->insert(Uloga::getTableName(), array(
                    'naziv_uloge' => $model->getNazivUloge(),
                    'rang' => $model->getRang()
                ));
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        } else {
            return false;
        }
    }

    public function update(array $params, array $where): bool
    {
        try {
            return $this->db->update(Uloga::getTableName(), $params, $where);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function delete(BaseModel $model): bool
    {
        if ($model instanceof Uloga) {
            try {
                return $this->db->delete(Uloga::getTableName(), $model->getId());
            } catch (Exception $e) {
                Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
                return false;
            }
        } else {
            return false;
        }
    }

    public function count(array $params = null): int
    {
        try {
            return $this->db->count(Uloga::getTableName(), $params);
        } catch (Exception $e) {
            Log::Write(Log::$errorLog, $e->getMessage() . "\n" . $e->getTraceAsString());
            return -1;
        }
    }

}