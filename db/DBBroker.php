<?php


class DBBroker
{

    /**
     * @var PDO $conn
     */
    private $conn;
    private static $instance;

    private function __construct()
    {
        try {
            $config = require_once ROOT_URI . '/config/db.config.php';
            $this->connect($config);
        } catch (Exception $e) {
            throw $e;
        }


    }

    private function connect(array $config)
    {
        try {
            $this->conn = new PDO('mysql:host=' . $config['host'] . ';port=' . $config['port'] . ';dbname=' . $config['dbname'] . ';charset=utf8', $config['username'], $config['password']);
        } catch (Exception $e) {
            throw new Exception("Грешка при конекцији.");
        }

    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new DBBroker();
        }
        return self::$instance;
    }

    public function startTransaction()
    {
        return $this->conn->beginTransaction();
    }

    public function commit()
    {
        return $this->conn->commit();
    }

    public function rollback()
    {
        return $this->conn->rollBack();
    }

    public function insert(string $tableName, array $values)
    {

        foreach ($values as $key => $value) {
            $columnNames[] = $key;
            $val[] = $value;
        }

        return $this->insertHelp($tableName, $columnNames, $val);

    }

    private function insertHelp(string $tableName, array $columnNames, array $values)
    {
        if (empty($tableName) || empty($columnNames) || empty($values)) {
            throw new Exception("Greska prilikom operacije insert.");
        }

        if (count($columnNames) !== count($values)) {
            throw new Exception("Nizovi moraju imati isti broj elemenata.");
        }

        $columnNamesString = '';
        $valuesString = '';

        foreach ($columnNames as $columnName) {
            $columnNamesString .= $columnName . ', ';
            $valuesString .= '?, ';
        }


        $columnNamesString = substr($columnNamesString, 0, -2);
        $valuesString = substr($valuesString, 0, -2);

        $query = "INSERT INTO $tableName ($columnNamesString) VALUES($valuesString)";

//        $this->startTransaction();

        $stmt = $this->conn->prepare($query);


        $param = 1;
        foreach ($values as $value) {
            $res = $stmt->bindValue($param, $value);
            $param++;
            if (!$res) {
//                $this->rollback();
                throw new Exception('Грешка приликом убацивања.');
            }
        }

        $result = $stmt->execute();

//        if ($result) {
//            $this->commit();
//        } else {
//            $this->rollback();
//        }

        return $result;
    }

    public function select(string $query)
    {
        $query = $this->conn->query($query);
        if ($query) {
            return $query->fetchAll(PDO::FETCH_ASSOC);
        } else {
            throw new Exception('Грешка. SELECT.');
        }

    }

    public function selectBy(string $tableName, array $by, array $orderBy = null)
    {

        $where = '';
        foreach ($by as $key => $value) {
            $where .= "$key = '" . $value . "' AND ";
        }

        $where = substr($where, 0, strlen($where) - 5);

        $order = "";
        if (!empty($orderBy)) {
            foreach ($orderBy as $key => $value) {
                $order .= "ORDER BY $key $value";
                break;
            }
        }

        $query = "SELECT * FROM $tableName WHERE $where";
        if (!empty($order)) {
            $query .= " $order";
        }

        $query = $this->conn->query($query);
        if ($query) {
            return $query->fetchAll(PDO::FETCH_ASSOC);
        } else {
            throw new Exception("Грешка у упиту.");
        }
    }


    public function selectOne(string $query)
    {
        $stmt = $this->conn->query($query);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function count(string $tableName, array $params = null)
    {
        $query = "SELECT COUNT(*) as broj FROM $tableName";

        if (!is_null($params)) {
            $where = '';
            foreach ($params as $key => $value) {
                $where .= "$key = '$value' AND ";
            }

            $where = substr($where, 0, -5);
            $query .= " WHERE $where";
        }

        $stmt = $this->conn->query($query);
        if ($stmt) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result['broj'];
        } else {
            throw new Exception("Грешка база. -> COUNT");
        }
    }

    public function delete(string $tableName, int $id)
    {
        $query = "DELETE FROM $tableName WHERE id = ? LIMIT 1";
        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(1, $id);
        return $stmt->execute();
    }

    public function deleteBy(string $tableName, array $whereCondition)
    {
        if (is_null($whereCondition) || empty($whereCondition)) {
            throw new Exception('Неисправан услов.');
        }

        $where = "";
        foreach ($whereCondition as $column => $value) {
            $where .= "$column = ? AND ";
        }

        $where = substr($where, 0, -5);

        $query = "DELETE FROM $tableName WHERE $where";

        $stmt = $this->conn->prepare($query);

        $paramNum = 1;
        foreach ($whereCondition as $column => $value) {
            $stmt->bindValue($paramNum++, $value);
        }

        return $stmt->execute();
    }

    public function update(string $tableName, array $params, array $whereCondition)
    {

        if (empty($params) || empty($whereCondition)) {
            throw new Exception('Параметри и услов су обавезни.');
        }

        $where = '';
        foreach ($whereCondition as $column => $value) {
            $where .= "$column = '" . $value . "' AND ";
        }

        $where = substr($where, 0, -5);

        $newValues = '';
        foreach ($params as $column => $value) {
            $newValues .= "$column = '$value', ";
        }

        $newValues = substr($newValues, 0, -2);

        $query = "UPDATE $tableName SET $newValues WHERE $where";

        $stmt = $this->conn->query($query);

        return $stmt->execute();
    }
}

?>