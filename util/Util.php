<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 29-Jan-18
 * Time: 21:21
 */

abstract class Util
{
    private static $timeZone = 'Europe/Belgrade';

    public static function stringDateToObject(string $dateTime): DateTime
    {
        return new DateTime($dateTime, new DateTimeZone(self::$timeZone));
    }

    public static function DateObjectToString(?DateTime $dateTime): string
    {
        if ($dateTime) {
            return $dateTime->format('Y-m-d H:i:s');
        }

        return "";
    }


    public static function compareDates(DateTime $datum)
    {
        $now = new DateTime('now', new DateTimeZone('Europe/Belgrade'));

        $razlika = $now->diff($datum);

        if ($razlika->y > 0) {
            if ($razlika->m > 0) {
                return "Пре " . $razlika->y . " година и " . $razlika->m . " месеци";
            }
            return "Пре {$razlika->y} година";
        }

        if ($razlika->m > 0) {
            return "Пре {$razlika->m} месеци";
        }

        if ($razlika->d > 0) {
            return "Пре {$razlika->d} дана";
        }

        if ($razlika->h > 0) {
            if ($razlika->m > 0) {
                return "Пре {$razlika->h} сати и {$razlika->m} минута";
            }
            return "Пре {$razlika->h} сати";
        }

        if ($razlika->i > 0) {
            return "Пре {$razlika->i} минута";
        }

        if ($razlika->s > 0) {
            return "Пре {$razlika->s} секунди";
        }

        return 'Пре мање од минут';
    }

    public static function getCurrentDateTimestamp()
    {
        $date = new DateTime('now', new DateTimeZone('Europe/Belgrade'));
        return $date->getTimestamp();
    }

    public static function getCurrentDateTime(): DateTime
    {
        return new DateTime('now', new DateTimeZone('Europe/Belgrade'));
    }

    public static function compareTwoDates(string $stringTime1, string $stringTime2): int
    {
        $dateTime1 = new DateTime($stringTime1);
        $dateTime2 = new DateTime($stringTime2);

        return $dateTime1->getTimestamp() > $dateTime2->getTimestamp() ? 1 : -1;

    }


}