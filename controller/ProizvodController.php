<?php

class ProizvodController extends BaseController
{
    public function indexAction(int $page = 1)
    {
        $proizvodi = (new ProizvodDao())->getAll();
        $kategorije = (new KategorijaDao())->getAll();

        $paginacija = $this->getPagination($proizvodi, '/proizvod/index/', $page, 8);

        echo $this->view('proizvod/index.php', array(
            'proizvodi' => $paginacija['data'],
            'paginacija' => $paginacija,
            'kategorije' => $kategorije,
        ));
    }

    public function showAction(string $nazivPr)
    {
        $proizvod = (new ProizvodDao())->getBy(['naziv' => urldecode(trim($nazivPr))]);
        $komentari = (new KomentarDao())->getBy(['proizvod_id' => $proizvod[0]->getId()]);

        echo $this->view('proizvod/show.php', ['proizvod' => $proizvod[0], 'komentari' => $komentari]);
    }

    public function insertAction()
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo('/');
            return;
        }

        $proizvodjaci = (new ProizvodjacDao())->getAll();
        $kategorije = (new KategorijaDao())->getAll();

        echo $this->view("admin/proizvod/insert.php", array(
            'proizvodjaci' => $proizvodjaci,
            'kategorije' => $kategorije
        ));
    }

    public function createAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST" || !$this->isAdminLoggedIn()) {
            redirectTo('/admin/proizvodi/');
            return;
        }

        $naziv = (isset($_POST['naziv'])) ? strip_tags($_POST['naziv']) : 'Непознат назив';
        $proizvodjacID = (isset($_POST['proizvodjac_id'])) ? strip_tags($_POST['proizvodjac_id']) : -1;
        $kategorijaID = (isset($_POST['kategorija_id'])) ? strip_tags($_POST['kategorija_id']) : -1;
        $slika = (isset($_POST['slika'])) ? strip_tags($_POST['slika']) : 'Непозната слика';
        $boja = (isset($_POST['boja'])) ? strip_tags($_POST['boja']) : 'Непозната боја';
        $kolicinaNaStanju = (isset($_POST['kolicina_na_stanju'])) ? strip_tags($_POST['kolicina_na_stanju']) : 'Непозната количина';
        $specifikacija = (isset($_POST['specifikacija'])) ? strip_tags($_POST['specifikacija']) : 'Непозната спецификација';
        $cena = (isset($_POST['cena'])) ? strip_tags($_POST['cena']) : 'Непозната цена';

        $proizvodjac = (new ProizvodjacDao())->getById($proizvodjacID);
        $kategorija = (new KategorijaDao())->getById($kategorijaID);

        $admin = (new KorisnikDao())->getById($this->getAdminId());

        $proizvod = new Proizvod();

        $proizvod->setSlika($slika);
        $proizvod->setNaziv($naziv);
        $proizvod->setSpecifikacija($specifikacija);
        $proizvod->setKolicinaNaStanju($kolicinaNaStanju);
        $proizvod->setProizvodjac($proizvodjac);
        $proizvod->setKategorija($kategorija);
        $proizvod->setBoja($boja);
        $proizvod->setCena($cena);

        $validno = (new ProizvodValidator())->validate($proizvod);

        if (!$validno) {
            ViewMessage::set('Грешка приликом валидације производа.', MessageType::MESSAGE_DANGER);
            redirectTo('/proizvod/insert/');
            return;
        }

        $proizvodDao = new ProizvodDao();
        if ($proizvodDao->insert($proizvod)) {
            ViewMessage::set('Производ је успешно креиран.', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је додао нови производ " . $proizvod->getNaziv());

        } else {
            ViewMessage::set('Грешка. Производ није креиран', MessageType::MESSAGE_DANGER);
        }
        redirectTo('/admin/proizvodi/');
    }


    public function editAction(int $id)
    {

        $proizvod = (new ProizvodDao())->getById($id);
        $proizvodjaci = (new ProizvodjacDao())->getAll();
        $kategorije = (new KategorijaDao())->getAll();

        echo $this->view("admin/proizvod/edit.php", array(
            'proizvod' => $proizvod,
            'proizvodjaci' => $proizvodjaci,
            'kategorije' => $kategorije,
        ));
    }

    public function updateAction(int $id)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST" || !$this->isAdminLoggedIn()) {
            redirectTo('/admin/proizvodi');
            return;
        }


        $admin = (new KorisnikDao())->getById($this->getAdminId());

        $proizvodDao = new ProizvodDao();

        /** @var Proizvod $proizvod */
        $proizvod = $proizvodDao->getById($id);

        $naziv = (isset($_POST['naziv'])) ? strip_tags($_POST['naziv']) : 'Непознат назив';
        $slika = (isset($_POST['slika'])) ? strip_tags($_POST['slika']) : 'Непозната слика';

        $proizvodjacID = (isset($_POST['proizvodjac_id'])) ? strip_tags($_POST['proizvodjac_id']) : -1;
        $kategorijaID = (isset($_POST['kategorija_id'])) ? strip_tags($_POST['kategorija_id']) : -1;
        $cena = (isset($_POST['cena'])) ? strip_tags($_POST['cena']) : 'Непозната цена';
        $boja = (isset($_POST['boja'])) ? strip_tags($_POST['boja']) : 'Непозната боја';
        $kolicinaNaStanju = (isset($_POST['kolicina_na_stanju'])) ? strip_tags($_POST['kolicina_na_stanju']) : 'Непозната количина';
        $specifikacija = (isset($_POST['specifikacija'])) ? strip_tags($_POST['specifikacija']) : 'Непозната спецификација';

        $proizvodjac = (new ProizvodjacDao())->getById($proizvodjacID);
        $kategorija = (new KategorijaDao())->getById($kategorijaID);

        $uspesno = $proizvodDao->update(array(
            'naziv' => $naziv,
            'slika' => $slika,
            'proizvodjac_id' => $proizvodjac->getId(),
            'kategorija_id' => $kategorija->getId(),
            'cena' => $cena,
            'boja' => $boja,
            'kolicina_na_stanju' => $kolicinaNaStanju,
            'specifikacija' => $specifikacija
        ), array(
            'id' => $proizvod->getId()
        ));

        if ($uspesno) {
            ViewMessage::set('Производ је успешно ажуриран.', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је ажурирао " . $proizvod->getNaziv());
        } else {
            ViewMessage::set('Производ је успешно креирана.', MessageType::MESSAGE_DANGER);
        }
        redirectTo('/admin/proizvodi/');
    }

    public function deleteAction(int $id)
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo("/admin/proizvodi/");
            return;
        }

        $admin = (new KorisnikDao())->getById($this->getAdminId());

        $proizvodDao = new ProizvodDao();
        $proizvod = $proizvodDao->getById($id);

        if (!empty($proizvod) && $proizvodDao->delete($proizvod)) {
            ViewMessage::set('Производ је успешно обрисан', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је обрисао производ " . $proizvod->getNaziv());
        } else {
            ViewMessage::set('Грешка. Производ није обрисан', MessageType::MESSAGE_DANGER);
        }
        redirectTo('/admin/proizvodi/');
    }


}

