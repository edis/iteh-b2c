<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 28-Jan-18
 * Time: 03:09
 */

class KorisnikController extends BaseController
{
    public function indexAction()
    {
        $userId = $this->getAdminOrUserId();
        $korisnik = (new KorisnikDao())->getById($userId);

        echo $this->view('korisnik/index.php', array(
            'korisnik' => $korisnik,
        ));
    }

    public function editAction()
    {
        $userId = $this->getAdminOrUserId();
        $korisnik = (new KorisnikDao())->getById($userId);

        echo $this->view('korisnik/edit.php', array(
            'korisnik' => $korisnik,
        ));
    }

    public function showCustomerAction(int $id)
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo("admin/form");
        }

        $korisnik = (new KorisnikDao())->getById($id);

        echo $this->view("korisnik/index.php", array(
            'korisnik' => $korisnik
        ));
    }

    public function deleteAction(int $id)
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo("/login");
            return;
        }

        $korisnikDao = new KorisnikDao();
        $korisnik = $korisnikDao->getById($id);
        $admin = $korisnikDao->getById($this->getAdminId());


        if (!empty($korisnik) && $korisnikDao->delete($korisnik)) {
            ViewMessage::set('Корисник је успешно обрисан', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је обрисао корисника: " . $korisnik->getIme() . " " . $korisnik->getPrezime());
            redirectTo('/admin/korisnici/');
        } else {
            ViewMessage::set('Грешка. Корисник није обрисан', MessageType::MESSAGE_DANGER);
            redirectTo('/admin/korisnici/');
        }

    }

    public function updateSifraAction()
    {
        $korisnikID = $this->getAdminOrUserId();
        if ($_SERVER['REQUEST_METHOD'] !== 'POST' || empty($korisnikID)) {
            redirectTo('/error');
            return;
        }

        $sifra = isset($_POST['sifra']) ? $_POST['sifra'] : null;
        $sifra1 = isset($_POST['sifra1']) ? $_POST['sifra1'] : null;

        $valid = KorisnikValidator::validateSifra($sifra, $sifra1);


        if ($valid == 'valid') {
            $korisnikDao = new KorisnikDao();

            $korisnik = $korisnikDao->getById($korisnikID);

            if (empty($korisnik)) {
                redirectTo('/error');
                return;
            }

            $where['id'] = $korisnikID;
            $params['sifra'] = $sifra;

            if ($korisnikDao->update($params, $where)) {
                echo "Успешна промена шифре.";
                Log::Write(Log::$eventLog, $korisnik->getIme() . " " . $korisnik->getPrezime() . "(" . $korisnik->getUloga()->getNazivUloge() . ") је променио шифру");
            } else {
                echo "Неуспешна промена шифре.";
            }
        } else {
            echo $valid;
        }
    }


    public function updateProfilAction()
    {
        $korisnikId = $this->getAdminOrUserId();
        if ($_SERVER['REQUEST_METHOD'] !== 'POST' || empty($korisnikId)) {
            redirectTo('/error');
            return;
        }

        $ime = isset($_POST['ime']) ? $_POST['ime'] : null;
        $prezime = isset($_POST['prezime']) ? $_POST['prezime'] : null;
        $email = isset($_POST['email']) ? $_POST['email'] : null;

        $valid = KorisnikValidator::validateProfil($ime, $prezime, $email);

        if (is_array($valid)) {
            $korisnikDao = new KorisnikDao();
            $stariKorisnik = $korisnikDao->getById($korisnikId);
            $noviKorisnik = $korisnikDao->getBy($valid);

            if (empty($noviKorisnik) || ($stariKorisnik instanceof Korisnik && $noviKorisnik[0] instanceof Korisnik)) {

                if (empty($noviKorisnik) || $stariKorisnik->getEmail() == $noviKorisnik[0]->getEmail()) {

                    $where['id'] = $korisnikId;
                    $params['ime'] = $ime;
                    $params['prezime'] = $prezime;
                    $params['email'] = $email;
                    if ($korisnikDao->update($params, $where)) {
                        echo 'Успешна измена';
                        Log::Write(Log::$eventLog, $stariKorisnik->getIme() . " " . $stariKorisnik->getPrezime() . "(" . $stariKorisnik->getUloga()->getNazivUloge() . ") је ажурирао профил");

                    } else {
                        echo 'Неуспешна измена';
                    }
                } else {
                    echo 'Корисник са унетим имејлом већ постоји.';
                    return;
                }
            }
        } else {
            echo $valid;
        }
    }

    public function logoutAction()
    {
        if ($this->isUserLoggedIn()) {
            $korisnik = (new KorisnikDao())->getById($this->getUserId());
            Log::Write(Log::$eventLog, $korisnik->getIme() . " " . $korisnik->getPrezime() . " (корисник) се одјавио");
            setcookie('id', $this->getUserId(), time() - (86400 * 30), "/");
        }

        if ($this->isAdminLoggedIn()) {
            $korisnik = (new KorisnikDao())->getById($this->getAdminId());
            Log::Write(Log::$eventLog, $korisnik->getIme() . " " . $korisnik->getPrezime() . " (администратор) се одјавио");
            setcookie('admin_id', $this->getAdminId(), time() - (86400 * 30), "/");
        }

        redirectTo('/');
    }


    public function uploadSlikaAction()
    {
        $back = $_SERVER['HTTP_REFERER'];

        $fileName = $_FILES['slika']['name'];
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $fileSize = $_FILES['slika']['size'];

        $dirname = 'slike';

        $dozvoljeniFormati = array('png', 'jpg', 'jpeg');

        if (empty($fileName) || $fileSize == 0) {
            ViewMessage::set("Морате изабрати слику.", MessageType::MESSAGE_DANGER);
            redirectTo($back);
            return;
        } else if (!in_array($extension, $dozvoljeniFormati)) {
            ViewMessage::set("Формат $extension није дозвољен.", MessageType::MESSAGE_DANGER);
            redirectTo($back);
            return;
        } else if (file_exists(ROOT_URI . '/' . $dirname . '/' . $fileName)) {
            ViewMessage::set("Слика са називом $fileName већ постоји", MessageType::MESSAGE_DANGER);
            redirectTo($back);
            return;
        }

        $korisnikDao = new KorisnikDao();

        /** @var Korisnik $korisnik */
        $korisnik = $korisnikDao->getById($this->getAdminOrUserId());
        if (empty($korisnik)) {
            redirectTo('/error');
            return;
        }


        if (move_uploaded_file($_FILES['slika']['tmp_name'], ROOT_URI . '/' . $dirname . '/' . $fileName)) {
            if ($korisnikDao->update(['slika' => $fileName], ['id' => $korisnik->getId()])) {
                ViewMessage::set("Слика је постављена успешно", MessageType::MESSAGE_SUCCESS);
                Log::Write(Log::$eventLog, $korisnik->getIme() . " " . $korisnik->getPrezime() . "(" . $korisnik->getUloga()->getNazivUloge() . ") је ажурирао слику на профилу");

            } else {
                ViewMessage::set("Грешка. Слика није постављена.", MessageType::MESSAGE_DANGER);
            }
        } else {
            ViewMessage::set("Грешка. Слика није постављена.", MessageType::MESSAGE_DANGER);
        }

        redirectTo($back);
    }

    public function createAction()
    {

        if ($_SERVER['REQUEST_METHOD'] !== "POST" || !$this->isAdminLoggedIn()) {
            redirectTo('/admin/korisnici/');
            return;
        }

        $ime = (isset($_POST['ime'])) ? strip_tags($_POST['ime']) : 'Непознато име';
        $prezime = (isset($_POST['prezime'])) ? strip_tags($_POST['prezime']) : 'Непознато презиме';
        $email = (isset($_POST['email'])) ? strip_tags($_POST['email']) : 'Непознат имејл';
        $sifra = (isset($_POST['sifra'])) ? strip_tags($_POST['sifra']) : 'Непозната шифра';


        $korisnik = new Korisnik();

        $korisnik->setIme($ime);
        $korisnik->setPrezime($prezime);
        $korisnik->setEmail($email);
        $korisnik->setSifra($sifra);
        $korisnik->setDatumRegistracije(Util::DateObjectToString(Util::getCurrentDateTime()));


        $validanProfil = (new KorisnikValidator())->validateProfil($ime, $prezime, $email);

        if (!$validanProfil) {
            redirectTo('/korisnik/insert/');
            return;
        }

        $korisnikDao = new KorisnikDao();
        $admin = $korisnikDao->getById($this->getAdminId());
        if ($korisnikDao->insert($korisnik)) {
            ViewMessage::set('Корисник је успешно креиран.', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је унео новог корисника: Име: " . $korisnik->getIme() . " Презиме: " . $korisnik->getPrezime());
            redirectTo('/admin/korisnici/');
        } else {
            ViewMessage::set('Грешка. Корисник није креиран', MessageType::MESSAGE_DANGER);
            redirectTo('/admin/korisnici/');
        }
    }

    public function insertAction()
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo("/login");
            return;
        }

        echo $this->view("admin/korisnik/insert.php");
    }

    public function updateAction(int $id)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST" || !$this->isAdminLoggedIn()) {
            redirectTo('/admin/prodavnica');
            return;
        }


        $korisnikDao = new KorisnikDao();

        /** @var Korisnik $korisnik */
        $korisnik = $korisnikDao->getById($id);
        $admin = $korisnikDao->getById($this->getAdminId());
        $ime = (isset($_POST['ime'])) ? strip_tags($_POST['ime']) : 'Непознато име';
        $prezime = (isset($_POST['prezime'])) ? strip_tags($_POST['prezime']) : 'Непознато презиме';
        $email = (isset($_POST['email'])) ? strip_tags($_POST['email']) : 'Непознат имејл';
        $sifra = (isset($_POST['sifra'])) ? strip_tags($_POST['sifra']) : 'Непозната шифра';
        $ulogaID = (isset($_POST['uloga_id'])) ? strip_tags($_POST['uloga_id']) : 1;

        $uspesno = $korisnikDao->update(array(
            'ime' => $ime,
            'prezime' => $prezime,
            'email' => $email,
            'sifra' => $sifra,
            'uloga_id' => $ulogaID,
        ), array(
            'id' => $korisnik->getId()
        ));

        if ($uspesno) {
            ViewMessage::set('Корисник је успешно ажуриран.', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је изменио корисника: Име: " . $korisnik->getIme() . " Презиме: " . $korisnik->getPrezime());
            if ($korisnik->getId() == $this->getAdminId()) {
                redirectTo('/korisnik/logout');
                return;
            }
        } else {
            ViewMessage::set('Грешка. Корисник није ажуриран', MessageType::MESSAGE_DANGER);
        }
        redirectTo('/admin/korisnici/');
    }

    public function adminEditAction(int $id)
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo("/login");
            return;
        }

        $korisnik = (new KorisnikDao())->getById($id);
        $uloge = (new UlogaDao())->getAll();

        echo $this->view("admin/korisnik/edit.php", array(
            'korisnik' => $korisnik,
            'uloge' => $uloge,
        ));
    }


}