<?php

class LoginController extends BaseController
{
    public function __construct()
    {
        if (!empty($this->getAdminOrUserId())) {
            redirectTo('/');
        }
    }

    public function indexAction()
    {
        echo $this->view('login/index.php');
    }

    public function loginAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST") {
            redirectTo('/error');
        }

        $email = (isset($_POST['email'])) ? $_POST['email'] : null;
        $sifra = (isset($_POST['sifra'])) ? $_POST['sifra'] : null;

        $ok = PrijavaValidator::validate($email, $sifra);

        if (is_array($ok)) {
            $korisnikDao = new KorisnikDao();
            $korisnik = $korisnikDao->getBy($ok);
            if (empty($korisnik) || count($korisnik) != 1) {
                echo 'Унели сте погрешно корисничко име или шифру';
                return;
            } else {
                $this->setCookieValues($korisnik[0]->getId(), $korisnik[0]->getUloga()->getRang());
                echo 'Успешна пријава';
                Log::Write(Log::$eventLog, $korisnik[0]->getIme() . ' ' . $korisnik[0]->getPrezime() . ' (' . $korisnik[0]->getUloga()->getNazivUloge() . ')  се пријавио');

            }
        } else {
            echo $ok;
        }
    }


    private function setCookieValues($id, int $rang)
    {
        if ($rang == 111) {
            setcookie('admin_id', $id, time() + (86400 * 30), "/");
        } else {
            setcookie('id', $id, time() + (86400 * 30), "/");
        }
    }

}