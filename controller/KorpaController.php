<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 29-Jan-18
 * Time: 20:21
 */

class KorpaController extends BaseController
{
    public function indexAction(int $page = 1)
    {
        if (empty($this->getAdminOrUserId())) {
            redirectTo('/login');
            return;
        }

        $korisnikId = $this->getAdminOrUserId();
        $proizvodi = (new KorpaDao())->getBy(array('korisnik_id' => $korisnikId));

        $paginacija = $this->getPagination($proizvodi, '/korpa/index/', $page);

        echo $this->view('/korpa/index.php', array(
            'proizvodi' => $paginacija['data'],
            'paginacija' => $paginacija,
        ));
    }

    public function dodajUKorpuAction(int $proizvodId)
    {
        $korisnikID = $this->getAdminOrUserId();
        if (empty($korisnikID)) {
            echo 'Нисте улоговани. Пријавите се да бисте могли да додате производ у корпу.';
            return false;
        }

        $korisnik = (new KorisnikDao())->getById($korisnikID);
        $proizvod = (new ProizvodDao())->getById($proizvodId);


        if (empty($korisnik) || empty($proizvod)) {
            echo 'Дошло је до грешке.';
            return false;
        }


        $korpaDao = new KorpaDao();
        $korpa = $korpaDao->getByIdAggr($korisnikID, $proizvodId);

        if (!empty($korpa)) {
            echo 'Овај производ се већ налази у Вашој корпи.';
            return false;
        }

        $korpa = new Korpa();
        $korpa->setProizvod($proizvod);
        $korpa->setKorisnik($korisnik);
        $korpa->setCena($proizvod->getCena());
        $korpa->setKolicina(1);
        $korpa->setDatum(Util::getCurrentDateTime());

        echo ($korpaDao->insert($korpa)) ? 'Производ је додат у корпу.' . Log::Write(Log::$eventLog, $korisnik->getIme() . " " . $korisnik->getPrezime() . "(" . $korisnik->getUloga()->getNazivUloge() . ") је производ " . $proizvod->getNaziv() . " у корпу") : 'Грешка. Производ није додат у корпу.';

    }

    public function obrisiAction(int $proizvodId)
    {
        $korisnikId = $this->getAdminOrUserId();
        $korisnik = (new KorisnikDao())->getById($korisnikId);
        if (empty($korisnikId)) {
            echo 'Грешка. Пријавите се да бисте обрисали производ из корпе.';
            return false;
        }


        $korpaDao = new KorpaDao();
        $korpa = $korpaDao->getByIdAggr($korisnikId, $proizvodId);
        if (empty($korpa)) {
            echo 'Грешка. Производ није у корпи.';
            return false;
        }

        echo ($korpaDao->delete($korpa)) ? 'Производ је успешно обрисан из корпе' . Log::Write(Log::$eventLog, $korisnik->getIme() . " " . $korisnik->getPrezime() . "(" . $korisnik->getUloga()->getNazivUloge() . ") је обрисао производ " . $korpa->getProizvod()->getNaziv() . " из корпе") : 'Грешка. Производ није обрисан';
    }

    public function promenaKolicineAction(int $korisnikId, int $proizvodId)
    {
        $novaKolicina = (int)(isset($_POST['nova_kolicina'])) ? strip_tags($_POST['nova_kolicina']) : null;
        if (empty($novaKolicina) || !is_numeric($novaKolicina)) {
            echo 'Грешка. Количина мора бити број.';
            echo false;
            return;
        }

        $novaKolicina = (int)$novaKolicina;

        $korpaDao = new KorpaDao();

        /** @var Korpa $korpa */
        $korpa = $korpaDao->getByIdAggr($korisnikId, $proizvodId);
        if (empty($korpa)) {
            echo false;
            return;
        }

        echo ($korpaDao->update(['kolicina' => $novaKolicina],
            ['korisnik_id' => $korisnikId, 'proizvod_id' => $proizvodId])) ? true : false;

    }
}