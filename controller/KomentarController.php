<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 1/31/2018
 * Time: 11:32 PM
 */

class KomentarController extends BaseController
{
    public function addCommentAction(int $proizvodId)
    {
        $komentarValue = strip_tags(trim($_POST['komentar']));
        if (empty($komentarValue) || mb_strlen($komentarValue, 'UTF-8') < 2) {
            ViewMessage::set('Морате унети коментар.', MessageType::MESSAGE_DANGER);
            redirectTo($_SERVER['HTTP_REFERER']);
            return;
        }

        $korisnik = null;

        if ($this->isAdminLoggedIn()) {
            $korisnik = (new KorisnikDao())->getById($this->getAdminId());
        } else if ($this->isUserLoggedIn()) {
            $korisnik = (new KorisnikDao())->getById($this->getUserId());
        } else {
            ViewMessage::set('Пријавите се да бисте поставили кометар.', MessageType::MESSAGE_DANGER);
            redirectTo($_SERVER['HTTP_REFERER']);
            return;
        }

        if (empty($korisnik)) {
            ViewMessage::set('Пријавите се да бисте поставили кометар.', MessageType::MESSAGE_DANGER);
            redirectTo($_SERVER['HTTP_REFERER']);
            return;
        }

        if ($this->isUserLoggedIn() || $this->isAdminLoggedIn()) {

            $komentarDao = new KomentarDao();

            $komentar = new Komentar();
            $komentar->setProizvod((new ProizvodDao())->getById($proizvodId));
            $komentar->setKorisnik($korisnik);
            $komentar->setKomentar($komentarValue);
            $komentar->setDatumIVreme(Util::getCurrentDateTime());

            if ($komentarDao->insert($komentar)) {
                ViewMessage::set('Коментар је успешно постављен.', MessageType::MESSAGE_SUCCESS);
                Log::Write(Log::$eventLog, $korisnik->getIme() . " " . $korisnik->getPrezime() . "(" . $korisnik->getUloga()->getNazivUloge() . ") је поставио коментар: " . $komentar->getKomentar());
                redirectTo($_SERVER['HTTP_REFERER']);
                return;
            } else {
                ViewMessage::set('Догодила се грешка приликом памћења коментара.', MessageType::MESSAGE_DANGER);
                redirectTo($_SERVER['HTTP_REFERER']);
                return;
            }
        }
    }

    public function editAction(int $id)
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo('/');
            return;
        }

        $komentar = (new KomentarDao())->getById($id);

        echo $this->view("admin/komentar/edit.php", array(
            'komentar' => $komentar
        ));
    }

    public function updateAction(int $id)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST" || !$this->isAdminLoggedIn()) {
            redirectTo('/proizvod/');
            return;
        }


        $komentarDao = new KomentarDao();

        /** @var Komentar $komentar */
        $komentar = $komentarDao->getById($id);
        $admin = (new KorisnikDao())->getById($this->getAdminId());
        $komentarTekst = (isset($_POST['komentar'])) ? strip_tags(trim($_POST['komentar'])) : 'Непознат коментар';

        $uspesno = $komentarDao->update(array(
            'komentar' => $komentarTekst,
        ), array(
            'id' => $komentar->getId()
        ));


        if ($uspesno) {
            ViewMessage::set('Коментар је успешно ажуриран.', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је изменио коментар: " . $komentar->getKomentar());
        } else {
            ViewMessage::set('Грешка. Коментар није ажуриран.', MessageType::MESSAGE_DANGER);
        }
        redirectTo('/proizvod');
    }

    public function deleteAction(int $id)
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo("/proizvod");
            return;
        }


        $komentarDao = new KomentarDao();
        $komentar = $komentarDao->getById($id);
        $admin = (new KorisnikDao())->getById($this->getAdminId());
        $tekst = $komentar->getKomentar();
        if (!empty($komentar) && $komentarDao->delete($komentar)) {
            ViewMessage::set('Коментар је успешно обрисан', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је избрисао коментар: " . $tekst);
            redirectTo($_SERVER['HTTP_REFERER']);
        } else {
            ViewMessage::set('Грешка. Коментар није обрисан', MessageType::MESSAGE_DANGER);
            redirectTo('/proizvod');
        }
    }


}