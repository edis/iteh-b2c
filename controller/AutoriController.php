<?php

class AutoriController extends BaseController
{
    public function indexAction()
    {
        $studenti = Factory::createApiFactory()->createAutorProxy()->getAll();

        echo $this->view('autori/index.php', array(
            'studenti' => $studenti
        ));
    }
}