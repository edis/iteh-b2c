<?php


class IndexController extends BaseController
{
    public function indexAction()
    {
        $proizvodi = (new ProizvodDao())->getAll();
        if (count($proizvodi) > 3) {
            $proizvodi = array_slice($proizvodi, 0, 3);
        }

        echo $this->view('index/index.php', array(
            'proizvodi' => $proizvodi,
        ));
    }
}