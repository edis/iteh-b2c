<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 30-Jan-18
 * Time: 22:25
 */

class PorudzbinaController extends BaseController
{
    public function __construct()
    {
        if (empty($this->getAdminOrUserId())) {
            redirectTo('/login');
            return;
        }
    }

    public function indexAction(int $page = 1)
    {
        $porudzbine = (new PorudzbinaDao())->getBy(['korisnik_id' => $this->getAdminOrUserId()]);

        $paginacija = $this->getPagination($porudzbine, '/porudzbina/index/', $page);

        $iznosKupovinePoMesecimaData = IznosKupovinePoMesecima::getData();
        $brojPorucenihProizvodaPoKategoriji = BrojPorucenihProizvodaPoKategoriji::getData(intval(date('Y')), $this->getAdminOrUserId());

        echo $this->view('porudzbina/index.php', array(
            'porudzbine' => $paginacija['data'],
            'paginacija' => $paginacija,
            'iznosKupovinePoMesecimaData' => $iznosKupovinePoMesecimaData,
            'brojPorucenihProizvodaPoKategoriji' => $brojPorucenihProizvodaPoKategoriji,
        ));
    }

    public function createAction(int $proizvodId)
    {
        $korisnikID = $this->getAdminOrUserId();
        if (empty($korisnikID)) {
            echo 'Нисте улоговани пријавите се да бисте наручили производ.';
            return false;
        }

        $korisnik = (new KorisnikDao())->getById($korisnikID);
        $proizvod = (new ProizvodDao())->getById($proizvodId);

        if (empty($proizvod)) {
            echo 'Производ који желите да наручите не постоји.';
            return false;
        }

        $korpaDao = new KorpaDao();
        $korpa = $korpaDao->getByIdAggr($korisnikID, $proizvodId);
        if (empty($korpa)) {
            echo 'Грешка. Производ који хоћете да поручите није у Вашој корпи.';
            return false;
        }

        DBBroker::getInstance()->startTransaction();

        $porudzbina = new Porudzbina();
        $porudzbina->setKorisnik($korisnik);
        $porudzbina->setProizvod($proizvod);
        $porudzbina->setDatum(Util::getCurrentDateTime());
        $porudzbina->setCena($korpa->getCena());
        $porudzbina->setKolicina($korpa->getKolicina());
        $dodatoUPorudzbine = (new PorudzbinaDao())->insert($porudzbina);

        $uspesnoBrisanjeIzKorpe = $korpaDao->delete($korpa);

        if ($dodatoUPorudzbine && $uspesnoBrisanjeIzKorpe) {
            DBBroker::getInstance()->commit();
            echo 'Производ је успешно поручен.';
            Log::Write(Log::$eventLog, $korisnik->getIme() . " " . $korisnik->getPrezime() . "(" . $korisnik->getUloga()->getNazivUloge() . ") је поручио производ " . $proizvod->getNaziv());
            return true;
        } else {
            DBBroker::getInstance()->rollback();
            echo 'Грешка. Производ није поручен.';
            return false;
        }


    }
}