<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 31-Jan-18
 * Time: 15:47
 */

class AdminController extends BaseController
{

    public function __construct()
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo('/');
            return;
        }
    }

    public function indexAction(int $godina = -1)
    {
        if ($godina == -1 || ($godina > intval(date('Y')))) {
            $godina = intval(date('Y'));
        }

        $prihodPoMesecimaData = PrihodPoMesecima::getData($godina);
        $brojPorudzbinaPoMesecimaData = BrojPorudzbinaPoMesecima::getData($godina);
        $brojPorucenihProizvodaPoKategoriji = BrojPorucenihProizvodaPoKategoriji::getData($godina);
        $prihodPoProizvodu = PrihodPoProizvodu::getData($godina);

        echo $this->view('admin/index.php', array(
            'brojPorudzbinaPoMesecimaData' => $brojPorudzbinaPoMesecimaData,
            'prihodPoMesecimaData' => $prihodPoMesecimaData,
            'brojPorucenihProizvodaPoKategoriji' => $brojPorucenihProizvodaPoKategoriji,
            'prihodPoProizvodu' => $prihodPoProizvodu,
            'godina' => $godina,
        ));
    }

    public function korisniciAction()
    {
        $korisnici = (new KorisnikDao())->getAll();

        echo $this->view("admin/korisnik/index.php", array(
            'korisnici' => $korisnici,
        ));
    }

    public function prodavnicaAction(string $poruka = null)
    {
        $prodavnice = (new ProdavnicaDao())->getAll();

        echo $this->view('admin/prodavnica/index.php', array(
            'prodavnice' => $prodavnice,
            'poruka' => urldecode($poruka),
        ));
    }

    public function proizvodiAction()
    {
        $proizvodi = (new ProizvodDao())->getAll();

        echo $this->view("admin/proizvod/index.php", array(
            'proizvodi' => $proizvodi,
        ));
    }

    public function komentariAction()
    {
        $komentari = (new KomentarDao())->getAll();

        echo $this->view("admin/komentar/index.php", array(
            'komentari' => $komentari
        ));
    }

    public function LogAction()
    {
        echo $this->view("admin/log/index.php");
    }
}