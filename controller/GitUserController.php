<?php

class GitUserController extends BaseController
{

    public function indexAction()
    {
        echo $this->view('gituser/index.php');
    }

    public function showAction(string $userName)
    {
        $user = Factory::createApiFactory()->createGitHubUserProxy($userName)->getOne();

        echo $this->view('gituser/show.php', [
            'user' => $user,
        ]);
    }

}