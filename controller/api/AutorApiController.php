<?php
/**
 * Created by PhpStorm.
 * User: Petar
 * Date: 2/7/2018
 * Time: 10:32 PM
 */

class AutorApiController extends BaseController
{
    public function __construct()
    {
        $this->setJsonContentHeader();
    }

    public function indexAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $autori = (new AutorDao())->getAll();

        $autoriApi = [];

        /** @var Autor $autor */
        foreach ($autori as $autor) {
            $autoriApi[] = $autor->getAsArray();
        }

        echo json_encode(array(
            'total' => count($autoriApi),
            'data' => $autoriApi,
        ));
    }

    public function idAction(int $id = -1)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $autor = (new AutorDao())->getById($id);
        echo (empty($autor)) ?
            json_encode(array('error' => 'Аутор није пронађен')) :
            json_encode($autor->getAsArray());
    }

    public function createAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $porukaGreska = json_encode(array('message' => 'Грешка. Аутор није додат.'));
        $porukaUspesno = json_encode(array('message' => 'Аутор је успешно додат.'));

        $reqData = $this->getDataFromRequest();

        if (empty($reqData)) {
            echo $porukaGreska;
            return;
        }

        $autor = Factory::createDatabaseFactory()->createAutorDbBuilder()->createAutorFromStdClass($reqData);
        $valid = Factory::createValidator('Autor')->validate($autor);

        if (!$valid) {
            echo $porukaGreska;
            return;
        }

        $autorDao = new AutorDao();
        if ($autorDao->insert($autor)) {
            echo $porukaUspesno;
        } else {
            echo $porukaGreska;
        }

    }

}