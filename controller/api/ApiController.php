<?php

class ApiController extends BaseController
{
    public function __construct()
    {
        $this->setJsonContentHeader();
    }

    public function indexAction()
    {
        header('Content-type: text/html;charset=utf-8');
        echo $this->view('api/index.php');
    }

    public function puskiceVestiAction(string $queryString = '')
    {
        $queryStringArray = $this->createArrayFromQueryString($queryString);

        $page = (isset($queryStringArray['page'])) ? $queryStringArray['page'] : 1;
        echo json_encode($this->getDataFromApi('http://api.puskice.org/news?page=' . $page));
    }

    public function studentAction(int $id = -1)
    {
        if ($id <= 0) {
            $data = $this->getDataFromApi('http://localhost:2140/api/student');
        } else {
            $data = $this->getDataFromApi('http://localhost:2140/api/student/' . $id);
        }

        if (is_null($data)) {
            echo json_encode(['Грешка приликом учитавања.']);
        } else {
            echo json_encode($data);
        }

    }

}