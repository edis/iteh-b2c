<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 28-Jan-18
 * Time: 14:27
 */

class ProdavnicaApiController extends BaseController
{
    public function __construct()
    {
        $this->setJsonContentHeader();
    }

    public function indexAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $prodavnice = (new ProdavnicaDao())->getAll();
        $prodavniceApi = array();

        /** @var Prodavnica $p */
        foreach ($prodavnice as $p) {
            $prodavniceApi[] = $p->getAsArray();
        }

        echo json_encode(array(
            'total' => count($prodavniceApi),
            'data' => $prodavniceApi,
        ));
    }

    public function idAction(int $id = -1)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $prodavnica = (new ProdavnicaDao())->getById($id);
        echo (empty($prodavnica)) ? json_encode(array('error' => 'Продавница није пронађена')) : json_encode($prodavnica->getAsArray());
    }

    public function createAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $porukaGreska = json_encode(array('message' => 'Грешка. Продавница није додата.'));
        $porukaUspesno = json_encode(array('message' => 'Продавница је успешно додата.'));

        $reqData = $this->getDataFromRequest();

        if (empty($reqData)) {
            echo $porukaGreska;
            return;
        }

        $prodavnica = Factory::createDatabaseFactory()->createProdavnicaDbBuilder()->createProdavnicaFromStdClass($reqData);
        $valid = Factory::createValidator('Prodavnica')->validate($prodavnica);

        if (!$valid) {
            echo $porukaGreska;
            return;
        }

        $prodavnicaDao = new ProdavnicaDao();
        if ($prodavnicaDao->insert($prodavnica)) {
            echo $porukaUspesno;
            return;
        } else {
            echo $porukaGreska;
            return;
        }
    }

    public function updateAction(int $id)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "PUT") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $prodavnicaDao = new ProdavnicaDao();
        /** @var Prodavnica $prodavnica */
        $prodavnica = $prodavnicaDao->getById($id);

        if (empty($prodavnica)) {
            echo json_encode(array('error' => 'Грешка. Продавница не постоји.'));
            return;
        }

        $requestData = $this->getDataFromRequest();

        if (isset($requestData->naziv)) {
            $prodavnica->setNaziv($requestData->naziv);
        }

        if (isset($requestData->adresa)) {
            $prodavnica->setAdresa($requestData->adresa);
        }

        if (isset($requestData->email)) {
            $prodavnica->setEmail($requestData->email);
        }

        if (isset($requestData->radno_vreme)) {
            $prodavnica->setRadnoVreme($requestData->radno_vreme);
        }

        if (isset($requestData->telefon)) {
            $prodavnica->setTelefon($requestData->telefon);
        }

        if ($prodavnicaDao->update(array(
            'naziv' => $prodavnica->getNaziv(),
            'adresa' => $prodavnica->getAdresa(),
            'email' => $prodavnica->getEmail(),
            'radno_vreme' => $prodavnica->getRadnoVreme(),
            'telefon' => $prodavnica->getTelefon(),
        ), array(
            'id' => $prodavnica->getId(),
        ))) {
            echo json_encode(array('message' => 'Продавница је успешно ажурирана.'));
        } else {
            echo json_encode(array('message' => 'Грешка. Продавница није ажурирана.'));
        }
    }

    public function deleteAction(int $id)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "DELETE") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $prodavnicaDao = new ProdavnicaDao();
        $prodavnica = $prodavnicaDao->getById($id);
        if (empty($prodavnica)) {
            echo json_encode(array('error' => 'Грешка. Продавница не постоји.'));
            return;
        }

        if ($prodavnicaDao->delete($prodavnica)) {
            echo json_encode(array('message' => 'Продавница је успешно обрисана.'));
        } else {
            echo json_encode(array('message' => 'Грешка. Продавница није обрисана.'));
        }
    }


}