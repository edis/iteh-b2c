<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 07-Feb-18
 * Time: 20:56
 */

class ProizvodApiController extends BaseController
{
    public function __construct()
    {
        $this->setJsonContentHeader();
    }

    public function indexAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $proizvodi = (new ProizvodDao())->getAll();
        $proizvodiApi = array();

        /** @var Proizvod $p */
        foreach ($proizvodi as $p) {
            array_push($proizvodiApi, $p->getAsArray());
        }

        echo json_encode(array(
            'total' => count($proizvodiApi),
            'data' => $proizvodiApi,
        ));
    }

    public function idAction(int $id)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $proizvod = (new ProizvodDao())->getById($id);
        echo (empty($proizvod)) ? json_encode(array('error' => 'Производ није пронађен')) : json_encode($proizvod->getAsArray());

    }

    public function kategorijaAction(int $kategorijaId)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $kategorija = (new KategorijaDao())->getById($kategorijaId);
        if (empty($kategorija)) {
            echo json_encode(array('total' => 0, 'error' => 'Категорија не постоји.'));
            return;
        }

        $proizvodi = (new ProizvodDao())->getBy(['kategorija_id' => $kategorijaId]);

        $proizvodiApi = array();
        /** @var Proizvod $proizvod */
        foreach ($proizvodi as $proizvod) {
            array_push($proizvodiApi, $proizvod->getAsArray());
        }

        echo json_encode(array('total' => count($proizvodiApi), 'kategorija' => $kategorija->getNaziv(), 'data' => $proizvodiApi));
    }

    public function nazivLikeAction(string $naziv)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $proizvodi = (new ProizvodDao())->getLike($naziv);

        $proizvodiApi = array();

        foreach ($proizvodi as $proizvod) {
            array_push($proizvodiApi, $proizvod->getAsArray());
        }

        echo json_encode(array('total' => count($proizvodiApi), 'data' => $proizvodiApi));
    }
}