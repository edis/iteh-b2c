<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 01-Feb-18
 * Time: 23:19
 */

class KategorijaApiController extends BaseController
{
    public function __construct()
    {
        $this->setJsonContentHeader();
    }

    public function indexAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $kategorije = (new KategorijaDao())->getAll();

        $kategorijeApi = [];

        /** @var Kategorija $kategorija */
        foreach ($kategorije as $kategorija) {
            $kategorijeApi[] = $kategorija->getAsArray();
        }

        echo json_encode(array(
            'total' => count($kategorijeApi),
            'data' => $kategorijeApi,
        ));
    }

    public function idAction(int $id = -1)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $kategorija = (new KategorijaDao())->getById($id);
        echo (empty($kategorija)) ?
            json_encode(array('error' => 'Категорија није пронађена')) :
            json_encode($kategorija->getAsArray());
    }

    public function createAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $porukaGreska = json_encode(array('message' => 'Грешка. Категорија није додата.'));
        $porukaUspesno = json_encode(array('message' => 'Категорија је успешно додата.'));

        $reqData = $this->getDataFromRequest();

        if (empty($reqData)) {
            echo $porukaGreska;
            return;
        }

        $kategorija = Factory::createDatabaseFactory()->createKategorijaDbBuilder()->createKategorijaFromStdClass($reqData);
        $valid = Factory::createValidator('Kategorija')->validate($kategorija);

        if (!$valid) {
            echo $porukaGreska;
            return;
        }

        $kategorijaDao = new KategorijaDao();
        if ($kategorijaDao->insert($kategorija)) {
            echo $porukaUspesno;
        } else {
            echo $porukaGreska;
        }

    }
}