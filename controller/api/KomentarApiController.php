<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 01-Feb-18
 * Time: 23:47
 */

class KomentarApiController extends BaseController
{
    public function __construct()
    {
        $this->setJsonContentHeader();
    }

    public function indexAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "GET") {
            echo json_encode(array('error' => 'Bad method request'));
            return;
        }

        $komentari = (new KomentarDao())->getAll();
        $komentariApi = array();

        /** @var Komentar $komentar */
        foreach ($komentari as $komentar) {
            $komentariApi[] = $komentar->getAsArray();
        }

        echo json_encode(array(
            'total' => count($komentariApi),
            'data' => $komentariApi,
        ));
    }

    public function komentariAction(string $email)
    {
        // vratiti sve komentare korisnika $username
    }

}