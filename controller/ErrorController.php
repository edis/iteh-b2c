<?php

class ErrorController extends BaseController
{
    private $headerTitle = 'Грешка';

    public function indexAction()
    {
        echo $this->view('404.php', ['headerTitle' => $this->headerTitle . ' 404']);
    }
}