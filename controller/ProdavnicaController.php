<?php
/**
 * Created by PhpStorm.
 * User: edis
 * Date: 27-Jan-18
 * Time: 14:58
 */

class ProdavnicaController extends BaseController
{
    public function indexAction(int $page = 1)
    {
        $page = ($page > 0) ? $page : 1;
        $prodavnice = (new ProdavnicaDao())->getAll();

        $paginacija = $this->getPagination($prodavnice, '/prodavnica/index/', $page);

        echo $this->view('prodavnica/index.php', array(
            'prodavnice' => $paginacija['data'],
            'paginacija' => $paginacija,
        ));
    }

    public function showAction(int $id)
    {
        $prodavnica = (new ProdavnicaDao())->getById($id);

        echo $this->view('prodavnica/show.php', array(
            'prodavnica' => $prodavnica,
        ));
    }

    public function editAction(int $id)
    {

        if (!$this->isAdminLoggedIn()) {
            redirectTo('/admin/prodavnica');
            return;
        }

        $prodavnica = (new ProdavnicaDao())->getById($id);

        echo $this->view('admin/prodavnica/edit.php', array(
            'prodavnica' => $prodavnica,
        ));
    }

    public function insertAction()
    {
        if (!$this->isAdminLoggedIn()) {
            redirectTo('/admin/prodavnica');
            return;
        }

        echo $this->view('admin/prodavnica/insert.php');
    }

    public function createAction()
    {

        if ($_SERVER['REQUEST_METHOD'] !== "POST" || !$this->isAdminLoggedIn()) {
            redirectTo('/admin/prodavnica/');
            return;
        }

        $naziv = (isset($_POST['naziv'])) ? strip_tags($_POST['naziv']) : 'Непознат назив';
        $email = (isset($_POST['email'])) ? strip_tags($_POST['email']) : 'Непознат имејл';
        $adresa = (isset($_POST['adresa'])) ? strip_tags($_POST['adresa']) : 'Непозната адреса';
        $telefon = (isset($_POST['telefon'])) ? strip_tags($_POST['telefon']) : 'Непознат телефон';
        $radnoVreme = (isset($_POST['radno_vreme'])) ? strip_tags($_POST['radno_vreme'], '<BR>') : 'Непознато радно време';

        $prodavnica = new Prodavnica();

        $prodavnica->setNaziv($naziv);
        $prodavnica->setEmail($email);
        $prodavnica->setAdresa($adresa);
        $prodavnica->setTelefon($telefon);
        $prodavnica->setRadnoVreme($radnoVreme);

        $admin = (new KorisnikDao())->getById($this->getAdminId());

        $validno = (new ProdavnicaValidator())->validate($prodavnica);

        if (!$validno) {
            redirectTo('/prodavnica/insert/');
            return;
        }

        $prodavnicaDao = new ProdavnicaDao();
        if ($prodavnicaDao->insert($prodavnica)) {
            ViewMessage::set('Продавница је успешно креирана.', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је додао нову продавницу " . $prodavnica->getNaziv());
        } else {
            ViewMessage::set('Грешка. Продавница није креирана', MessageType::MESSAGE_DANGER);
        }

        redirectTo('/admin/prodavnica/');
    }

    public function updateAction(int $id)
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST" || !$this->isAdminLoggedIn()) {
            redirectTo('/admin/prodavnica');
            return;
        }


        $prodavnicaDao = new ProdavnicaDao();

        /** @var Prodavnica $prodavnica */
        $prodavnica = $prodavnicaDao->getById($id);
        $admin = (new KorisnikDao())->getById($this->getAdminId());

        $naziv = (isset($_POST['naziv'])) ? strip_tags($_POST['naziv']) : 'Непознат назив';
        $email = (isset($_POST['email'])) ? strip_tags($_POST['email']) : 'Непознат имејл';
        $adresa = (isset($_POST['adresa'])) ? strip_tags($_POST['adresa']) : 'Непозната адреса';
        $telefon = (isset($_POST['telefon'])) ? strip_tags($_POST['telefon']) : 'Непознат телефон';
        $radnoVreme = (isset($_POST['radno_vreme'])) ? strip_tags($_POST['radno_vreme']) : 'Непознато радно време';

        $uspesno = $prodavnicaDao->update(array(
            'naziv' => $naziv,
            'email' => $email,
            'adresa' => $adresa,
            'telefon' => $telefon,
            'radno_vreme' => $radnoVreme,
        ), array(
            'id' => $prodavnica->getId()
        ));

        if ($uspesno) {
            ViewMessage::set('Продавница је успешно ажурирана.', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је ажурирао продавницу " . $prodavnica->getNaziv());
        } else {
            ViewMessage::set('Грешка. Продавница није ажурирана.', MessageType::MESSAGE_DANGER);
        }
        redirectTo('/admin/prodavnica/');
    }

    public function deleteAction(int $id)
    {

        if (!$this->isAdminLoggedIn()) {
            redirectTo("/admin/prodavnica/");
            return;
        }
        $admin = (new KorisnikDao())->getById($this->getAdminId());
        $prodavnicaDao = new ProdavnicaDao();
        $prodavnica = $prodavnicaDao->getById($id);

        if (!empty($prodavnica) && $prodavnicaDao->delete($prodavnica)) {
            ViewMessage::set('Продавница је успешно обрисана', MessageType::MESSAGE_SUCCESS);
            Log::Write(Log::$eventLog, $admin->getIme() . " " . $admin->getPrezime() . "(администратор) је обрисао продавницу " . $prodavnica->getNaziv());

            redirectTo('/admin/prodavnica/');
        } else {
            ViewMessage::set('Грешка. Продавница није обрисана', MessageType::MESSAGE_DANGER);
            redirectTo('/admin/prodavnica/');
        }
    }
}