<?php
/**
 * Created by PhpStorm.
 * User: DusanT
 * Date: 29-Jan-18
 * Time: 14:57
 */

class RegistracijaController extends BaseController
{
    public function __construct()
    {
        if ($this->isAdminLoggedIn() || $this->isUserLoggedIn()) {
            redirectTo('/');
        }
    }

    public function indexAction()
    {
        echo $this->view('registracija/index.php');
    }

    public function createAction()
    {
        if ($_SERVER['REQUEST_METHOD'] !== "POST") {
            redirectTo('/error');
        }

        $ime = (isset($_POST['ime'])) ? $_POST['ime'] : null;
        $prezime = (isset($_POST['prezime'])) ? $_POST['prezime'] : null;
        $email = (isset($_POST['email'])) ? $_POST['email'] : null;
        $sifra = (isset($_POST['sifra'])) ? $_POST['sifra'] : null;
        $slika = (isset($_POST['slika'])) ? $_POST['slika'] : '';


        $ok = RegistracijaValidator::validate($ime, $prezime, $email, $sifra);


        if (is_array($ok)) {
            $korisnikDao = new KorisnikDao();
            $korisnik = $korisnikDao->getBy($ok);
            if (empty($korisnik)) {
                $noviKorisnik = Factory::createDatabaseFactory()->createKorisnikDbBuilder()->createKorisnikFromRegistration($ime, $prezime, $email, $sifra, $slika);
                if ($korisnikDao->insert($noviKorisnik)) {
                    echo 'Успешна регистрација';
                    Log::Write(Log::$eventLog, "Успешно је регистрован нови корисник. Име: " . $ime . " Презиме: " . $prezime . " Имејл: " . $email);
                } else {
                    echo 'Неуспешна регистрација';
                }
            } else {
                echo 'Корисник са унетим имејлом већ постоји.';
                return;
            }
        } else {
            echo $ok;
        }

    }
}